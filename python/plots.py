# Copyright (c) 2022 Arthur Perais
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import concurrent.futures
from tqdm import tqdm

import json
import math
import os
from os import listdir, mkdir
from os.path import join, exists

import pandas as pd
import pandas_bokeh

from bokeh.io import export_png, export_svg
from bokeh.models import ColumnDataSource, Range1d, LinearAxis
from bokeh.plotting import figure, output_file, save

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

def savePlot(plot, stat, path):
  if not exists(path):
    mkdir(path)

  export_svg(plot, filename=join(path, stat+'.svg'), timeout=99999999999999)
  output_file(join(path, stat+'.html'))
  save(plot)

def plotOne(frame, stat):
  source = ColumnDataSource(data=frame)
  p = frame.plot_bokeh.bar(
    ylabel=stat,
    xlabel='Workload',
    title=stat,
    figsize=(1920,1080),
    alpha=0.6,
    toolbar_location=None, show_figure=False
  )
  p.xaxis.major_label_orientation = math.pi/3
  p.add_layout(p.legend[0], 'above')
  return p

def plotStacked(frame, stat):
  source = ColumnDataSource(data=frame)
  p = frame.plot_bokeh.bar(
    xlabel='Workload',
    ylabel=stat,
    title=stat,
    alpha=0.6,
    figsize=(1920,1080),
    toolbar_location=None, show_figure=False,
    stacked=True
  )
  p.add_layout(p.legend[0], 'right')
  p.xaxis.major_label_orientation = math.pi/3
  return p

def generateCategoryDataframe(df_list):
  return pd.concat(df_list)

def generateScalarDataframe(stat, json_dict):
  stat_values = [float(json_dict[trace]['Stats'][stat]['Value']) for trace in json_dict.keys()]
  frame_dict = {trace:[value] for (trace, value) in zip(json_dict.keys(), stat_values)}

  df = pd.DataFrame.from_dict(frame_dict, orient='index', columns=[stat])
  df = df.sort_values(by=[stat], axis=0)
  df.loc['Average'] = df.mean()
  return df

def generateHistoDataframe(stat, json_dict):
  stat_values = [[float(trace_dict['Stats'][stat]['Value'][bucket]) for bucket in trace_dict['Stats'][stat]['Value'].keys()] for trace_dict in json_dict.values()]
  frame_dict = {trace:value for (trace, value) in zip(json_dict.keys(), stat_values)}
  df = pd.DataFrame.from_dict(frame_dict, orient='index', columns=[bucket for bucket in json_dict[list(json_dict.keys())[0]]['Stats'][stat]['Value'].keys()])

  # FIXME Provide key to sort with
  sort_key = df.columns[1]
  df = df.sort_values(by=[sort_key], axis=0)
  df.loc['Average'] = df.mean()
  return df

def generatePeriodicDataframe(stat, json_dict):
  stat_values = [[float(trace_dict['Stats'][stat]['Value'][bucket]) for bucket in trace_dict['Stats'][stat]['Value'].keys()] for trace_dict in json_dict.values()]
  traces = list(json_dict.keys())
  frame_dict = {trace:value for (trace, value) in zip(traces, stat_values)}

  # Find max columns
  max_columns = []
  for trace in traces:
    if len(json_dict[trace]['Stats'][stat]['Value'].keys()) > len(max_columns):
      max_columns = [bucket for bucket in json_dict[trace]['Stats'][stat]['Value'].keys()]

  for trace in frame_dict.keys():
    while len(frame_dict[trace]) < len(max_columns):
      frame_dict[trace].append(0)

  df = pd.DataFrame.from_dict(frame_dict, orient='index', columns=max_columns)

  # FIXME Provide key to sort with
  sort_key = df.columns[1]
  df = df.sort_values(by=[sort_key], axis=0)
  df.loc['Average'] = df.mean()
  return df

def normalizeHisto(df):
  rel_frame = pd.DataFrame()
  had_avg = 'Average' in df.index

  if had_avg:
    df = df.drop('Average', axis=0)

  for bucket in df.columns:
    rel_frame[bucket] = df[bucket] / df.sum(axis=1)

  sort_key = df.columns[1]
  rel_frame = rel_frame.sort_values(by=[sort_key], axis=0)

  if had_avg:
    rel_frame.loc['Average'] = rel_frame.mean()

  return rel_frame

def extractCategories(json_dict):
  cat_dict = {}
  cat_list = list(dict.fromkeys([json_dict[tool][next(iter(json_dict[tool].keys()))]['ToolCategory'] for tool in json_dict.keys()]))

  for cat in cat_list:
    cat_dict[cat] = [tool for tool in json_dict.keys() if json_dict[tool][next(iter(json_dict[tool].keys()))]['ToolCategory'] == cat]

  stat_dict = {}

  for cat in cat_list:
    stat_dict[cat] = {}
    stat_dict[cat]["Scalar"] = availableScalarStats(json_dict[cat_dict[cat][0]])
    stat_dict[cat]["Histo"] = availableHistogramStats(json_dict[cat_dict[cat][0]])
    stat_dict[cat]["Periodic"] = availablePeriodicStats(json_dict[cat_dict[cat][0]])

  return (cat_dict, stat_dict)

def constructDataDict(runs):
  json_dict = {}
  for tool in runs[0].toolinfo.keys():
    json_dict[tool] = {}
    for run in runs:
      json_dict[tool][run.name()] = run.tooldata[tool]

  return json_dict

def availableScalarStats(json_dict):
  return [stat for stat in json_dict[list(json_dict.keys())[0]]['Stats'].keys() if json_dict[list(json_dict.keys())[0]]['Stats'][stat]['Type'] == 'Scalar']

def availableHistogramStats(json_dict):
  return [stat for stat in json_dict[list(json_dict.keys())[0]]['Stats'].keys() if json_dict[list(json_dict.keys())[0]]['Stats'][stat]['Type'] == 'Histogram']

def availablePeriodicStats(json_dict):
  return [stat for stat in json_dict[list(json_dict.keys())[0]]['Stats'].keys() if json_dict[list(json_dict.keys())[0]]['Stats'][stat]['Type'] == 'Periodic']

def generatePlots(runs, study_dir):
  json_dict = constructDataDict(runs)

  for tool in json_dict.keys():
    for stat in availableScalarStats(json_dict[tool]):
      df = generateScalarDataframe(stat, json_dict[tool])
      savePlot(plotOne(df, stat), stat, join(study_dir, "plots", tool))

    for stat in availableHistogramStats(json_dict[tool]):
      df = generateHistoDataframe(stat, json_dict[tool])
      savePlot(plotStacked(df, stat), stat, join(study_dir, "plots", tool))
      df = normalizeHisto(df)
      savePlot(plotStacked(df, "normalized_" + stat), "normalized_" + stat, join(study_dir, "plots", tool))

    for stat in availablePeriodicStats(json_dict[tool]):
      df = generatePeriodicDataframe(stat, json_dict[tool])
      savePlot(plotStacked(df, stat), stat, join(study_dir, "plots", tool))

  tool_cats, cat_stats = extractCategories(json_dict)
  for tool_cat in tool_cats:
    for stat in cat_stats[tool_cat]["Scalar"]:
      df_list = []
      for tool in tool_cats[tool_cat]:
        df = generateScalarDataframe(stat, json_dict[tool])
        df_list.append(pd.DataFrame.from_dict({tool:[df.loc['Average'][stat]]}, orient='index', columns=[stat]))
        cat_df = generateCategoryDataframe(df_list)
        savePlot(plotOne(cat_df, 'avg_' + stat), 'avg_' + stat, join(study_dir, "plots", tool_cat))

    for stat in cat_stats[tool_cat]["Histo"]:
      df_list = []
      for tool in tool_cats[tool_cat]:
        df = generateHistoDataframe(stat, json_dict[tool])
        df_list.append(pd.DataFrame.from_dict({tool:list(df.loc['Average'])}, orient='index', columns=df.columns))
        cat_df = generateCategoryDataframe(df_list)
        savePlot(plotStacked(cat_df, 'avg_' + stat), 'avg_' + stat, join(study_dir, "plots", tool_cat))
        cat_df = normalizeHisto(cat_df)
        savePlot(plotStacked(cat_df, 'normalized_avg_' + stat), 'normalized_avg_' + stat,  join(study_dir, "plots", tool_cat))

    for stat in cat_stats[tool_cat]["Periodic"]:
      df_list = []
      for tool in tool_cats[tool_cat]:
        df = generatePeriodicDataframe(stat, json_dict[tool])
        df_list.append(pd.DataFrame.from_dict({tool:list(df.loc['Average'])}, orient='index', columns=df.columns))
        cat_df = generateCategoryDataframe(df_list)
        savePlot(plotStacked(cat_df, 'avg_' + stat), 'avg_' + stat, join(study_dir, "plots", tool_cat))
