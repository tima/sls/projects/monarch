# Copyright (c) 2022 Arthur Perais
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from os.path import isfile, join, exists, abspath
from os import listdir, mkdir, system, chdir, getcwd


from http.server import BaseHTTPRequestHandler, CGIHTTPRequestHandler, HTTPServer, SimpleHTTPRequestHandler
from functools import partial

import sys
import json

from Monarch import *
from Monarch.Tracing import *
from Monarch.Tools import *

class MonarchRun:
  
  def __init__(self, path, trace, info):
    self.path = path
    self.tracename = trace.split("/")[-1]
    self.tracedir = trace
    self.toolinfo = info
    self.tooldata = {}

  def name(self):
    return self.tracename
    
  def addTool(self, toolname, toolparams):
    self.toolinfo[toolname] = toolparams

  def serialize(self):
    # Tooldata is read only
    with open(join(self.path, "info.json"), "w") as infofile:
      info = {'Tools' : self.toolinfo, 'TraceDir' : self.tracedir}
      json.dump(info, infofile)

  def toolCategories(self):
    # Maybe do it with exposed per-class static function instead ?
    cats = []
    for tool in self.tooldata.keys():
      cats.append(self.tooldata[tool]['ToolCategory'])
    return list(dict.fromkeys(cats))

  def toolNames(self):
    return self.toolinfo.keys()

  def run(self, force = False):
    chdir(self.path)

    old_stdout = sys.stdout
    old_stderr = sys.stderr

    sys.stdout = open('stdout', 'w')
    sys.stderr = open('stderr', 'w')
  
    # Create the runtime objects
    driver = Driver()
    reader = TraceReader(self.tracedir)
    profilers = [globals()[self.toolinfo[tool_name]["type"]](self.toolinfo[tool_name]) for tool_name in self.toolinfo.keys()]
    driver.registerTraceReader(reader)
    
    for profiler in profilers:
      driver.registerTool(profiler)

    try:
      driver.run()
    except:
      sys.stdout.close()
      sys.stderr.close()

      sys.stdout = old_stdout
      sys.stderr = old_stderr

      print("Failed to run", self.tracename)
    else:
      print("Run succesful")

    sys.stdout.close()
    sys.stderr.close()

    sys.stdout = old_stdout
    sys.stderr = old_stderr
  
  def runGdb(self):
    chdir(self.path)
  
    driver = Driver()
    reader = TraceReader(self.tracedir)
  
    profilers = [globals()[self.toolinfo[tool_name]["type"]](self.toolinfo[tool_name]) for tool_name in self.toolinfo.keys()]
    driver.registerTraceReader(reader)
    
    for profiler in profilers:
      driver.registerTool(profiler)

    driver.run()

  def __str__(self):
    return "MonarchRun (%s, %s, %s)" % (str(self.path), str(self.tracedir), str(self.toolinfo))

  def refresh(self):
    print("Reloading from", self.path)
    with open(join(self.path, 'info.json'), 'r') as infofile:
      info = json.load(infofile)
      self.__init__(abspath(self.path), info['TraceDir'], info['Tools'])

      for tool in self.toolinfo.keys():
        if exists(join(self.path, tool + '.json')):
          with open(join(self.path, tool + '.json'), 'r') as infofile:
            self.tooldata[tool] = json.load(infofile)


  @staticmethod
  def load(path):
    with open(join(path, 'info.json'), 'r') as infofile:
      info = json.load(infofile)
      run = MonarchRun(abspath(path), info['TraceDir'], info['Tools'])

      for tool in run.toolinfo.keys():
        if exists(join(path, tool + '.json')):
          with open(join(path, tool + '.json'), 'r') as infofile:
            run.tooldata[tool] = json.load(infofile)

    return run

  @staticmethod
  def create(path, trace, tools):
    return MonarchRun(abspath(path), trace, tools)

class MonarchExperiment:

  def __init__(self, path, tools, runs):
    self.study_path = path
    self.toolinfo = tools
    self.runs = runs

  def addTool(self, toolname, toolparams):
    toolparams["name"] = toolname
    self.toolinfo[toolname] = toolparams
    for run in self.runs:
      run.addTool(toolname, toolparams)

  def addTraceDir(self, tracedirname):
    traces = [trace for trace in listdir(tracedirname)]
    for trace in traces:
      if len([run for run in self.runs if run.tracedir == abspath(trace)]) != 0:
        print("## Rundir already exists, is this really what you wanted to do ?")
      else:
        self.runs.append(MonarchRun.create(join(self.study_path, "runs", trace), abspath(join(tracedirname, trace)), self.toolinfo.copy()))
    
  def serialize(self):
    if not exists(self.study_path):
      mkdir(self.study_path)
      mkdir(join(self.study_path, "runs"))
      mkdir(join(self.study_path, "plots"))

    with open(join(self.study_path, "info.json"), "w") as infofile:
      info = {'Tools' : self.toolinfo}
      json.dump(info, infofile)
    for run in self.runs:
      if not exists(join(run.path)):
        mkdir(run.path)
      run.serialize()

  def generatePlots(self):
    import plots
    plots.generatePlots(self.runs, self.study_path)
    
  def serveResultsOverHTTP(self):
    chdir(join(self.study_path, "plots"))
    import platform
    hostname = platform.node()
    port = 8080
    handler = partial(MonarchHTTPRequestHandler, self)
    webserver = HTTPServer((hostname, port), handler)

    print("Serving results at: http://" + hostname + ":" + str(port))
    webserver.serve_forever()

  def __str__(self):
    repstr = "MonarchStudy\nDir: %s\n Tools: %s\nRundirs:\n" % (str(self.study_path), (str(self.toolinfo.keys())))
    for run in self.runs:
      repstr = repstr + str(run) + "\n"
    return repstr

  def refresh(self):
    for run in self.runs:
      run.refresh()

  @staticmethod
  def create(path):
    if exists(path):
      print("##", path, "exists, load it with MonarchExperiment.load()")
      return None
    else:
      print("## Creating study dir:", path)
      
    return MonarchExperiment(path, {}, [])

  @staticmethod
  def load(path):
    if not exists(path):
      print("## Directory", path, "does not exist")
      return None
    else:
      # Open study info
      with open(join(path, 'info.json'), 'r') as infofile:
        infos = json.load(infofile)
        tools = infos['Tools']
        runs = []
        for run in listdir(join(path, "runs")):
          runs.append(MonarchRun.load(join(path, "runs", run)))

        exp = MonarchExperiment(path, tools, runs)
        return exp

class MonarchHTTPRequestHandler(SimpleHTTPRequestHandler):
  def __init__(self, experiment, *args, **kwargs):
    self.exp = experiment
    super().__init__(*args, **kwargs)

  def do_GET(self):
    self.send_response(200)
    self.send_header("Content-type", "text/html")
    self.end_headers()

    if ".html" in self.path:
      return SimpleHTTPRequestHandler.do_GET(self)

    self.wfile.write(bytes("<html><head><title>https://pythonbasics.org</title></head>", "utf-8"))

    for path in getcwd():
      self.wfile.write(bytes("<h1>%s</h1>" % path, "utf-8"))
      for plot in listdir(join(getcwd(),path)):
        if ".html" in plot:
          self.wfile.write(bytes("<p><a href=%s" % join(path, plot), "utf-8"))
          self.wfile.write(bytes(">%s</a></p>" % plot, "utf-8"))

    self.wfile.write(bytes("</body></html>", "utf-8"))
