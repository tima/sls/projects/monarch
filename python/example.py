#!/bin/python

# Copyright (c) 2022 Arthur Perais
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import multiprocessing
import json

from run import runParallel, runSerial

parser = argparse.ArgumentParser(description='Run some traces, print interesting metrics')
parser.add_argument('--trace_dir', help='<Required> Directory containing traces to run', dest='trace_dir', action='store', required=True)
parser.add_argument('--exp_dir', help='<Required> Directory to run the analysis in', dest='exp_dir', action='store', required=True)
parser.add_argument('--config_file', help='<Required> Path to config file', dest='config_path', action='store', required=True)

parser.add_argument('--num_workers', help='<Optional> Number of parallel runs', dest='num_workers', type=int, action='store', default=multiprocessing.cpu_count() - 2)
parser.add_argument('--gdb', help='<Optional> Launch a single run under gdb', action='store_true', dest='enable_gdb')

args = parser.parse_args()

def main():
  
  from structs import MonarchExperiment

  exp = MonarchExperiment.create(args.exp_dir)
  if exp is None:
    print("## Experiment folder already exists, exiting")
    exit()

  with open(args.config_path, 'r') as config_file:
    config_dict = json.load(config_file)

  exp.addTraceDir(args.trace_dir)

  for tool in config_dict["tools"].keys():
    toolname = tool
    toolparams = config_dict["tools"][toolname]
    exp.addTool(toolname, toolparams)

  exp.serialize()

  if args.enable_gdb:
    print("## Running ", exp.runs[0].tracename)
    exp.runs[0].runGdb()
  else:
    print("## Running ")
    res = runParallel(exp.runs, args.num_workers)

  exp.refresh()
  exp.generatePlots()

  

if __name__ == "__main__":
    main()
