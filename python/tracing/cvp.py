#!/bin/python

# Copyright (c) 2022 Arthur Perais
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import multiprocessing
import concurrent.futures
import os
import time
import sys
import time
from tqdm import tqdm

from os import listdir, mkdir, system
from os.path import isfile, join, exists, isdir

import argparse
import inspect

parser = argparse.ArgumentParser(description='Convert CVP2 Traces')
parser.add_argument('--tracedir', help='<Required> Directory containing traces to run', dest='trace_dir', action='store', required=True)
parser.add_argument('--outdir', help='<Required> Directory to run the analysis in', dest='outdir', action='store', required=True)
parser.add_argument('--converter', help='<Required> Path to converter', dest='converter_path', action='store', required=True)
parser.add_argument('--num_workers', help='<Optional> Number of parallel runs', dest='num_workers', type=int, action='store', default=multiprocessing.cpu_count() - 2)

args = parser.parse_args()

def convertOne(params):
  trace_path = join(args.trace_dir, params["input_trace"])
  outpath = join(args.outdir, params["output_trace"])

  if exists(outpath):
    print("##", outpath, "exists, giving up to avoid overwriting it")
    exit()
  else:
    print("## Creating output dir:", outpath)
    mkdir(outpath)

  command = args.converter_path + " " + trace_path + " " + outpath
  os.system(command)

def main():
  # Create ouput dir
  if exists(args.outdir):
    print("##", args.outdir, "exists, giving up to avoid overwriting it")
    exit()
  else:
    mkdir(args.outdir)

  runs = []
  # Collect all traces to convert
  for trace in listdir(args.trace_dir):
    runs.append({"input_trace" : trace, "output_trace" : str(trace).split(".gz")[0]})

  print(runs)

  with concurrent.futures.ProcessPoolExecutor(max_workers=args.num_workers) as executor:
    res = list(tqdm(executor.map(convertOne, runs), leave=True, colour="blue", desc="Running", total=len(runs)))


if __name__ == "__main__":
    main()
