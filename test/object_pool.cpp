/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <gtest/gtest.h>

#include <util/object_pool.h>

namespace
{

TEST(ObjectPool, ObjectPoolPrimitive)
{
  ObjectPool<int, 1> int_pool;
#ifndef NDEBUG
  ASSERT_EQ(int_pool.capacity(), 1);
#endif
  int * ptr = int_pool.allocate(5);
#ifndef NDEBUG
  ASSERT_EQ(int_pool.occupancy(), 1);
#endif
  ASSERT_NE(ptr, nullptr);
  ASSERT_EQ(*ptr, 5);
  int_pool.deallocate(ptr);
#ifndef NDEBUG
  ASSERT_EQ(int_pool.occupancy(), 0);
#endif

  int * ptr2 = int_pool.allocate(10);
#ifndef NDEBUG
  ASSERT_EQ(int_pool.capacity(), 1);
#endif
  ASSERT_EQ(ptr, ptr2);
  ASSERT_EQ(*ptr, 10);
  ASSERT_EQ(*ptr2, 10);
#ifndef NDEBUG
  ASSERT_EQ(int_pool.occupancy(), 1);
#endif

  int * ptr3 = int_pool.allocate(42);
#ifndef NDEBUG
  ASSERT_EQ(int_pool.capacity(), 2);
  ASSERT_EQ(int_pool.occupancy(), 2);
#endif
  ASSERT_NE(ptr2, ptr3);
  ASSERT_EQ(*ptr2, 10);
  ASSERT_EQ(*ptr3, 42);

  int_pool.deallocate(ptr2);
  int_pool.deallocate(ptr3);
#ifndef NDEBUG
  ASSERT_EQ(int_pool.capacity(), 2);
  ASSERT_EQ(int_pool.occupancy(), 0);
#endif
}

TEST(ObjectPool, ObjectPoolPod)
{
  struct PODType
  {
    uint64_t m_data;
    uint64_t *m_ptr;

    PODType(uint64_t val)
    : m_data(val)
    , m_ptr(&m_data)
    {}

    PODType() = default;
  };

  ObjectPool<PODType, 1> pod_pool;
#ifndef NDEBUG
  ASSERT_EQ(pod_pool.capacity(), (size_t) 1);
#endif
  PODType * ptr = pod_pool.allocate(5);
#ifndef NDEBUG
  ASSERT_EQ(pod_pool.occupancy(), (size_t) 1);
#endif
  ASSERT_NE(ptr, nullptr);
  ASSERT_EQ(ptr->m_data, 5lu);
  pod_pool.deallocate(ptr);
#ifndef NDEBUG
  ASSERT_EQ(pod_pool.occupancy(), (size_t) 0);
#endif

  PODType * ptr2 = pod_pool.allocate(10);
#ifndef NDEBUG
  ASSERT_EQ(pod_pool.capacity(), (size_t) 1);
#endif
  ASSERT_EQ(ptr, ptr2);
  ASSERT_EQ(ptr->m_data, 10lu);
  ASSERT_EQ(ptr2->m_data, 10lu);
#ifndef NDEBUG
  ASSERT_EQ(pod_pool.occupancy(), (size_t) 1);
#endif

  PODType * ptr3 = pod_pool.allocate(42);
#ifndef NDEBUG
  ASSERT_EQ(pod_pool.capacity(), (size_t) 2);
  ASSERT_EQ(pod_pool.occupancy(), (size_t) 2);
#endif
  ASSERT_NE(ptr2, ptr3);
  ASSERT_EQ(ptr2->m_data, 10lu);
  ASSERT_EQ(ptr3->m_data, 42lu);

  pod_pool.deallocate(ptr2);
  pod_pool.deallocate(ptr3);
#ifndef NDEBUG
  ASSERT_EQ(pod_pool.capacity(), (size_t) 2);
  ASSERT_EQ(pod_pool.occupancy(), (size_t) 0);
#endif
}

}