/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <gtest/gtest.h>

#include <trace/lzma_buf.h>
#include <trace/lzma_threaded_streamer.h>

#include <cstdio>
#include <random>
#include <algorithm>
#include <cstring>
#include <iostream>

namespace
{
  
template<size_t Size>
bool doTheThing()
{
  std::string name = std::to_string(Size);
  std::random_device rd;
  shrinkwrap::xz::ostream output(name + ".xz");
  std::array<char, Size> * input_buf = new std::array<char, Size>();

  std::generate(input_buf->begin(), input_buf->end(), [&]()
  {
    return (char) (rd() % 255);
  });

  output.write(input_buf->data(), Size);
  output.flush();

  shrinkwrap::xz::istream input(name + ".xz");
  ThreadedLZMAInputStreamer streamer(input);

  std::array<char, Size> * output_buf = new std::array<char, Size>();
  streamer.read(output_buf->data(), Size);
  remove((name + ".xz").c_str());
  bool equal = memcmp(input_buf->data(), output_buf->data(), Size) == 0;

  delete input_buf;
  delete output_buf;

  return equal;
}

TEST(ThreadedLZMAInputStreamer, SmallRead)
{
  ASSERT_EQ(doTheThing<512>(), true);
}

TEST(ThreadedLZMAInputStreamer, BufSizeMinusOne)
{
 ASSERT_EQ(doTheThing<(1024*1024)-1>(), true);
}

TEST(ThreadedLZMAInputStreamer, BufSize)
{
  ASSERT_EQ(doTheThing<(1024*1024)>(), true);
}

TEST(ThreadedLZMAInputStreamer, BufSizePlusOne)
{
  ASSERT_EQ(doTheThing<(1024*1024)+1>(), true);
}

TEST(ThreadedLZMAInputStreamer, ThreeBufSizePlusOne)
{
  ASSERT_EQ(doTheThing<(1024*1024*3)+1>(), true);
}

}