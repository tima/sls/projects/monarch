/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <gtest/gtest.h>

#include <trace/instruction.h>
#include <trace/lzma_buf.h>
#include <trace/lzma_threaded_streamer.h>

#include <iostream>
#include <fstream>
#include <cstdio>
#include <unordered_map>
#include <map>

using namespace Monarch;

namespace
{

TEST(ProgramCounter, ProgramCounter) {
  ProgramCounter pc;
  pc = {
    0x42,
    {      
      0x12,
      0x0
    }
  };

  ASSERT_EQ(pc.m_pc, 0x42);
  ASSERT_EQ(pc.m_context.m_context_id, 0x12);
  ASSERT_EQ(pc.m_context.m_ring, 0x0);
}

TEST(ProgramCounter, ProgramCounterEquality) {
  ProgramCounter pc1 = {
    0x42,
    {
      0x12,
      0x0
    }
  };

  ProgramCounter pc2 = {
    0x42,
    {
      0x12,
      0x0
    }
  };

  ASSERT_EQ(pc1, pc2);
}

TEST(ProgramCounter, ProgramCounterAssign)
{
  ProgramCounter pc1 = {
    0x42,
    {
      0x12,
      0x0
    }
  };

  ProgramCounter pc2 = pc1;
  ASSERT_EQ(pc1, pc2);
}

TEST(ProgramCounter, ProgramCounterAsKey)
{
  std::map<ProgramCounter, uint64_t> test_map;

  ProgramCounter pc1 = {
    0x0,
    {
      0x0,
      0x0
    }
  };

  ProgramCounter pc2 = {
    0xffffffffffffffff,
    {
      0xffffffffffffff,
      0xff
    }
  };

  ASSERT_EQ(std::hash<ProgramCounter>()(pc1), std::hash<ProgramCounter>()(pc2));
  test_map[pc1] = 12;
  test_map[pc2] = 42;
  ASSERT_EQ(test_map.size(), 2);

  std::set<ProgramCounter> test_set;

  test_set.insert(pc1);
  test_set.insert(pc2);

  ASSERT_EQ(test_set.size(), 2);
}

TEST(StaticRegInfo, SerDes)
{
  shrinkwrap::xz::ostream os("test_static_reg_info.bin");
  
  StaticRegInfo write_test;
  write_test.m_num_regs = 2;
  write_test.m_regs = {12, 17};
  write_test.m_reg_widths = {32, 64};
  std::cout << "Serializing " << write_test << std::endl;
  serialize(os, write_test);
  os.flush();

  shrinkwrap::xz::istream is_stream("test_static_reg_info.bin");
  ThreadedLZMAInputStreamer is(is_stream);

  StaticRegInfo read_test;
  deserialize(is, read_test);
  std::cout << "Deserializing " << read_test << std::endl;

  ASSERT_EQ(read_test == write_test, true);
  remove("test_static_reg_info.bin");
}

TEST(StaticBranchInfo, SerDes)
{
  shrinkwrap::xz::ostream os("test_static_branch_info.bin");
  
  StaticBranchInfo write_test;
  write_test.m_type = StaticBranchInfo::BranchType::Uncond;
  write_test.m_target_type = StaticBranchInfo::BranchTargetType::Indirect;
  std::cout << "Serializing " << write_test << std::endl;
  serialize(os, write_test);
  os.flush();

  shrinkwrap::xz::istream is_stream("test_static_branch_info.bin");
  ThreadedLZMAInputStreamer is(is_stream);

  StaticBranchInfo read_test;
  deserialize(is, read_test);
  std::cout << "Deserializing " << read_test << std::endl;

  ASSERT_EQ(read_test == write_test, true);
  remove("test_static_branch_info.bin");
}

TEST(StaticMemoryInfo, SerDes)
{
  shrinkwrap::xz::ostream os("test_static_mem_info.bin");
  
  StaticMemoryInfo write_test;
  write_test.m_access_size = 8;
  write_test.m_base_register = 5;
  write_test.m_offset_register = 12;
  write_test.m_immediate = 0xff;
  write_test.m_mode = StaticMemoryInfo::AddressingMode::PostIncrement;
  write_test.m_type = StaticMemoryInfo::AccessType::ReadWrite;
  std::cout << "Serializing " << write_test << std::endl;
  serialize(os, write_test);
  os.flush();

  shrinkwrap::xz::istream is_stream("test_static_mem_info.bin");
  ThreadedLZMAInputStreamer is(is_stream);

  StaticMemoryInfo read_test;
  deserialize(is, read_test);
  std::cout << "Deserializing " << read_test << std::endl;

  ASSERT_EQ(read_test == write_test, true);
  remove("test_static_mem_info.bin");
}

TEST(StaticFlagWriterInfo, SerDes)
{
  shrinkwrap::xz::ostream os("test_static_flag_writer_info.bin");
  
  StaticFlagWriterInfo write_test;
  write_test.m_output_flags = 0xf;
  std::cout << "Serializing " << write_test << std::endl;
  serialize(os, write_test);
  os.flush();

  shrinkwrap::xz::istream is_stream("test_static_flag_writer_info.bin");
  ThreadedLZMAInputStreamer is(is_stream);

  StaticFlagWriterInfo read_test;
  deserialize(is, read_test);
  std::cout << "Deserializing " << read_test << std::endl;

  ASSERT_EQ(read_test == write_test, true);
  remove("test_static_flag_writer_info.bin");
}

TEST(StaticFlagReaderInfo, SerDes)
{
  shrinkwrap::xz::ostream os("test_static_flag_reader_info.bin");
  
  StaticFlagReaderInfo write_test;
  write_test.m_input_flags = 0xf;
  std::cout << "Serializing " << write_test << std::endl;
  serialize(os, write_test);
  os.flush();

  shrinkwrap::xz::istream is_stream("test_static_flag_reader_info.bin");
  ThreadedLZMAInputStreamer is(is_stream);

  StaticFlagReaderInfo read_test;
  deserialize(is, read_test);
  std::cout << "Deserializing " << read_test << std::endl;

  ASSERT_EQ(read_test == write_test, true);
  remove("test_static_flag_reader_info.bin");
}

TEST(StaticSimdInfo, SerDes)
{
  shrinkwrap::xz::ostream os("test_static_simd_info.bin");
  
  StaticSimdInfo write_test;
  write_test.m_element_size = 8;
  std::cout << "Serializing " << write_test << std::endl;
  serialize(os, write_test);
  os.flush();

  shrinkwrap::xz::istream is_stream("test_static_simd_info.bin");
  ThreadedLZMAInputStreamer is(is_stream);

  StaticSimdInfo read_test;
  deserialize(is, read_test);
  std::cout << "Deserializing " << read_test << std::endl;

  ASSERT_EQ(read_test == write_test, true);
  remove("test_static_simd_info.bin");
}

TEST(StaticPredicationInfo, SerDes)
{
  shrinkwrap::xz::ostream os("test_static_predication_info.bin");
  
  StaticPredicationInfo write_test;
  write_test.m_predicates = 0xdeadbeef;
  std::cout << "Serializing " << write_test << std::endl;
  serialize(os, write_test);
  os.flush();

  shrinkwrap::xz::istream is_stream("test_static_predication_info.bin");
  ThreadedLZMAInputStreamer is(is_stream);

  StaticPredicationInfo read_test;
  deserialize(is, read_test);
  std::cout << "Deserializing " << read_test << std::endl;

  ASSERT_EQ(read_test == write_test, true);
  remove("test_static_predication_info.bin");
}

 char m_disassembly [128];

  StaticInstFlags m_type;

  StaticRegInfo m_input_regs;
  StaticRegInfo m_output_regs;
  std::optional<StaticBranchInfo> m_br_info;
  std::optional<StaticMemoryInfo> m_mem_info;
  std::optional<StaticSysRegInfo> m_sysreg_info;
  std::optional<StaticFlagWriterInfo> m_flag_writer_info;
  std::optional<StaticFlagReaderInfo> m_flag_reader_info;
  std::optional<StaticSimdInfo> m_simd_info;
  std::optional<StaticPredicationInfo> m_predication_info;

TEST(StaticInstInfo, SerDes)
{  
  shrinkwrap::xz::ostream os("test_static_info.bin");
  
  StaticInstInfo write_test;
  strcpy(write_test.m_disassembly, "Ceci n'est pas une instruction");
  write_test.m_type.set(InstructionType::Atomic | InstructionType::Fence | InstructionType::Logic);
  write_test.m_input_regs.m_num_regs = 2;
  write_test.m_input_regs.m_regs = {12, 42};
  write_test.m_input_regs.m_reg_widths = {64, 64};
  write_test.m_output_regs.m_num_regs = 2;
  write_test.m_output_regs.m_regs = {3, 39};
  write_test.m_output_regs.m_reg_widths = {64, 64};

  std::cout << "Serializing " << write_test << std::endl;
  serialize(os, write_test);
  os.flush();

  shrinkwrap::xz::istream is_stream("test_static_info.bin");
  ThreadedLZMAInputStreamer is(is_stream);

  StaticInstInfo read_test;
  deserialize(is, read_test);
  std::cout << "Deserializing " << read_test << std::endl;

  ASSERT_EQ(read_test == write_test, true);
  remove("test_static_info.bin");
}

}