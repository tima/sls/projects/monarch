find_package(Python REQUIRED COMPONENTS Interpreter Development)

set(BINARY ${CMAKE_PROJECT_NAME}_test)

set(MAIN_FILE main.cpp)
set(UNIT_TESTS stats.cpp cache_model.cpp object_pool.cpp reference_counted.cpp instruction.cpp bitvector.cpp lzma_threaded_streamer.cpp fixed_array.cpp)

add_executable(unit-test ${UNIT_TESTS} ${MAIN_FILE})
target_include_directories(unit-test PUBLIC ${Python_INCLUDE_DIRS})
target_include_directories(unit-test PUBLIC ../include ../extern/pybind11/include)

target_link_libraries(unit-test gtest gtest_main rt lzma pthread ${Python_LIBRARIES})

set_target_properties(unit-test
  PROPERTIES
  ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/test"
  LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/test"
  RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/test"
)
