/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <gtest/gtest.h>

#include <stats/stats.h>

namespace
{

TEST(NameGenerator, Gen)
{
  NameGenerator test("item");
  ASSERT_EQ(test(), "item0");
  ASSERT_EQ(test(), "item1");
  ASSERT_EQ(test(), "item2");
  ASSERT_EQ(test(), "item3");
}

TEST(Stats, CounterInit) {
  Counter cnt;
  ASSERT_EQ(cnt.evaluate(), 0.0);
}

TEST(Stats, CounterIncrement)
{
  Counter cnt;
  cnt++;
  ASSERT_EQ(cnt.evaluate(), 1.0);
  cnt++;
  ASSERT_EQ(cnt.evaluate(), 2.0);
}

TEST(Stats, CounterAssign)
{
  Counter cnt = 42;
  ASSERT_EQ(cnt.evaluate(), 42.0);
}

TEST(Stats, CounterReset)
{
  Counter cnt = 42;
  cnt.reset();
  ASSERT_EQ(cnt.evaluate(), 0.0);
  cnt++,
  cnt.reset();
  ASSERT_EQ(cnt.evaluate(), 0.0);
}

TEST(Stats, FormulaSimple)
{
  Counter x;
  Formula f;
  f = x * 1;
  ASSERT_EQ(f.evaluate(), 0.0);
  x++;
  ASSERT_EQ(f.evaluate(), 1.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 42.0);
}

TEST(Stats, FormulaShared)
{
  {
    Counter x = 4;
    Counter y = 4;
    Formula f1;
    f1 = x - y;
    ASSERT_EQ(f1.evaluate(), 0.0);
    Formula f2;
    f2 = x + y;
    ASSERT_EQ(f2.evaluate(), 8.0);
  }
}

TEST(Stats, FormulaSimpleOperators)
{
  {
  Counter x;
  Formula f;
  f = x + 1;
  ASSERT_EQ(f.evaluate(), 1.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 43.0);
  }
  {
  Counter x;
  Formula f;
  f = x - 1;
  ASSERT_EQ(f.evaluate(), -1.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 41.0);
  }
  {
  Counter x;
  Formula f;
  f = x * 1;
  ASSERT_EQ(f.evaluate(), 0.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 42.0);
  }
  {
  Counter x;
  Formula f;
  f = x / 1;
  ASSERT_EQ(f.evaluate(), 0.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 42.0);
  }
  {
  Counter x;
  Formula f;
  f = x % 1;
  ASSERT_EQ(f.evaluate(), 0.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 0.0);
  }
}

TEST(Stats, FormulaComplex)
{
  Counter x;
  Counter y = 42;
  Formula f;
  f = (x - y) + ((-1.0 * x) + y);
  ASSERT_EQ(f.evaluate(), 0.0);

  Formula f2;
  x++;
  f2 = (((2 * x) + (4 % y)) / 2) - 4;
  ASSERT_EQ(f2.evaluate(), -1.0);
}

TEST(Stats, FormulaComplexOperators)
{
  {
  Counter x;
  Counter y = 42;
  Formula f;
  f = x + y;
  ASSERT_EQ(f.evaluate(), 42.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 84.0);
  }
  {
  Counter x;
  Counter y = 42;
  Formula f;
  f = x - y;
  ASSERT_EQ(f.evaluate(), -42.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 0);
  }
  {
  Counter x;
  Counter y = 42;
  Formula f;
  f = x * y;
  ASSERT_EQ(f.evaluate(), 0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 42.0 * 42.0);
  }
  {
  Counter x;
  Counter y = 42;
  Formula f;
  f = x / y;
  ASSERT_EQ(f.evaluate(), 0.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 1.0);
  }
  {
  Counter x;
  Counter y = 42;
  Formula f;
  f = x % y;
  ASSERT_EQ(f.evaluate(), 0.0);
  x = 42;
  ASSERT_EQ(f.evaluate(), 0.0);
  }
}

TEST(Stats, FixedBucketsHistogram)
{
  FixedBucketsHistogram histo("dut", 1, 5);
  histo.addElement(0);
  ASSERT_EQ(histo.m_buckets[0], 1);
  histo.addElement(4, 23);
  ASSERT_EQ(histo.m_buckets[4], 23);
  histo.addElement(15, 12);
  ASSERT_EQ(histo.m_overflows, 12);
  histo.addElement(5);
  ASSERT_EQ(histo.m_overflows, 13);

  histo.reset();
  ASSERT_EQ(histo.m_buckets[0], 0);
  ASSERT_EQ(histo.m_buckets[1], 0);
  ASSERT_EQ(histo.m_buckets[2], 0);
  ASSERT_EQ(histo.m_buckets[3], 0);
  ASSERT_EQ(histo.m_buckets[4], 0);
  ASSERT_EQ(histo.m_overflows, 0);
}

}