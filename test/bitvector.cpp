/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <gtest/gtest.h>

#include <trace/lzma_buf.h>
#include <trace/lzma_threaded_streamer.h>

#include <util/bitvector.h>

using namespace Monarch;

namespace
{

enum class TestFlags
{
  A,
  B, 
  C,
  numFlags
};

using TestBitvector = Bitvector<TestFlags>;

TEST(Bitvector, Construct) 
{
  TestBitvector bv1;
  ASSERT_EQ(bv1.none(), true);
  ASSERT_EQ(bv1.any(TestFlags::A), false);
  ASSERT_EQ(bv1.any(TestFlags::B), false);
  ASSERT_EQ(bv1.any(TestFlags::C), false);

  TestBitvector bv2{TestFlags::B};
  ASSERT_EQ(bv2.any(TestFlags::A), false);
  ASSERT_EQ(bv2.any(TestFlags::B), true);
  ASSERT_EQ(bv2.any(TestFlags::C), false);

  TestBitvector bv3{TestFlags::B};
  ASSERT_EQ(bv3.any(TestFlags::A), false);
  ASSERT_EQ(bv3.any(TestFlags::B), true);
  ASSERT_EQ(bv3.any(TestFlags::C), false);
}

TEST(Bitvector, FlagsOperators) 
{
  TestBitvector bv1{TestFlags::A | TestFlags::B};
  
  ASSERT_EQ(bv1.any(TestFlags::A | TestFlags::B | TestFlags::C), true);
  ASSERT_EQ(bv1.all(TestFlags::A | TestFlags::B | TestFlags::C), false);
  ASSERT_EQ(bv1.none(TestFlags::C), true);
  ASSERT_EQ(bv1.none(TestFlags::C | TestFlags::A), false);
}

TEST(Bitvector, BitvectorOperators)
{
  TestBitvector bv1{TestFlags::A};
  TestBitvector bv2{TestFlags::B};

  ASSERT_EQ((bv1 | bv2).any(TestFlags::A | TestFlags::B | TestFlags::C), true);
  ASSERT_EQ((bv1 | bv2).all(TestFlags::A | TestFlags::B | TestFlags::C), false);

  ASSERT_EQ((bv1 & bv1).any(TestFlags::A | TestFlags::B | TestFlags::C), true);
  ASSERT_EQ((bv1 & bv1).all(TestFlags::A | TestFlags::B), false);
  ASSERT_EQ((bv1 & bv1).all(TestFlags::A | TestFlags::B | TestFlags::C), false);

  ASSERT_EQ((bv1 ^ bv1).any(TestFlags::A | TestFlags::B | TestFlags::C), false);
  ASSERT_EQ((bv1 ^ bv1).all(TestFlags::A | TestFlags::B), false);
  ASSERT_EQ((bv1 ^ bv1).all(TestFlags::A | TestFlags::B | TestFlags::C), false);
  ASSERT_EQ((bv1 ^ bv1).none(TestFlags::A | TestFlags::B | TestFlags::C), true);
  ASSERT_EQ((bv1 ^ bv1).none(), true);
}

TEST(Bitvector, Set)
{
  TestBitvector bv1;
  bv1.set(TestFlags::A | TestFlags::C);
  ASSERT_EQ(bv1.any(TestFlags::A), true);
  ASSERT_EQ(bv1.any(TestFlags::C), true);
  ASSERT_EQ(bv1.any(TestFlags::B), false);
}

TEST(Bitvector, Equality)
{
  TestBitvector bv1{TestFlags::A | TestFlags::B};
  TestBitvector bv2{TestFlags::B};
  TestBitvector bv3{TestFlags::B};

  ASSERT_EQ(bv1 == bv2, false);
  ASSERT_EQ(bv1 == bv3, false);
  ASSERT_EQ(bv3 == bv2, true);

  bv3.set(TestFlags::C);
  ASSERT_EQ(bv1 == bv3, false);

  bv2.set(TestFlags::A);
  ASSERT_EQ(bv1 == bv2, true);
}

TEST(Bitvector, SerDes)
{
  TestBitvector::setupSerDesVersion(0);
  shrinkwrap::xz::ostream os("test_bitvector.bin");
  
  TestBitvector write_test;
  write_test.set(TestFlags::C | TestFlags::B);
  serialize(os, write_test);
  os.flush();

  shrinkwrap::xz::istream is_stream("test_bitvector.bin");
  ThreadedLZMAInputStreamer is(is_stream);

  TestBitvector read_test;
  deserialize(is, read_test);

  ASSERT_EQ(read_test == write_test, true);
  remove("test_bitvector.bin");
}

}