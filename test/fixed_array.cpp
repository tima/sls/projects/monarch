/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <gtest/gtest.h>

#include <algorithm>

#include <util/fixed_array.h>

using namespace Monarch;

namespace 
{

struct DummyEntry
{
    int _a;
    int _b;

    DummyEntry(int a, int b)
    : _a(a)
    , _b(b)
    {}

    DummyEntry()
    : _a(42)
    , _b(29)
    {}

    bool operator==(const DummyEntry& rhs) const
    {
        return _a == rhs._a && _b == rhs._b;
    }
};


TEST(FixedArray, PrimitiveType)
{
  FixedArray<int> array_default{16};
  FixedArray<int> array_init{16, 42};

  ASSERT_EQ(array_default.size(), (size_t) 16);
  ASSERT_EQ(array_init.size(), (size_t) 16);

  for(size_t i = 0; i < array_init.size(); i++)
  {
    ASSERT_EQ(array_default[i], 0);
    ASSERT_EQ(array_init[i], 42);
  }

  std::fill(array_default.begin(), array_default.end(), 42);

  for(size_t i = 0; i < array_init.size(); i++)
  {
    ASSERT_EQ(array_default[i], array_init[i]);
    ASSERT_EQ(array_default[i], 42);
  }
}

TEST(FixedArray, PODType)
{
  FixedArray<DummyEntry> array_default{16};
  FixedArray<DummyEntry> array_init{16, 12, 24};


  ASSERT_EQ(array_default.size(), (size_t) 16);
  ASSERT_EQ(array_init.size(), (size_t) 16);

  for(size_t i = 0; i < array_init.size(); i++)
  {
    ASSERT_EQ(array_init[i]._a, 12);
    ASSERT_EQ(array_init[i]._b, 24);
    ASSERT_EQ(array_default[i]._a, 42);
    ASSERT_EQ(array_default[i]._b, 29);
  }

  std::fill(array_default.begin(), array_default.end(), DummyEntry{12, 24});

  for(size_t i = 0; i < array_init.size(); i++)
  {
    ASSERT_EQ(array_default[i], array_init[i]);
    ASSERT_EQ(array_default[i]._a, 12);
    ASSERT_EQ(array_default[i]._b, 24);
  }
}

}