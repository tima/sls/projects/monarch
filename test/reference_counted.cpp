/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <gtest/gtest.h>

#include <util/reference_counted.h>

namespace
{

TEST(RefCounted, Simple)
{
  ReferenceCounted<int> rc(1);
  ASSERT_EQ(*rc, 1);
  ASSERT_EQ(*rc.m_count, (size_t) 1);
}

TEST(RefCounted, Nullptr)
{
  ReferenceCounted<int> rc(nullptr);
  ASSERT_EQ(rc.m_ptr, nullptr);
  ASSERT_EQ(rc.m_count, nullptr);
  rc = nullptr;
  ASSERT_EQ(rc.m_ptr, nullptr);
  ASSERT_EQ(rc.m_count, nullptr);
}

TEST(RefCounted, Boolean)
{
  ReferenceCounted<int> rc(1);
  ASSERT_EQ((bool) rc, true);

  ReferenceCounted<int> rd(nullptr);
  ASSERT_EQ((bool) rd, false);
}

TEST(RefCounted, POD)
{
  struct PODType
  {
    uint64_t m_data;
    uint64_t*  m_ptr;

    PODType()= default;

    PODType(uint64_t data)
    : m_data(data)
    , m_ptr(&m_data)
    {}
  };

  ReferenceCounted<PODType> rc(1);
  ASSERT_EQ(rc->m_data, 1lu);
  ASSERT_EQ(rc->m_ptr, &rc->m_data);
  ASSERT_EQ(*rc.m_count, 1lu);
}

TEST(RefCounted, SimpleCopyCtor)
{
  ReferenceCounted<int> rc(1);
  ASSERT_EQ(*rc, 1);
  ASSERT_EQ(*rc.m_count, 1lu);

  ReferenceCounted<int> rd(rc);
  ASSERT_EQ(*rc, 1);
  ASSERT_EQ(*rc.m_count, 2lu);
  ASSERT_EQ(*rd, 1);
  ASSERT_EQ(*rd.m_count, 2lu);
}

TEST(RefCounted, SimpleCopyAssign)
{
  ReferenceCounted<int> rc(1);
  ASSERT_EQ(*rc, 1);
  ASSERT_EQ(*rc.m_count, 1lu);

  ReferenceCounted<int> rd(12);
  rd = rc;
  ASSERT_EQ(*rc, 1);
  ASSERT_EQ(*rc.m_count, 2lu);
  ASSERT_EQ(*rd, 1);
  ASSERT_EQ(*rd.m_count, 2lu);
}

TEST(RefCounted, SimpleMoveCtor)
{
  ReferenceCounted<int> rc(1);
  ASSERT_EQ(*rc, 1);
  ASSERT_EQ(*rc.m_count, 1lu);

  ReferenceCounted<int> rd(std::move(rc));
  ASSERT_EQ(rc.m_ptr, nullptr);
  ASSERT_EQ(rc.m_count, nullptr);
  ASSERT_EQ(*rd, 1);
  ASSERT_EQ(*rd.m_count, 1lu);
}

TEST(RefCounted, SimpleMoveAssign)
{
  ReferenceCounted<int> rc(1);
  ASSERT_EQ(*rc, 1);
  ASSERT_EQ(*rc.m_count, 1lu);

  ReferenceCounted<int> rd = std::move(rc);
  ASSERT_EQ(rc.m_ptr, nullptr);
  ASSERT_EQ(rc.m_count, nullptr);
  ASSERT_EQ(*rd, 1);
  ASSERT_EQ(*rd.m_count, 1lu);
}

TEST(RefCounted, Equality)
{
  ReferenceCounted<int> rc(1);
  ReferenceCounted<int> rd(rc);

  ASSERT_EQ(rc, rd);
  ASSERT_NE(rc, nullptr);
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wself-assign-overloaded"
TEST(RefCounted, AutoAssign)
{
  ReferenceCounted<int> rc(1);
  rc = rc;
  ASSERT_EQ(*rc.m_count, 1lu);
  ASSERT_EQ(*rc, 1);
}
#pragma clang diagnostic pop

TEST(RefCounted, Liveness)
{
  ReferenceCounted<int> rc(1);
  {
    ReferenceCounted<int> rd(rc);
  }
  ASSERT_NE(rc.m_ptr, nullptr);
  ASSERT_NE(rc.m_count, nullptr);
  ASSERT_EQ(*rc, 1);
  ASSERT_EQ(*rc.m_count, 1lu);
}

TEST(RefCounted, Update)
{
  ReferenceCounted<int> rc(1);
  {
    ReferenceCounted<int> rd(rc);
    *rd = 12;
  }
  ASSERT_NE(rc.m_ptr, nullptr);
  ASSERT_NE(rc.m_count, nullptr);
  ASSERT_EQ(*rc, 12);
  ASSERT_EQ(*rc.m_count, 1lu);
}

TEST(RefCounted, Container)
{
  std::vector<ReferenceCounted<int>> vec;
  for(int i = 0; i < 50; i++)
  {
    vec.emplace_back(42);
  }

  for(auto & elt : vec)
  {
    elt = vec[0];
  }

  ASSERT_EQ(*vec[0].m_count, 50lu);
  vec.pop_back();
  ASSERT_EQ(*vec[0].m_count, 49lu);
  vec.emplace_back(12);
  ASSERT_EQ(*vec[0].m_count, 49lu);
  ASSERT_EQ(*vec[0], 42);
  ASSERT_EQ(*vec[49], 12);
}

}