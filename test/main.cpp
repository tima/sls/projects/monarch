#include <gtest/gtest.h>

#include <util/serdes.h>

int main(int argc, char **argv) {
    // Do that somewhere else
    Monarch::SerDesBase::setupSerDesVersion(0);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
