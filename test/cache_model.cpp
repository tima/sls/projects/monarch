/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <gtest/gtest.h>

#include "../src/cache_model.cpp"

using namespace Monarch;

namespace
{

using DummyEntry = BaseCacheEntry<uint64_t, uint64_t, BaseContentType, SetStats>;


TEST(Set, RandomReplInit)
{
  StatsContainer container;
  Set<DummyEntry> set(8, ReplPolicy::Random, NameGenerator("set"), &container);
  ASSERT_EQ(0, set.numValidWays());
  ASSERT_EQ(8, set.numWays());
}

TEST(Set, LRUReplInit)
{
  StatsContainer container;
  Set<DummyEntry> set(8, ReplPolicy::LRU, NameGenerator("set"), &container);
  ASSERT_EQ(0, set.numValidWays());
  ASSERT_EQ(8, set.numWays());
}

TEST(Set, RandomReplFill)
{
  StatsContainer container;
  Set<DummyEntry> set(8, ReplPolicy::Random, NameGenerator("set"), &container);
  while(set.numValidWays() < set.numWays())
  {
    int way = set.getVictimWay(DummyEntry::ContentType::Data);
    set.insert({static_cast<uint64_t>(set.numValidWays()), static_cast<uint64_t>(set.numValidWays()), DummyEntry::ContentType::Data}, way);
  }
  ASSERT_EQ(set.numWays(), set.numValidWays());

  for(DummyEntry::TagType addr = 0; addr < static_cast<DummyEntry::TagType>(set.numWays()); addr++)
  {
    auto [entry, way] = set.search(addr, DummyEntry::ContentType::Data);
    ASSERT_NE(entry, nullptr);
    ASSERT_NE(way, -1);
    ASSERT_EQ(entry->getTag(), addr);
  }
}

TEST(Set, Invalidate)
{
  StatsContainer container;
  Set<DummyEntry> set(8, ReplPolicy::Random, NameGenerator("set"), &container);
  set.insert({static_cast<uint64_t>(0), static_cast<uint64_t>(0), DummyEntry::ContentType::Data}, 0);
  ASSERT_EQ(set.numValidWays(), 1);
  set.invalidate(0);
  ASSERT_EQ(set.numValidWays(), 0);
}

TEST(Set, LRUReplFill)
{
  StatsContainer container;
  Set<DummyEntry> set(8, ReplPolicy::LRU, NameGenerator("set"), &container);
  while(set.numValidWays() < set.numWays())
  {
    int way = set.getVictimWay(DummyEntry::ContentType::Data);
    set.insert({static_cast<uint64_t>(set.numValidWays()), static_cast<uint64_t>(set.numValidWays()), DummyEntry::ContentType::Data}, way);
    set.updateOnInsert(way, {});
  }
  ASSERT_EQ(set.numWays(), set.numValidWays());

  for(DummyEntry::TagType addr = 0; addr < static_cast<DummyEntry::TagType>(set.numWays()); addr++)
  {
    auto [entry, way] = set.search(addr, DummyEntry::ContentType::Data);
    ASSERT_NE(entry, nullptr);
    ASSERT_NE(way, -1);
    ASSERT_EQ(entry->getTag(), addr);
  }

  for(DummyEntry::TagType addr = 0; addr < static_cast<DummyEntry::TagType>(set.numWays()); addr++)
  {
    int way = set.getVictimWay(DummyEntry::ContentType::Data);
    ASSERT_EQ(set.getWay(way)->getTag(), addr);
    set.insert({static_cast<uint64_t>(addr + 8), static_cast<uint64_t>(addr + 8), DummyEntry::ContentType::Data}, way);
    set.updateOnInsert(way, {});
  }

  for(DummyEntry::TagType addr = 0; addr < static_cast<DummyEntry::TagType>(set.numWays()); addr++)
  {
    auto [entry_old, way_old] = set.search(addr, DummyEntry::ContentType::Data);
    ASSERT_EQ(entry_old, nullptr);
    ASSERT_EQ(way_old, -1);

    auto [entry, way] = set.search(addr + 8, DummyEntry::ContentType::Data);
    ASSERT_NE(entry, nullptr);
    ASSERT_NE(way, -1);
    ASSERT_EQ(entry->getTag(), addr + 8);
  }
}

TEST(SetAssocStructure, getSet)
{
  StatsContainer container;
  SetAssocStructure<DummyEntry> cache {std::string("cache"), &container, 2, 4, 2, ReplPolicy::LRU};
  DummyEntry::TagType addr = 0x0;
  ASSERT_EQ(cache.getSet(addr), 0);
  ASSERT_EQ(cache.getSet(addr + 1), 0);
  ASSERT_EQ(cache.getSet(addr + 2), 1);
  ASSERT_EQ(cache.getSet(addr + 3), 1);
  ASSERT_EQ(cache.getSet(addr + 4), 2);
  ASSERT_EQ(cache.getSet(addr + 5), 2);
  ASSERT_EQ(cache.getSet(addr + 6), 3);
  ASSERT_EQ(cache.getSet(addr + 7), 3);
  ASSERT_EQ(cache.getSet(addr + 8), 0);
  ASSERT_EQ(cache.getSet(addr + 9), 0);
  ASSERT_EQ(cache.getSet(0x42), cache.getSet(0x62));
}

TEST(SetAssocStructure, getTag)
{
  StatsContainer container;
  SetAssocStructure<DummyEntry> cache {std::string("cache"), &container, 2, 4, 2, ReplPolicy::LRU};
  ASSERT_EQ(cache.getTag(0x8), 0x1);
  ASSERT_EQ(cache.getTag(0x9), 0x1);
  ASSERT_EQ(cache.getTag(0xa), 0x1);
  ASSERT_EQ(cache.getTag(0xb), 0x1);
  ASSERT_EQ(cache.getTag(0xc), 0x1);
  ASSERT_EQ(cache.getTag(0xd), 0x1);
  ASSERT_EQ(cache.getTag(0xe), 0x1);
  ASSERT_EQ(cache.getTag(0xf), 0x1);
  ASSERT_EQ(cache.getTag(0x10), 0x2);
  ASSERT_EQ(cache.getTag(0x17), 0x2);
}

TEST(SetAssocStructure, search)
{
  StatsContainer container;
  SetAssocStructure<DummyEntry> cache {std::string("cache"), &container, 2, 4, 2, ReplPolicy::LRU};
  {
    auto [entry, set, way] = cache.search(0x42, DummyEntry::ContentType::Data);
    ASSERT_EQ(entry, nullptr);
    ASSERT_EQ(set, 0x1);
    ASSERT_EQ(way, -1);
  }
  {
    DummyEntry entry(cache.getTag(0x42), 0x42, DummyEntry::ContentType::Data);
    cache.insert(0x42, entry);
  }
  {
    auto [entry, set, way] = cache.search(0x42, DummyEntry::ContentType::Data);
    ASSERT_NE(entry, nullptr);
    ASSERT_EQ(set, 0x1);
    ASSERT_EQ(way, 0);
  }

}

TEST(MonitoredSet, LRUReplInit)
{
  StatsContainer container;
  Set<DummyEntry> set(8, ReplPolicy::LRU, NameGenerator("set"), &container, true);
  ASSERT_EQ(0, set.numValidWays());
  ASSERT_EQ(8, set.numWays());
}

TEST(MonitoredSet, InsertAndSearch)
{
  StatsContainer container;
  Set<DummyEntry> set(8, ReplPolicy::LRU, NameGenerator("set"), &container, true);
  ASSERT_EQ(0, set.numValidWays());
  ASSERT_EQ(8, set.numWays());

  {
    auto [entry, way] = set.search(0x42, DummyEntry::ContentType::Data);
    ASSERT_EQ(set.getStats().total_accesses.evaluate(), 1.0);
    ASSERT_EQ(set.getStats().misses.evaluate(), 1.0);
    ASSERT_EQ(set.getStats().hits.evaluate(), 0.0);
  }

  set.insert({0x42, 0x42, DummyEntry::ContentType::Data}, set.getVictimWay(DummyEntry::ContentType::Data));

  {
    auto [entry, way] = set.search(0x42, DummyEntry::ContentType::Data);
    ASSERT_EQ(set.getStats().total_accesses.evaluate(), 2.0);
    ASSERT_EQ(set.getStats().misses.evaluate(), 1.0);
    ASSERT_EQ(set.getStats().hits.evaluate(), 1.0);
  }

}

TEST(SetAssocStructure, insert)
{
  StatsContainer container;
  SetAssocStructure<DummyEntry> cache {std::string("cache"), &container, 2, 4, 2, ReplPolicy::LRU};
  {
    DummyEntry new_entry(cache.getTag(0x42), 0x42, DummyEntry::ContentType::Data);
    auto [entry,set, way] = cache.insert(0x42, new_entry);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x42, DummyEntry::ContentType::Data)), nullptr);
  }

  {
    DummyEntry new_entry(cache.getTag(0x62), 0x62, DummyEntry::ContentType::Data);
    auto [entry,set, way] = cache.insert(0x62, new_entry);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x42, DummyEntry::ContentType::Data)), nullptr);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x62, DummyEntry::ContentType::Data)), nullptr);
  }
  
  {
    DummyEntry new_entry(cache.getTag(0x82), 0x82, DummyEntry::ContentType::Data);
    auto [entry, set, way] = cache.insert(0x82, new_entry);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x82, DummyEntry::ContentType::Data)), nullptr);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x62, DummyEntry::ContentType::Data)), nullptr);
    ASSERT_EQ(std::get<DummyEntry*>(cache.search(0x42, DummyEntry::ContentType::Data)), nullptr);
  }
}

TEST(SetAssocStructure, invalidate)
{
  StatsContainer container;
  SetAssocStructure<DummyEntry> cache {std::string("cache"), &container, 2, 4, 2, ReplPolicy::LRU};
  DummyEntry entry(cache.getTag(0x42), 0x42, DummyEntry::ContentType::Data);
  cache.insert(0x42, entry);
  ASSERT_NE(std::get<DummyEntry*>(cache.search(0x42, DummyEntry::ContentType::Data)), nullptr);
  
  cache.invalidate(0x42);
  ASSERT_EQ(std::get<DummyEntry*>(cache.search(0x42, DummyEntry::ContentType::Data)), nullptr);
}

TEST(SetAssocStructure, updateRepl)
{
  StatsContainer container;
  SetAssocStructure<DummyEntry> cache {std::string("cache"), &container, 2, 4, 2, ReplPolicy::LRU};
  {
    DummyEntry entry(cache.getTag(0x42), 0x42, DummyEntry::ContentType::Data);
    cache.insert(0x42, entry);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x42, DummyEntry::ContentType::Data)), nullptr);
    DummyEntry entry2(cache.getTag(0x62), 0x62, DummyEntry::ContentType::Data);
    cache.insert(0x62, entry2);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x62, DummyEntry::ContentType::Data)), nullptr);
  }

  {
    DummyEntry entry(cache.getTag(0x52), 0x52, DummyEntry::ContentType::Data);
    cache.insert(0x52, entry);
    ASSERT_EQ(std::get<DummyEntry*>(cache.search(0x42, DummyEntry::ContentType::Data)), nullptr);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x62, DummyEntry::ContentType::Data)), nullptr);
    DummyEntry entry2(cache.getTag(0x72), 0x72, DummyEntry::ContentType::Data);
    auto [e, set, way] = cache.insert(0x72, entry2);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x62, DummyEntry::ContentType::Data)), nullptr);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x72, DummyEntry::ContentType::Data)), nullptr);
  }

  {
    DummyEntry entry(cache.getTag(0x82), 0x82, DummyEntry::ContentType::Data);
    cache.insert(0x82, entry);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x82, DummyEntry::ContentType::Data)), nullptr);
    ASSERT_NE(std::get<DummyEntry*>(cache.search(0x72, DummyEntry::ContentType::Data)), nullptr);
  }
}


}