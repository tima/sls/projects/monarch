# Ƹ̵̡Ӝ̵̨̄Ʒ - Monarch Toolbox

The Monarch framework provides a set of analysis _tools_ to analyze execution traces. It is therefore an architectural-level framework, and typical analysis may include instruction mix, memory footprint, branch prediction behavior, etc...

**Disclaimer** This project is quite over-engineered and code organization may not always make sense. Any feedback is welcome.

# Prerequisites

- Monarch uses C++17 facilities and requires a compiler that supports it (e.g., clang11). On Debian-based distributions :

```
sudo apt install clang-11
```
- Monarch relies on the `build-essential`, `lzma-dev` and `zlib1g-dev` packages:
```
sudo apt-get install build-essential
sudo apt-get install lzma-dev
sudo apt-get install zlib1g-dev
``` 
- Monarch relies on the `tqdm`, `pandas`, `pandas_bokeh` and 'selenium' packages:
```
pip install tqdm
pip install pandas
pip install pandas-bokeh
pip install selenium
```
- Monarch relies on pybind11 and googleTest, which are embedded as a submodules. The following command **must be run the first time the repository is cloned**.
```
git submodule init && git submodule update
``` 

# Code Organization

- `./config` contains an example config file (JSON) that specifies which _tools_ should be used.
- `./python` contains python scripts to run Monarch and process/plot results
- `./include/tools` contains C++ classes implementing the analysis tools. **This is where new tools are added**
- `./include/stats` contains utility C++ classes relating to stat collection during analysis
- `./include/trace` contains C++ classes related to creating/reading traces (including driving an analysis)
- `./include/util` contains C++ utility classes
- `./src` contains the required source files, notably the source exposing most C++ classes to Python through `pybind11`. **This is where new tools are exposed to Python**
- `./test` contains test source files (GoogleTest framework)

Most of the C++ functionalities are usable from Python through the `Monarch` package. 

The Monarch module has the following hierarchy:
- `Monarch` : Root of the module, contains the `Driver` abstraction which is now redundant with `MonarchRun` but still exists
- `Monarch.Tools` : Contains the different analysis tools available
- `Monarch.Tracing` : Contains the trace reader and architecture-independent `Instruction` representation
- `Monarch.Stats` : Contains the utility stats classes

Other python files also provide the following objects :
- `MonarchExperiment` : Represents an experiment that can be run, plotted, etc
- `MonarchRun` : Represents a run (one trace) that can be...run

Ways to use those classes are shown in `python/example.py` and the examples below.

# Usage

## Compiling

```
# First time only
git submodule init && git submodule update
```

```
setenv CXX clang++-11 # Or available C++17 or above compiler OR export CXX=clang++-11 on standard shell
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=RelWithDebugInfo .. 
make -j 8
```

## Running

### From Scratch 

Experiments are launched via Python, with the following flow :
1. Specify which tools should be run with a JSON config file (such as `./config/l1d_example_config.json`)
2. Create a script that will
   - Create a `MonarchExperiment` object and tell it where on disk it should go
   - Tell it where the traces are
   - Tell it which tools are needed
   - Serialize to disk (`MonarchExperiment.serialize()`)
   - Launch the runs (batch, serial, single)
   - Refresh from disk (`MonarchExperiment.refresh()`)
   - Plot the results

`python/example.py` performs all substeps of Step 2. Arguably, the directory containing the traces could be specified in the JSON config file (TODO).

### Reloading an Experiment from Disk

After running, all results are written to disk in JSON format. Therefore, you can generate plots from an existing study on disk, by first loading it from disk :

```
from structs import MonarchExperiment
exp = MonarchExperiments.load("/path/to/study")
exp.generatePlots()
```
Note that if the experiment was serialized to disk but never run, you will have to run it if you want to plot the results.

# Writing an Analysis Tool

To write a tool, create a new header file in `./include/tools`. A tool must implement the `AbstractProfiler` interface class in `./include/tools/interfaces.h`.
Monarch provides facilities for registering statistics that are automatically dumped to JSON after the run. This allows the python post-processing API to easily consolidate and plot those statistics. A simple example tool is the `MIPSProfiler` tool, which can serve as a base for your own tool.

To make the tool available to python, the C++ class needs to be exposed to Python using pybind11. This is done in `src/module_monarch.cpp`. See for instance exmaple that exposes the `MIPSProfiler` tool.

# Debugging 

To debug, the python interpreter can be run under `gdb` to analyze and fix any issue within the C++ code.

