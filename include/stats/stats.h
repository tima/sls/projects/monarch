/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <inttypes.h>
#include <memory>
#include <algorithm>

#include <pybind11/pybind11.h>

namespace py = pybind11;

struct NameGenerator
{
  NameGenerator(const std::string& seed)
  : count(0)
  , root_name(seed)
  {}

  std::string operator()() const
  {
    return root_name + std::to_string(count++);
  }

  mutable uint64_t count;
  std::string root_name;
};

struct Stat
{
  const std::string m_name;
  const bool m_dump;

  Stat() 
  : m_name("Invalid")
  , m_dump(false)
  {}

  Stat(const std::string & name, bool dump = true)
  : m_name(name)
  , m_dump(dump)
  {}

  const std::string & name() const
  {
    // Should never call name() on invalid stat
    assert(m_name != "Invalid");
    return m_name;
  }

  bool mustDump() const
  {
    return m_dump;
  }

  virtual ~Stat() = default;

  virtual double evaluate() const = 0;

  virtual void reset() = 0;

  virtual py::dict dump() const = 0;
};

struct Operator
{
  enum class Op 
  {
    Plus,
    Minus,
    Multiply,
    Divide,
    Modulo,
    None
  };

  Op m_op = Op::None;

  Operator(Op op)
  : m_op(op)
  {}

  ~Operator() = default;

  double evaluate(Stat * left, Stat * right)
  {
    switch(m_op)
    {
      case Op::Plus:
        return left->evaluate() + right->evaluate();
        break;
      case Op::Minus:
        return left->evaluate() - right->evaluate();
        break;
      case Op::Multiply:
        return left->evaluate() * right->evaluate();
        break;
      case Op::Divide:
        return left->evaluate() / right->evaluate();
        break;
      case Op::Modulo:
        return (double) ((uint64_t) left->evaluate() % (uint64_t) right->evaluate());
        break;
      case Op::None:
      default:
        assert(false);
        return 0.0;
    }
  }
};

struct Counter;

struct Formula : public Stat
{
  Stat * m_left = nullptr;
  bool m_delete_m_left = false;
  Stat * m_right = nullptr;
  bool m_delete_m_right = false;
  Operator * m_op = nullptr;
  double m_leaf = 0.0;

  Formula()
  : Stat()
  {}

  Formula(const std::string & name, bool dump = true)
  : Stat(name, dump)
  {}

  Formula(Stat * left, Operator * op, Stat * right)
  : Stat()
  , m_left(left)
  , m_right(right)
  , m_op(op)
  {}

  template<typename T>
  Formula(T value)
  : Stat()
  , m_leaf((double) value)
  {}

  Formula(const Formula &) = delete;
  Formula(Formula &) = delete;

  ~Formula()
  {
    if(m_op)
    {
      delete m_op;
    }
    if(m_left && m_delete_m_left)
    {
      delete m_left;
    }
    if(m_right && m_delete_m_right)
    {
      delete m_right;
    }
  }

  Formula& operator=(Formula & rhs)
  {
    this->m_leaf = rhs.m_leaf;
    this->m_left = rhs.m_left;
    this->m_delete_m_left = rhs.m_delete_m_left;
    this->m_right = rhs.m_right;
    this->m_delete_m_right = rhs.m_delete_m_right;
    this->m_op = rhs.m_op;

    rhs.m_op = nullptr;
    rhs.m_left = nullptr;
    rhs.m_right = nullptr;

    // Keep the name
    return *this;
  }

  Formula& operator=(const Formula &) = delete;
  Formula& operator=(Formula * rhs) = delete;
  Formula& operator=(Formula &&) = delete;

  template<typename T>
  friend Formula& operator+(T lhs, Formula & rhs)
  {
    Formula * res = new Formula();
    res->m_left =  new Formula(lhs);
    res->m_delete_m_left = true;
    res->m_right = &rhs;
    res->m_op = new Operator(Operator::Op::Plus);
    return *res;
  }

  template<typename T>
  friend Formula& operator+(Formula & lhs, T rhs)
  {
    Formula * res = new Formula();
    res->m_left = &lhs;
    res->m_right = new Formula(rhs);
    res->m_delete_m_right = true;
    res->m_op = new Operator(Operator::Op::Plus);
    return *res;
  }

  Formula& operator+(Formula & rhs)
  {
    return *(new Formula(this, new Operator(Operator::Op::Plus), &rhs));
  }

  template<typename T>
  friend Formula& operator-(T lhs, Formula & rhs)
  {
    Formula * res = new Formula();
    res->m_left =  new Formula(lhs);
    res->m_delete_m_left = true;
    res->m_right = &rhs;
    res->m_op = new Operator(Operator::Op::Minus);
    return *res;
  }

  template<typename T>
  friend Formula& operator-(Formula & lhs, T rhs)
  {
    Formula * res = new Formula();
    res->m_left = &lhs;
    res->m_right = new Formula(rhs);
    res->m_delete_m_right = true;
    res->m_op = new Operator(Operator::Op::Minus);
    return *res;
  }

  Formula& operator-(Formula & rhs)
  {
    return *(new Formula(this, new Operator(Operator::Op::Minus), &rhs));
  }

  template<typename T>
  friend Formula& operator/(T lhs, Formula & rhs)
  {
    Formula * res = new Formula();
    res->m_left =  new Formula(lhs);
    res->m_delete_m_left = true;
    res->m_right = &rhs;
    res->m_op = new Operator(Operator::Op::Divide);
    return *res;
  }

  template<typename T>
  friend Formula& operator/(Formula & lhs, T rhs)
  {
    Formula * res = new Formula();
    res->m_left = &lhs;
    res->m_right = new Formula(rhs);
    res->m_delete_m_right = true;
    res->m_op = new Operator(Operator::Op::Divide);
    return *res;
  }

  Formula& operator/(Formula & rhs)
  {
    return *(new Formula(this, new Operator(Operator::Op::Divide), &rhs));
  }

  template<typename T>
  friend Formula& operator*(T lhs, Formula & rhs)
  {
    Formula * res = new Formula();
    res->m_left =  new Formula(lhs);
    res->m_delete_m_left = true;
    res->m_right = &rhs;
    res->m_op = new Operator(Operator::Op::Multiply);
    return *res;
  }

  template<typename T>
  friend Formula& operator*(Formula & lhs, T rhs)
  {
    Formula * res = new Formula();
    res->m_left = &lhs;
    res->m_right = new Formula(rhs);
    res->m_delete_m_right = true;
    res->m_op = new Operator(Operator::Op::Multiply);
    return *res;
  }

  Formula& operator*(Formula & rhs)
  {
    return *(new Formula(this, new Operator(Operator::Op::Multiply), &rhs));
  }

  template<typename T>
  friend Formula& operator%(T lhs, Formula & rhs)
  {
    Formula * res = new Formula();
    res->m_left =  new Formula(lhs);
    res->m_delete_m_left = true;
    res->m_right = &rhs;
    res->m_op = new Operator(Operator::Op::Minus);
    return *res;
  }

  template<typename T>
  friend Formula& operator%(Formula & lhs, T rhs)
  {
    Formula * res = new Formula();
    res->m_left = &lhs;
    res->m_right = new Formula(rhs);
    res->m_delete_m_right = true;
    res->m_op = new Operator(Operator::Op::Modulo);
    return *res;
  }

  Formula& operator%(Formula & rhs)
  {
    return *(new Formula(this, new Operator(Operator::Op::Modulo), &rhs));
  }

  void reset() override {}

  double evaluate() const override
  {
    const bool no_operator = m_op == nullptr;
    
    if(no_operator)
    {
      return m_leaf;
    }
    
    return m_op->evaluate(m_left, m_right);
  }

  py::dict dump() const override
  {
    py::dict sdata;
    sdata["Type"] = "Scalar";
    sdata["Value"] = evaluate();
    return sdata;
  }  
};

struct Counter : public Stat
{
private:
  std::shared_ptr<uint64_t> m_count = nullptr;

public:
  Counter()
  : Stat()
  , m_count(std::make_shared<uint64_t>(0))
  {}

  Counter(const std::string & name, bool dump = true)
  : Stat(name, dump)
  , m_count(std::make_shared<uint64_t>(0))
  {}

  template<typename T>
  Counter(T value)
  : Stat()
  , m_count(std::make_shared<uint64_t>(value))
  {}

  Counter(Counter &) = default;
  Counter(const Counter &) = default;
  Counter(Counter &&) = default;

  template<typename T>
  Counter& operator=(T val)
  {
    *m_count = val;
    return *this;
  }

  ~Counter() = default;

  friend Formula& operator+(const Counter & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Plus), new Counter(rhs));
    return *res;
  }

  template<typename T>
  friend Formula& operator+(const Counter & lhs, T rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Plus), new Counter(rhs));
    return *res;
  }

  template<typename T>
  friend Formula& operator+(T lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Plus), new Counter(rhs));
    return *res;
  }

  friend Formula& operator+(const Counter & lhs, Formula & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Plus), &rhs);  
    return *res;
  }

  friend Formula& operator+(Formula & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(&lhs, new Operator(Operator::Op::Plus), new Counter(rhs));  
    return *res;
  }

  friend Formula& operator-(const Counter & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Minus), new Counter(rhs));
    return *res;
  }

  template<typename T> 
  friend Formula& operator-(const Counter & lhs, T rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Minus), new Counter(rhs));
    return *res;
  }

  template<typename T> 
  friend Formula& operator-(T lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Minus), new Counter(rhs));
    return *res;
  }

  friend Formula& operator-(const Counter & lhs, Formula & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Minus), &rhs);  
    return *res;
  }

  friend Formula& operator-(Formula & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(&lhs, new Operator(Operator::Op::Minus), new Counter(rhs));  
    return *res;
  }

  friend Formula& operator/(const Counter & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Divide), new Counter(rhs));
    return *res;
  }

  template<typename T>
  friend Formula& operator/(const Counter & lhs, T rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Divide), new Counter(rhs));
    return *res;
  }

  template<typename T> 
  friend Formula& operator/(T lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Divide), new Counter(rhs));
    return *res;
  }

  friend Formula& operator/(const Counter & lhs, Formula & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Divide), &rhs);  
    return *res;
  }

  friend Formula& operator/(Formula & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(&lhs, new Operator(Operator::Op::Divide), new Counter(rhs));  
    return *res;
  }

  friend Formula& operator*(const Counter & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Multiply), new Counter(rhs));
    return *res;
  }

  template<typename T>
  friend Formula& operator*(const Counter & lhs, T rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Multiply), new Counter(rhs));
    return *res;
  }

  template<typename T>
  friend Formula& operator*(T lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Multiply), new Counter(rhs));
    return *res;
  }

  friend Formula& operator*(const Counter & lhs, Formula & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Multiply), &rhs);  
    return *res;
  }

  friend Formula& operator*(Formula & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(&lhs, new Operator(Operator::Op::Multiply), new Counter(rhs));  
    return *res;
  }

  friend Formula& operator%(const Counter & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Modulo), new Counter(rhs));
    return *res;
  }

  template<typename T> 
  friend Formula& operator%(const Counter & lhs, T rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Modulo), new Counter(rhs));
    return *res;
  }

  template<typename T>  
  friend Formula& operator%(T lhs, const Counter & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Modulo), new Counter(rhs));
    return *res;
  }

  friend Formula& operator%(const Counter & lhs, Formula & rhs)
  {
    Formula * res = new Formula(new Counter(lhs), new Operator(Operator::Op::Modulo), &rhs);  
    return *res;
  }

  friend Formula& operator%(Formula & lhs, const Counter & rhs)
  {
    Formula * res = new Formula(&lhs, new Operator(Operator::Op::Modulo), new Counter(rhs));  
    return *res;
  }

  Counter& operator+=(uint64_t increment)
  {
    (*m_count) += increment;
    return *this;
  }

  Counter& operator++(int)
  {
    (*m_count)++;
    return *this;
  }

  Counter& operator--(int)
  {
    (*m_count)--;
    return *this;
  }

  template<typename T>  
  operator T()
  {
    return (T) *m_count;
  }

  void reset() override
  {
    *m_count = 0;
  }

  double evaluate() const override
  {
    return *m_count;
  }

  py::dict dump() const override
  {
    py::dict sdata;
    sdata["Type"] = "Scalar";
    sdata["Value"] = evaluate();
    return sdata;
  }  
};

struct FixedBucketsHistogram : public Stat
{
  std::vector<uint64_t> m_buckets;
  uint64_t m_overflows = 0;
  uint64_t m_width;
  std::vector<std::string> m_labels;

public:
  FixedBucketsHistogram(const std::string & name, uint64_t bucket_width, uint64_t num_buckets, bool dump = true)
  : Stat(name, dump)
  , m_buckets(num_buckets, 0)
  , m_width(bucket_width)
  {
    uint64_t cur_base = 0;
    for(unsigned i = 0; i < m_buckets.size(); i++)
    {
      m_labels.push_back("[" + std::to_string(cur_base) + ":" + std::to_string(cur_base + m_width) + "[");
      cur_base += m_width;
    }
  }

  FixedBucketsHistogram(const std::string & name, uint64_t bucket_width, uint64_t num_buckets, const std::vector<std::string> & labels, bool dump = true)
  : Stat(name, dump)
  , m_buckets(num_buckets, 0)
  , m_width(bucket_width)
  , m_labels(labels)
  {
  }

  bool addElement(uint64_t item, uint64_t count = 1)
  {
    if(item >= (m_width * m_buckets.size()))
    {
      m_overflows += count;
      return false;
    }

    m_buckets.at((item / m_width)) += count;
    return true;
  }

  void reset() override
  {
    for(auto & bucket : m_buckets)
    {
      bucket = 0;
    }
    m_overflows = 0;
  }

  double evaluate() const override
  {
    // TODO : Return something useful
    return 0.0;
  }

  py::dict dump() const override
  {
    py::dict sdata;
    
    sdata["Type"] = "Histogram";
    sdata["Value"] = py::dict{};
    
    for(unsigned i = 0; i < m_buckets.size(); i++)
    {
      sdata["Value"][m_labels[i].c_str()] = m_buckets[i];
    }
    sdata["Value"]["Overflows"] = m_overflows;
    return sdata;
  }
};

struct FixedFloatBucketsHistogram : public Stat
{
  std::vector<float> m_buckets;
  uint64_t m_overflows = 0;
  uint64_t m_width;
  std::vector<std::string> m_labels;

public:
  FixedFloatBucketsHistogram(const std::string & name, uint64_t bucket_width, uint64_t num_buckets)
  : Stat(name)
  , m_buckets(num_buckets, 0)
  , m_width(bucket_width)
  {
    uint64_t cur_base = 0;
    for(unsigned i = 0; i < m_buckets.size(); i++)
    {
      m_labels.push_back("[" + std::to_string(cur_base) + ":" + std::to_string(cur_base + m_width) + "[");
      cur_base += m_width;
    }
  }

  FixedFloatBucketsHistogram(const std::string & name, uint64_t bucket_width, uint64_t num_buckets, const std::vector<std::string> & labels)
  : Stat(name)
  , m_buckets(num_buckets, 0)
  , m_width(bucket_width)
  , m_labels(labels)
  {
  }

  bool addElement(float item, float count = 1)
  {
    if(item >= (m_width * m_buckets.size()))
    {
      m_overflows += count;
      return false;
    }

    m_buckets.at((item / m_width)) += count;
    return true;
  }

  void reset() override
  {
    for(auto & bucket : m_buckets)
    {
      bucket = 0;
    }
    m_overflows = 0;
  }

  double evaluate() const override
  {
    // TODO : Return something useful
    return 0.0;
  }

  py::dict dump() const override
  {
    py::dict sdata;
    
    sdata["Type"] = "Histogram";
    sdata["Value"] = py::dict{};
    
    for(unsigned i = 0; i < m_buckets.size(); i++)
    {
      sdata["Value"][m_labels[i].c_str()] = m_buckets[i];
    }
    sdata["Value"]["Overflows"] = m_overflows;
    return sdata;
  }
};


struct ExponentialBucketsHistogram : public Stat
{

private:
  FixedBucketsHistogram m_linear;

  std::vector<uint64_t> m_buckets;
  std::vector<std::string> m_labels;
  uint64_t m_overflows = 0;

public:
  ExponentialBucketsHistogram(const std::string & name, uint64_t linear_bucket_width, uint64_t linear_num_buckets, uint64_t exp_num_buckets)
  : Stat(name)
  , m_linear("", linear_bucket_width, linear_num_buckets)
  , m_buckets(exp_num_buckets, 0)
  {

    uint64_t cur_base = linear_num_buckets * linear_bucket_width;
    for(unsigned i = 0; i < m_buckets.size(); i++)
    {
      m_labels.push_back("[" + std::to_string(cur_base) + ":" + std::to_string(cur_base + linear_bucket_width * (2 << i)) + "[");
      cur_base += (linear_bucket_width * (2 << i));
    }
  }


  bool addElement(uint64_t item, uint64_t count = 1)
  {
    if(const bool linear_space = m_linear.addElement(item, count); !linear_space)
    {
      // Do it stupid for now
      uint64_t cur_base = m_linear.m_buckets.size() * m_linear.m_width;
      for(unsigned i = 0; i < m_buckets.size(); i++)
      {
        if(item >= cur_base && item < (cur_base + (m_linear.m_width * (2 << i))))
        {
          m_buckets[i] += count;
          return true;
        }
        cur_base += (m_linear.m_width * (2 << i));
      }

      std::cout << "Adding overflow value " << item << std::endl;
      m_overflows += count;
      return false;
    }

    return true;
  }

  void reset() override
  {
    m_linear.reset();
    for(auto & bucket : m_buckets)
    {
      bucket = 0;
    }
    m_overflows = 0;
  }

  double evaluate() const override
  {
    // TODO : Return something useful
    return 0.0;
  }

  py::dict dump() const override
  {
    py::dict sdata;
    
    sdata["Type"] = "Histogram";
    sdata["Value"] = py::dict{};
    
    for(unsigned i = 0; i < m_linear.m_buckets.size(); i++)
    {
      sdata["Value"][m_linear.m_labels[i].c_str()] = m_linear.m_buckets[i];
    }
    for(unsigned i = 0; i < m_buckets.size(); i++)
    {
      sdata["Value"][m_labels[i].c_str()] = m_buckets[i];
    }
    sdata["Value"]["Overflows"] = m_overflows;
    return sdata;
  }
};

struct PeriodicFormula : public Stat
{
private:
  std::function<double()> m_sampling_function;

  std::vector<double> m_samples;

public:
  PeriodicFormula(const std::string & name, const std::function<double()> & func)
  : Stat(name)
  , m_sampling_function(func)
  {}

  void reset() override
  {
    m_samples.clear();
  }

  void sample()
  {
    m_samples.push_back(m_sampling_function());
  }

  double evaluate() const override
  {
    // Return something meaningful
    return 0.0;
  }

  py::dict dump() const override
  {
    py::dict sdata;
    
    sdata["Type"] = "Periodic";
    sdata["Value"] = py::dict{};
    
    const auto num_samples = m_samples.size();

    for(unsigned i = 0; i < num_samples; i++)
    {
      sdata["Value"][("Sample_" + std::to_string(i)).c_str()] = m_samples[i];
    }
    return sdata;
  }
};

struct StatsContainer
{
  mutable std::vector<std::unique_ptr<Stat>> m_stats;

  template<typename StatType, class... Args> 
  StatType& regStat(const std::string & name, Args... args) const
  {
    if(auto iter = std::find_if(m_stats.begin(), m_stats.end(), [&](const auto & ptr)
    {
      return ptr->name() == name;
    }); 
    iter != m_stats.end())
    {
      assert(false);
      //return dynamic_cast<StatType&>(**iter);
    }
    else
    {
      m_stats.emplace_back(std::move(std::make_unique<StatType>(name, args...)));
    }
    return dynamic_cast<StatType&>(*m_stats.back());
  }

  virtual py::dict dumpStats() const
  {
    py::dict data;
    for(const auto & stat : m_stats)
    {
      assert(stat != nullptr);
      if(stat->mustDump())
      {
        data[stat->name().c_str()] = stat->dump();
      }
    }
    return data;
  }
};