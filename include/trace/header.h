/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <iostream>

#include <util/bitvector.h>
#include <util/serdes.h>
#include <trace/instruction.h>

namespace Monarch
{

enum class TraceFeatures
{
  HasOutputValues,
  HasCorrectControlFlow,
  HasCorrectContextInfo,
  HasCorrectAddressingModes,
  HasCorrectMemSize,
  HasScatterGatherSupport,
  numFlags
};

static constexpr const char * trace_features_names[] = {
  "HasOutputValues", 
  "HasCorrectControlFlow",
  "HasCorrectContextInfo",
  "HasCorrectAddressingModes",
  "HasCorrectMemSize",
  "HasScatterGatherSupport",
  "Invalid"
};

enum class Arch
{
  X86,
  ARMv8,
  RISCV,
  Invalid
};

static constexpr const char * arch_names[] = {
  "x86",
  "ARMv8",
  "RISC-V",
  "Invalid"
};

struct TraceHeader
{
  Context m_context;
  size_t m_insts = 0;
  Bitvector<TraceFeatures> m_features;
  uint16_t m_version_number = 0;
  Arch m_arch;

  TraceHeader() = default;

  TraceHeader(int version)
  : m_context()
  , m_features()
  , m_version_number(version)
  , m_arch(Arch::Invalid)
  {
    // The header takes care of setting up the correct serializer logic
    // for all serializable classe
    SerDesBase::setupSerDesVersion(m_version_number);
  }

  TraceHeader(int version, Arch arch, const Bitvector<TraceFeatures> & features)
  : m_context()
  , m_features(features)
  , m_version_number(version)
  , m_arch(arch)
  {
     // The header takes care of setting up the correct serializer logic
    // for all serializable classe
    SerDesBase::setupSerDesVersion(m_version_number);
  }

  friend void serialize(std::ostream& output, const TraceHeader &hdr)
  {
    output.write((char*) &hdr.m_version_number, sizeof(hdr.m_version_number));
    serialize(output, hdr.m_context);
    output.write((char*) &hdr.m_insts, sizeof(hdr.m_insts));
    serialize(output, hdr.m_features);
    output.write((char*) &hdr.m_arch, sizeof(hdr.m_arch));
  }

  friend void deserialize(ThreadedLZMAInputStreamer& input, TraceHeader &hdr)
  {
    // The header takes care of setting up the correct deserializer logic
    // for all deserializable classe
    input.read((char*) &hdr.m_version_number, sizeof(hdr.m_version_number));
    SerDesBase::setupSerDesVersion(hdr.m_version_number);

    deserialize(input, hdr.m_context);
    input.read((char*) &hdr.m_insts, sizeof(hdr.m_insts));
    deserialize(input, hdr.m_features);
    input.read((char*) &hdr.m_arch, sizeof(hdr.m_arch));
  }

  friend std::ostream& operator<<(std::ostream & stream, const TraceHeader& header)
  {
    stream << "TraceHeader: v" << header.m_version_number << " ";
    stream << arch_names[static_cast<int>(header.m_arch)] << " -> ";
    stream << header.m_insts << " insts" << std::endl;
    for(int i = 0; i < static_cast<int>(TraceFeatures::numFlags); i++)
    {
      stream << trace_features_names[i] << " : ";
      stream << (header.m_features.any(static_cast<TraceFeatures>(i)) ? "Yes" : "No");
      stream << std::endl;
    }
    stream << "Starting context : " << header.m_context;
    return stream;
  }
};

}
