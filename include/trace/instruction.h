/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <iostream>
#include <cstring>
#include <cassert>
#include <optional>

#include <util/bitvector.h>
#include <util/serdes.h>
#include <util/reference_counted.h>

#include <trace/program_counter.h>
#include <trace/instruction_info.h>
#include <trace/lzma_threaded_streamer.h>

namespace Monarch
{

enum class InstructionType
{
  Arithmetic,
  Logic,
  Shift,
  Mult,
  Div,
  Transcend,
  Load,
  Store,
  Branch,
  Prefetch,
  Atomic,
  Fence,
  Integer,
  FP,
  Simd,
  SysRegRead,
  SysRegWrite,
  SysCall,
  SysRet,
  Predicated,
  FlagWriter,
  FlagReader,
  Other,
  TraceSpecial,
  numFlags
};

static constexpr const char * inst_type_names[] = {
  "Arithmetic", 
  "Logic",
  "Shift",
  "Mult",
  "Div",
  "Transcend",
  "Load",
  "Store",
  "Branch",
  "Prefetch",
  "Atomic",
  "Fence",
  "Integer",
  "FP",
  "SIMD",
  "SysRegRead",
  "SysRegWrite",
  "SysCall",
  "SysRet",
  "Predicated",
  "FlagWriter",
  "FlagReader",
  "Other",
  "TraceSpecial",
  "Invalid"
};

using StaticInstFlags = Bitvector<InstructionType>;

struct StaticInstInfo : public SerDes<StaticInstInfo>
{
  char m_disassembly [128];

  StaticInstFlags m_type;
  uint8_t m_inst_size;

  StaticRegInfo m_input_regs;
  StaticRegInfo m_output_regs;
  std::optional<StaticBranchInfo> m_br_info;
  std::optional<StaticMemoryInfo> m_mem_info;
  std::optional<StaticSysRegInfo> m_sysreg_info;
  std::optional<StaticFlagWriterInfo> m_flag_writer_info;
  std::optional<StaticFlagReaderInfo> m_flag_reader_info;
  std::optional<StaticSimdInfo> m_simd_info;
  std::optional<StaticPredicationInfo> m_predication_info;

  StaticInstInfo() = default;

  REGISTER_SERDES_INIT_CTOR(StaticInstInfo)

  static void serialize_v0(std::ostream & output, const StaticInstInfo& info)
  {
    output.write((char*) &info.m_disassembly, sizeof(info.m_disassembly));
    output.write((char*) &info.m_type, sizeof(info.m_type));
    output.write((char*) &info.m_inst_size, sizeof(info.m_inst_size));

    serialize(output, info.m_input_regs);
    serialize(output, info.m_output_regs);

    if(info.m_br_info)
    {
      assert(info.m_type.any(InstructionType::Branch));
      serialize(output, info.m_br_info.value());
    }

    if(info.m_mem_info)
    {
      assert(info.m_type.any(InstructionType::Load | InstructionType::Store | InstructionType::Prefetch));
      serialize(output, info.m_mem_info.value());
    }

    if(info.m_sysreg_info)
    {
      assert(info.m_type.any(InstructionType::SysRegRead | InstructionType::SysRegWrite));
      serialize(output, info.m_sysreg_info.value());
    }

    if(info.m_flag_writer_info)
    {
      assert(info.m_type.any(InstructionType::FlagWriter));
      serialize(output, info.m_flag_writer_info.value());
    }

    if(info.m_flag_reader_info)
    {
      assert(info.m_type.any(InstructionType::FlagReader));
      serialize(output, info.m_flag_reader_info.value());
    }

    if(info.m_simd_info)
    {
      assert(info.m_type.any(InstructionType::Simd));
      serialize(output, info.m_simd_info.value());
    }

    if(info.m_predication_info)
    {
      assert(info.m_type.any(InstructionType::Predicated));
      serialize(output, info.m_predication_info.value());
    }
  }

  static void deserialize_v0(ThreadedLZMAInputStreamer & input, StaticInstInfo& sc)
  {
    input.read((char*) &sc.m_disassembly, sizeof(sc.m_disassembly));
    input.read((char*) &sc.m_type, sizeof(sc.m_type));
    input.read((char*) &sc.m_inst_size, sizeof(sc.m_inst_size));

    deserialize(input, sc.m_input_regs);
    deserialize(input, sc.m_output_regs);

    // Now we need all the specific info
    if(sc.m_type.any(InstructionType::Branch))
    {
      // TODO : Why do we need to emplace with copies ?
      deserialize(input, sc.m_br_info.emplace());
    }

    if(sc.m_type.any(InstructionType::Load | InstructionType::Store | InstructionType::Prefetch))
    {
      deserialize(input, sc.m_mem_info.emplace());
    }

    if(sc.m_type.any(InstructionType::SysRegRead | InstructionType::SysRegWrite))
    {
      deserialize(input, sc.m_sysreg_info.emplace());
    }

    if(sc.m_type.any(InstructionType::FlagWriter))
    {
      deserialize(input, sc.m_flag_writer_info.emplace());
    }

    if(sc.m_type.any(InstructionType::FlagReader))
    {
      deserialize(input, sc.m_flag_reader_info.emplace());
    }

    if(sc.m_type.any(InstructionType::Simd))
    {
      deserialize(input, sc.m_simd_info.emplace());
    }

    if(sc.m_type.any(InstructionType::Predicated))
    {
      deserialize(input, sc.m_predication_info.emplace());
    }
  }

  bool operator==(const StaticInstInfo & info) const
  {
    bool disasm_match = strcmp(info.m_disassembly,  m_disassembly) == 0;
    bool type_matches = info.m_type == m_type;
    bool size_matches = info.m_inst_size == m_inst_size;

    if(!disasm_match || !type_matches || !size_matches)
    {
      return false;
    }

    bool regs_match = (info.m_input_regs == m_input_regs) && (info.m_output_regs == m_output_regs);

    if(!regs_match)
    {
      return false;
    }

    bool info_matches = true;

    if(info.m_br_info)
    {
      if(!m_br_info)
      {
        return false;
      }
      else
      {
        info_matches &= info.m_br_info.value() == m_br_info.value();
      }
    }

    if(info.m_mem_info)
    {
      if(!m_mem_info)
      {
        return false;
      }
      else
      {
        info_matches &= info.m_mem_info.value() == m_mem_info.value();
      }
    }

    if(info.m_sysreg_info)
    { 
      if(!m_sysreg_info)
      {
        return false;
      }
      else
      {
        info_matches &= info.m_sysreg_info.value() == m_sysreg_info.value();
      }
    }

    if(info.m_flag_writer_info)
    {
      if(!m_flag_writer_info)
      {
        return false;
      }
      else
      {
        info_matches &= info.m_flag_writer_info.value() == m_flag_writer_info.value();
      }
    }

    if(info.m_flag_reader_info)
    {
      if(!m_flag_reader_info)
      {
        return false;
      }
      else
      {
        info_matches &= info.m_flag_reader_info.value() == m_flag_reader_info.value();
      }
    }

    if(info.m_simd_info)
    {
      if(!m_simd_info)
      {
        return false;
      }
      else
      {
        info_matches &= info.m_simd_info.value() == m_simd_info.value();
      }
    }

    if(info.m_predication_info)
    {
      if(!m_predication_info)
      {
        return false;
      }
      else
      {
        info_matches &= info.m_predication_info.value() == m_predication_info.value();
      }
    }

    return info_matches;
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticInstInfo& info)
  {
    os << std::dec << "[" << std::string(info.m_disassembly) << " Flags: ";
    print(os, info.m_type, (char **) inst_type_names);
    os << " InRegs: [ " << info.m_input_regs << " ] ";
    os << " OutRegs : [" << info.m_output_regs << " ]";
    os << std::endl;

    if(info.m_br_info)
    {
      os << info.m_br_info.value() << std::endl;
    }
    if(info.m_mem_info)
    {
      os << info.m_mem_info.value() << std::endl;
    }
    if(info.m_sysreg_info)
    {
      os << info.m_sysreg_info.value() << std::endl;
    }
    if(info.m_flag_writer_info)
    {
      os << info.m_flag_writer_info.value() << std::endl;
    }
    if(info.m_flag_reader_info)
    {
      os << info.m_flag_reader_info.value() << std::endl;
    }
    if(info.m_simd_info)
    {
      os << info.m_simd_info.value() << std::endl;
    }
    if(info.m_predication_info)
    {
      os << info.m_predication_info.value() << std::endl;
    }
    return os;
  }
};

struct Instruction
{
  ProgramCounter m_pc;
  
  char const * m_disassembly;
  uint8_t m_size;

  StaticInstFlags m_type;

  DynRegInfo m_input_regs;
  DynRegInfo m_output_regs;

  std::optional<DynBranchInfo> m_br_info;
  std::optional<DynMemoryInfo> m_mem_info;
  std::optional<DynSysRegInfo> m_sysreg_info;
  std::optional<DynFlagWriterInfo> m_flag_writer_info;
  std::optional<DynFlagReaderInfo> m_flag_reader_info;
  std::optional<DynSimdInfo> m_simd_info;
  std::optional<DynPredicationInfo> m_predication_info;
  std::optional<DynTraceSpecialInfo> m_trace_info;

  Instruction()
  : m_pc{0xdeadbeef, {0 , 0}}
  , m_disassembly{"Invalid"}
  {
  }

  Instruction(ThreadedLZMAInputStreamer & input, const ProgramCounter & pc, const StaticInstInfo & info)
  : m_pc(pc)
  , m_disassembly(info.m_disassembly)
  , m_size(info.m_inst_size)
  , m_type(info.m_type)
  , m_input_regs(input, info.m_input_regs, false) // Don't have values for input
  , m_output_regs(input, info.m_output_regs)
  {
    // Now we need all the specific info
    // This will deserialize from input directly
    if(info.m_br_info)
    {
      m_br_info.emplace(input, info.m_br_info.value());
    }
    
    if(info.m_mem_info)
    {
      m_mem_info.emplace(input, info.m_mem_info.value());
    }
    
    if(info.m_sysreg_info)
    {
      m_sysreg_info.emplace(input, info.m_sysreg_info.value()); 
    }

    if(info.m_flag_writer_info)
    {
      m_flag_writer_info.emplace(input, info.m_flag_writer_info.value());
    }

    if(info.m_flag_reader_info)
    {
      m_flag_reader_info.emplace(input, info.m_flag_reader_info.value());
    }

    if(info.m_simd_info)
    {
      m_simd_info.emplace(input, info.m_simd_info.value());
    }

    if(info.m_predication_info)
    {
      m_predication_info.emplace(input, info.m_predication_info.value());
    }
  }

  // TODO : Prettier print
  friend std::ostream& operator<<(std::ostream& os, const Instruction& inst)
  {
    os << inst.m_pc << ": " << inst.m_disassembly << " - "; 
    print(os, inst.m_type, (char**) inst_type_names);
    return os;
  }

  void printInstrCVPFormat()
  {
    static constexpr const char * cInfo[] = {"aluOp", "loadOp", "stOp", "condBrOp", "uncondDirBrOp", "uncondIndBrOp", "fpOp", "slowAluOp" };

    int mType = 0;

    if(m_type.any(InstructionType::Load))
    {
      mType = 1;
    }
    else if(m_type.any(InstructionType::Store))
    {
      mType = 2;
    }
    else if(m_type.any(InstructionType::Integer))
    {
      if(m_type.any(InstructionType::Mult))
      {
        mType = 7;
      }
      if(m_type.any(InstructionType::Branch))
      {
        if(isCondBranch(*(m_br_info->m_static_info)))
        {
          mType = 3;
        }
        else if(isDirectBranch(*(m_br_info->m_static_info)))
        {
          mType = 4;
        }
        else
        {
          mType = 5;
        }
      }
    }
    else
    {
      mType = 6;
    }

    std::cout << "[PC: 0x" << std::hex << m_pc.m_pc << std::dec  << " type: "  << cInfo[mType];
    if(mType == 1 || mType == 2)
        std::cout << " ea: 0x" << std::hex << m_mem_info->m_effective_address << std::dec << " size: " << (uint64_t) m_mem_info->m_static_info->m_access_size;

      if(mType == 3 || mType == 4 || mType == 5)
        std::cout << " ( tkn:" << m_br_info->m_taken << " tar: 0x" << std::hex << m_br_info->m_target << ") ";

      std::cout << std::dec << " InRegs : { ";
      for(int i = 0; i < m_input_regs.m_static_info.m_num_regs; i++)
      {
        std::cout << (unsigned) m_input_regs.m_static_info.m_regs[i] << " ";
      }
      std::cout << " } OutRegs : { ";
      for(int i = 0, j = 0; i < m_output_regs.m_static_info.m_num_regs; i++)
      {
        if(m_output_regs.m_static_info.m_regs[i] >= 32 && m_output_regs.m_static_info.m_regs[i] != 64)
        {
          std::cout << std::dec << (unsigned) m_output_regs.m_static_info.m_regs[i] << std::hex << " (hi:" << m_output_regs.m_reg_values[j+1] << " lo:" <<  m_output_regs.m_reg_values[j] << ") ";
          j += 2;
        }
        else
        {
          std::cout << std::dec << (unsigned) m_output_regs.m_static_info.m_regs[i] << std::hex << " (" <<  m_output_regs.m_reg_values[j++] << ") ";
        }
      }
      std::cout << ") ]" << std::endl;
      std::fflush(stdout);
    }
};

bool isBranch(const Instruction& inst)
{
  return inst.m_type.any(InstructionType::Branch);
}

bool isStore(const Instruction& inst)
{
  return inst.m_type.any(InstructionType::Store);
}

bool isLoad(const Instruction& inst)
{
  return inst.m_type.any(InstructionType::Load);
}

Address getPC(const Instruction & inst)
{
  return inst.m_pc.m_pc;
}

Address getFallthroughPC(const Instruction & inst)
{
  return inst.m_pc.m_pc + inst.m_size;
}

Address getBranchTarget(const Instruction & inst)
{
  return inst.m_br_info->m_target;
}

Address getNextPC(const Instruction& inst)
{
  if(isTakenBranch(inst.m_br_info.value()))
  {
    return getBranchTarget(inst);
  }
  else
  {
    return getFallthroughPC(inst);
  }
}

ProgramCounter getFullPC(const Instruction& inst)
{
  return inst.m_pc;
}

Context getContext(const Instruction& inst)
{
  return inst.m_pc.m_context;
}

uint8_t getInstSize(const Instruction& inst)
{
  return inst.m_size;
}

const std::string getDisassembly(const Instruction& inst)
{
  return std::string{inst.m_disassembly};
}

#define REGISTER_STATIC_INFO_CONVERTER(Type, Member) \
  operator const Type&() const \
  { \
    return *(*this)->Member->m_static_info; \
  }

#define REGISTER_DYN_INFO_CONVERTER(Type, Member) \
  operator const Type&() const \
  { \
    return (*this)->Member.value(); \
  }

struct InstPointer : public ReferenceCounted<Instruction>
{
  InstPointer(std::nullptr_t)
  : ReferenceCounted<Instruction>(nullptr)
  {}

  template <class... Args>
  InstPointer(Args&&... args)
  : ReferenceCounted<Instruction>(args...)
  {}

  operator const Instruction&() const
  {
    return **this;
  }

  REGISTER_STATIC_INFO_CONVERTER(StaticBranchInfo, m_br_info)
  REGISTER_STATIC_INFO_CONVERTER(StaticMemoryInfo, m_mem_info)
  REGISTER_STATIC_INFO_CONVERTER(StaticSysRegInfo, m_sysreg_info)
  REGISTER_STATIC_INFO_CONVERTER(StaticFlagWriterInfo, m_flag_writer_info)
  REGISTER_STATIC_INFO_CONVERTER(StaticFlagReaderInfo, m_flag_reader_info)
  REGISTER_STATIC_INFO_CONVERTER(StaticSimdInfo, m_simd_info)
  REGISTER_STATIC_INFO_CONVERTER(StaticPredicationInfo, m_predication_info)

  REGISTER_DYN_INFO_CONVERTER(DynBranchInfo, m_br_info)
  REGISTER_DYN_INFO_CONVERTER(DynMemoryInfo, m_mem_info)
};

#undef REGISTER_DYN_INFO_CONVERTER
#undef REGISTER_STATIC_INFO_CONVERTER

REGISTER_SERDES_DUMMY(StaticInstInfo)

}
