/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <iostream>
#include <thread>
#include <atomic>
#include <cstring>

// TODO : Inherit from streambuf so this can be used as input stream
struct ThreadedLZMAInputStreamer
{
  static constexpr size_t BUFFSIZE = 1024 * 1024;

  // TODO : Clean this up without losing performance
  // This may not work on non x86 hardware :)

  std::array<char, BUFFSIZE> * m_buffer_1;
  bool m_buffer_1_rdy = false;
  size_t m_buffer_1_cons_bytes = 0;
  
  std::array<char, BUFFSIZE> * m_buffer_2;
  bool m_buffer_2_rdy = false;
  size_t m_buffer_2_cons_bytes = 0;

  volatile char * m_use_buffer = &(*m_buffer_1)[0];
  volatile bool* m_use_buffer_ready;
  volatile size_t * m_use_buffer_consumed_bytes = &m_buffer_1_cons_bytes;

  volatile char * m_fill_buffer = &(*m_buffer_2)[0];
  volatile bool* m_fill_buffer_ready;
  volatile size_t * m_fill_buffer_consumed_bytes = &m_buffer_2_cons_bytes;

  std::thread m_fill_thread;

  ThreadedLZMAInputStreamer(std::istream &input)
  : m_buffer_1(new std::array<char, BUFFSIZE>())
  , m_buffer_2(new std::array<char, BUFFSIZE>())
  {
    m_use_buffer_ready = &m_buffer_1_rdy;
    m_fill_buffer_ready = &m_buffer_2_rdy;
    m_buffer_1_rdy = true;

    input.read(m_buffer_1->data(), BUFFSIZE);
  
    if(input.gcount() == BUFFSIZE)
    {
      m_fill_thread = std::thread{ThreadedLZMAInputStreamer::fillBuffer, &input, &m_fill_buffer, &m_fill_buffer_ready, &m_fill_buffer_consumed_bytes};
    }
  }

  ThreadedLZMAInputStreamer(const ThreadedLZMAInputStreamer&) = delete;
  ThreadedLZMAInputStreamer(ThreadedLZMAInputStreamer&&) = delete;

  ThreadedLZMAInputStreamer& operator=(const ThreadedLZMAInputStreamer&) = delete;
  ThreadedLZMAInputStreamer& operator=(ThreadedLZMAInputStreamer&&) = delete;

  ~ThreadedLZMAInputStreamer()
  {
    if(m_fill_thread.joinable())
    {
      m_fill_thread.join();
    }

    delete m_buffer_1;
    delete m_buffer_2;
  }

  static void fillBuffer(std::istream *  input, volatile char * volatile * buf, volatile bool* volatile * buffer_rdy, volatile size_t * volatile * buf_consumed)
  {
    while(true)
    {
      if(!**buffer_rdy)
      {
        input->read((char*) *buf, BUFFSIZE);
        **buf_consumed = 0;
        **buffer_rdy = true;
        if(input->gcount() != BUFFSIZE)
        {
          return;
        }
      }
    }
  }
  
  void read(char * ptr, size_t size)
  {
    while(!(*(m_use_buffer_ready))) {}

    size_t bytes_remaining = BUFFSIZE - *m_use_buffer_consumed_bytes;
    size_t to_copy = std::min(bytes_remaining, size);
    const bool needs_more = size > bytes_remaining;
    memcpy(ptr, (char*) (m_use_buffer + *m_use_buffer_consumed_bytes), to_copy);
    *m_use_buffer_consumed_bytes = *m_use_buffer_consumed_bytes + to_copy;

    if(*m_use_buffer_consumed_bytes == BUFFSIZE)
    {
      while(!(*(m_fill_buffer_ready))) {}

      std::swap(m_use_buffer, m_fill_buffer);
      std::swap(m_use_buffer_consumed_bytes, m_fill_buffer_consumed_bytes);
      *(m_use_buffer_ready) = false;
      volatile bool * tmp = m_use_buffer_ready;
      m_use_buffer_ready = m_fill_buffer_ready;
      m_fill_buffer_ready = tmp;
    }

    if(needs_more)
    {
      read(ptr + to_copy, size - to_copy);
    }
  }
};
