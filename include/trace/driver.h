/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <vector>
#include <stdexcept>

#include <trace/instruction.h>
#include <trace/reader/trace_reader.h>

#include <tools/interfaces.h>

namespace Monarch
{

struct MonarchDriver 
{
  MonarchDriver() = default;
  ~MonarchDriver() = default;

  void registerTraceReader(const std::shared_ptr<TraceReader>& reader)
  {
    m_reader = reader;
  }

  void registerTool(const std::shared_ptr<AbstractProfiler>& tool)
  {
    m_tools.push_back(tool);
  }

  bool hasReader() const
  {
    return m_reader != nullptr;
  }

  bool hasTools() const
  {
    return !m_tools.empty();
  }

  void run()
  {
    if(!hasReader())
    {
      // TODO : Find out why this does not get correctly translated in Pybind
      throw new std::invalid_argument("No trace reader registered, set one with registerTraceReader()");
    }
    
    if(!hasTools())
    {
      // TODO : Find out why this does not get correctly translated in Pybind
      throw new std::invalid_argument("No tools registered, set at least one with registerTool()");
    }

    std::cout << m_reader->m_header << std::endl;

    InstPointer inst = nullptr;

#if 0
    for(auto & ptr : m_tools)
    {
      ptr->checkFeatureReqs(m_reader.getFeatures());
    }
#endif

    while((inst = m_reader->get_inst()) != nullptr)
    {
      for(auto & ptr : m_tools)
      {
        ptr->processInst(inst);
      }
    }

    std::cout << "Serializing stats" << std::endl;
    std::cout.flush();

    for(auto & ptr : m_tools)
    {
      ptr->serializeStats();
    }

    std::cout << "Done" << std::endl;
    std::cout.flush();
  }

private:
  std::shared_ptr<TraceReader> m_reader = nullptr;
  std::vector<std::shared_ptr<AbstractProfiler>> m_tools;
};

}
