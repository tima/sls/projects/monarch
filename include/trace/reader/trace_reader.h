/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <stdexcept>
#include <fstream>

#include <trace/instruction.h>
#include <trace/lzma_buf.h>
#include <trace/header.h>
#include <trace/lzma_threaded_streamer.h>

#include <util/bitvector.h>

namespace Monarch
{

struct BaseTraceReader
{
  BaseTraceReader() = default;

  virtual InstPointer get_inst() = 0;

  virtual ~BaseTraceReader() = default;
};

struct TraceReader : public BaseTraceReader
{
  TraceHeader m_header;
  Context m_current_context;

  size_t m_processed = 0;

  shrinkwrap::xz::istream static_input;
  shrinkwrap::xz::istream dynamic_input;
  shrinkwrap::xz::istream header_input;

  std::ifstream header_test;

  std::unordered_map<ProgramCounter, StaticInstInfo> m_static_info;

  ThreadedLZMAInputStreamer m_static_streamer;
  ThreadedLZMAInputStreamer m_dyn_streamer;
  ThreadedLZMAInputStreamer m_header_streamer;

  TraceReader(const TraceReader&) = delete;
  TraceReader(TraceReader&&) = delete;

  TraceReader& operator=(const TraceReader&) = delete;
  TraceReader& operator=(TraceReader&&) = delete;

  TraceReader(const std::string & name)
  : BaseTraceReader()
  , static_input(name + "/static_info.xz")
  , dynamic_input(name + "/dynamic_info.xz")
  , header_input(name + "/header_info.xz")
  , header_test(name + "/header_info.xz")
  , m_static_streamer(static_input)
  , m_dyn_streamer(dynamic_input)
  , m_header_streamer(header_input)
  {
    if(static_input.bad() || dynamic_input.bad())
    {
      throw std::invalid_argument("Wrong trace path");
    }

    if(!header_test.is_open())
    {
      // Assuming header is in static info
      populateTraceHeader(m_static_streamer);
    }
    else
    {
      populateTraceHeader(m_header_streamer);
    }

    populateStaticInfo(m_static_streamer);
  }

  /*
  const Bitvector<TraceFeatures>& getFeatures() const
  {
    return m_header.getFeatures();
  }
  */

  void populateTraceHeader(ThreadedLZMAInputStreamer& input)
  {
    deserialize(input, m_header);

    if(m_header.m_insts == 0)
    {
      throw new std::length_error("Trace reports 0 instructions");
    }

    m_current_context = m_header.m_context;
  }

  void populateStaticInfo(ThreadedLZMAInputStreamer& input)
  {
    size_t static_insts = 0;
    input.read((char*) &static_insts, sizeof(static_insts));

    for(size_t insts = static_insts; insts != 0; insts--)
    {
      readOneStatic(input);
    }

    assert(m_static_info.size() == static_insts);
  }

  void readOneStatic(ThreadedLZMAInputStreamer& input)
  {
    // Instruction PC
    ProgramCounter pc{};
    deserialize(input, pc);
    StaticInstInfo static_info{};
    deserialize(input, static_info);
    m_static_info.emplace(pc, static_info);
  }

  InstPointer readOneDynamic(ThreadedLZMAInputStreamer& input)
  {
    if(m_processed == m_header.m_insts)
    {
      std::cout << std::dec << "Processed " << m_processed << ", header has " << m_header.m_insts << std::endl;
      return {nullptr};
    }

    ProgramCounter pc;
    deserialize(input, pc.m_pc);
    m_processed++;

    pc.m_context = m_current_context;

    if(m_static_info.count(pc) == 0)
    {
      assert(pc.m_pc == 0xdeadbeef);

      InstPointer trace = {};
      trace->m_pc = pc;
      trace->m_trace_info.emplace(DynTraceSpecialInfo{input});

      // This is where we handle monarch-specific
      // instructions
      switch(trace->m_trace_info->m_type)
      {
        case DynTraceSpecialInfo::Type::ContextChange:
          m_current_context = trace->m_trace_info->m_new_context.value();
          m_processed--;
          break;
        default:
          assert(false);
          break;
      }

      return readOneDynamic(input);
    }

    auto & static_info = m_static_info[pc];

    if(m_processed % 1000000 == 0)
    {
      std::cout << std::dec << "Processed " << m_processed << std::endl;
    }

    return InstPointer{input, pc, static_info};
  }

  InstPointer get_inst() override
  {
    return readOneDynamic(m_dyn_streamer);
  }
};

}
