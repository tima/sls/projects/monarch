/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <iostream> 

#include <trace/reader/trace_reader.h>

namespace Monarch
{

struct DummyTraceReader : public BaseTraceReader
{
  DummyTraceReader()
  : BaseTraceReader()
  {}

  InstPointer get_inst() override
  {
    if(++m_generated_insts == m_insts_to_fake)
    {
      return nullptr;
    }

    if(m_generated_insts % 100000 == 0)
      std::cout << m_generated_insts << " instrs " << std::endl;

    InstPointer inst;
    inst->m_pc.m_pc = m_generated_insts;
    return inst;
  }

private:
  uint64_t m_insts_to_fake = 1000000000;
  uint64_t m_generated_insts = 0;
};

}