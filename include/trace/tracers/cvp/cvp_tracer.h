/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

// CVP Input Trace Format :
// Inst PC 				- 8 bytes
// Inst Type 				- 1 byte
// If load/storeInst
//   Effective Address 			- 8 bytes
//   Access Size (one reg)		- 1 byte
// If branch
//   Taken 				- 1 byte
//   If Taken
//     Target				- 8 bytes
// Num Input Regs 			- 1 byte
// Input Reg Names 			- 1 byte each
// Num Output Regs 			- 1 byte
// Output Reg Names 			- 1 byte each
// Output Reg Values
//   If INT (0 to 31) or FLAG (64) 	- 8 bytes each
//   If SIMD (32 to 63)			- 16 bytes each

#pragma once

#include <vector>
#include <cassert>
#include <algorithm>
#include <deque>
#include <sstream>

#include <trace/lzma_buf.h>
#include <trace/instruction.h>
#include <trace/header.h>
#include <trace/tracers/base_tracer.h>

#include "./gzstream.h"

namespace Monarch::Tracers
{
// INT registers are registers 0 to 31. SIMD/FP registers are registers 32 to 63. Flag register is register 64
enum Offset
{
  vecOffset = 32,
  ccOffset = 64
};

enum InstClass : uint8_t
{
  aluInstClass = 0,
  loadInstClass = 1,
  storeInstClass = 2,
  condBranchInstClass = 3,
  uncondDirectBranchInstClass = 4,
  uncondIndirectBranchInstClass = 5,
  fpInstClass = 6,
  slowAluInstClass = 7,
  undefInstClass = 8
};

struct CVPTracer : public BaseTracer
{
  struct Instr
  {
    uint64_t mPc;
    uint8_t mType; // Type is InstClass
    bool mTaken;
    bool mBaseUpdate;

    uint64_t mTarget;
    uint64_t mEffAddr;
    uint8_t mMemSize; // In bytes
    uint8_t mNumInRegs;
    std::vector<uint8_t> mInRegs;
    uint8_t mNumOutRegs;
    std::vector<uint8_t> mOutRegs;
    std::vector<uint64_t> mOutRegsValues;

    Instr()
    {
      reset();
    }

    void reset()
    {
      mPc = mTarget = 0xdeadbeef;
      mEffAddr = 0xdeadbeef;
      mMemSize = 0;
      mType = undefInstClass;
      mTaken = false;
      mBaseUpdate = false;
      mNumInRegs = mNumOutRegs = 0;
      mInRegs.clear();
      mOutRegs.clear();
      mOutRegsValues.clear();
    }

    std::string getDisasm() const
    {
      static constexpr const char * cInfo[] = {"aluOp", "loadOp", "stOp", "condBrOp", "uncondDirBrOp", "uncondIndBrOp", "fpOp", "slowAluOp" };
      std::stringstream ss;

      ss << "PC 0x" << std::hex << mPc << std::dec  << ": "  << cInfo[mType] << " InRegs: {";
      for(unsigned elt : mInRegs)
      {
        ss << elt << " ";
      }
      ss << "} OutRegs: {";
      for(unsigned elt : mOutRegs)
      {
        ss << elt << " ";
      }
      ss << "}";
      return ss.str();
    }

    void printInstr()
    {
      static constexpr const char * cInfo[] = {"aluOp", "loadOp", "stOp", "condBrOp", "uncondDirBrOp", "uncondIndBrOp", "fpOp", "slowAluOp" };

      std::cout << "[PC: 0x" << std::hex << mPc << std::dec  << " type: "  << cInfo[mType];
      if(mType == InstClass::loadInstClass || mType == InstClass::storeInstClass)
        std::cout << " ea: 0x" << std::hex << mEffAddr << std::dec << " size: " << (uint64_t) mMemSize;

      if(mType == InstClass::condBranchInstClass || mType == InstClass::uncondDirectBranchInstClass || mType == InstClass::uncondIndirectBranchInstClass)
        std::cout << " ( tkn:" << mTaken << " tar: 0x" << std::hex << mTarget << ") ";

      std::cout << std::dec << " InRegs : { ";
      for(unsigned elt : mInRegs)
      {
        std::cout << elt << " ";
      }
      std::cout << " } OutRegs : { ";
      for(unsigned i = 0, j = 0; i < mOutRegs.size(); i++)
      {
        if(mOutRegs[i] >= Offset::vecOffset && mOutRegs[i] != Offset::ccOffset)
        {
          assert(j+1 < mOutRegsValues.size());
          std::cout << std::dec << (unsigned) mOutRegs[i] << std::hex << " (hi:" << mOutRegsValues[j+1] << " lo:" << mOutRegsValues[j] << ") ";
          j += 2;
        }
        else
        {
          assert(j < mOutRegsValues.size());
          std::cout << std::dec << (unsigned) mOutRegs[i] << std::hex << " (" << mOutRegsValues[j++] << ") ";
        }
      }
      std::cout << ") ]" << std::endl;
      std::fflush(stdout);
    }
  };

  gz::igzstream * dpressed_input = nullptr;

  // Buffer to hold trace instruction information
  Instr mInstr;

  uint64_t max_cid_so_far = 0;

  Context m_current_context = {0, 0};
  Context invalid_context = {~0lu, 255};

  bool m_init_header = false;
  uint8_t last_flags = 0x0;

  // This is to fix incorrect target in indirect calls
  // jumping from x30
  std::deque<Instr> m_window;

  // Note that there is no check for trace existence, so modify to suit your needs.
  CVPTracer(const char * trace_name, const char * dir)
  : BaseTracer(dir, CVP_TRACER_CURRENT_VERSION, Arch::ARMv8, {TraceFeatures::HasOutputValues | TraceFeatures::HasCorrectControlFlow})
  {
    dpressed_input = new gz::igzstream();
    dpressed_input->open(trace_name, std::ios_base::in | std::ios_base::binary);

    if(dpressed_input->fail())
    {
      std::cerr << "CVPTracer() error: " << strerror(errno);
      exit(-1);
    }
  }

  virtual ~CVPTracer()
  {
    if(dpressed_input)
    {
      dpressed_input->close();
      delete dpressed_input;
    }
  }

  bool processOneInst() override
  {
   if(readInstr())
   {
     m_window.push_back(mInstr);
     if(m_window.size() < 2)
     {
       readInstr();
       m_window.push_back(mInstr);
     }
     populateNewInstr();
     m_window.pop_front();
   }
   // If the trace is done
   else
   {
    while(m_window.size())
    {
      populateNewInstr();
      m_window.pop_front();
    }
    return false;
   }

   return true;
  }

  StaticInstInfo fillStaticInfo(const Instr & instr)
  {
      StaticInstInfo s_info;
      StaticFlagReaderInfo f_info;
      StaticMemoryInfo mem_info;
      StaticBranchInfo br_info;
      strcpy(s_info.m_disassembly, instr.getDisasm().c_str());

      s_info.m_inst_size = 4;

      StaticRegInfo s_input;
      s_input.m_num_regs = instr.mNumInRegs;
      for(int i = 0; i < s_input.m_num_regs; i++)
      {
        s_input.m_regs[i] = instr.mInRegs[i];
        s_input.m_reg_widths[i] = (s_input.m_regs[i] >= Offset::vecOffset && s_input.m_regs[i] != Offset::ccOffset) ? 128 : 64;
      }

      s_info.m_input_regs = s_input;

      StaticRegInfo s_output;
      s_output.m_num_regs = instr.mNumOutRegs;
      for(int i = 0; i < s_output.m_num_regs; i++)
      {
        s_output.m_regs[i] = instr.mOutRegs[i];
        s_output.m_reg_widths[i] = (s_output.m_regs[i] >= Offset::vecOffset && s_output.m_regs[i] != Offset::ccOffset) ? 128 : 64;
      }

      s_info.m_output_regs = s_output;

      switch(instr.mType)
      {
        case aluInstClass:
          s_info.m_type.set(InstructionType::Integer | InstructionType::Arithmetic);

          // CSEL/CSNEG etc.
          if(auto flag_reader =
          std::find_if(s_info.m_input_regs.m_regs.begin(),
                     s_info.m_input_regs.m_regs.end(),
                     [](const auto & reg)
                     {
                       return reg == 64;
                     }); flag_reader != s_info.m_input_regs.m_regs.end())
        {
          f_info.m_input_flags = 15u; // NZCV even though not really
          s_info.m_flag_reader_info.emplace(f_info);
          s_info.m_type.set(InstructionType::FlagReader);
        }
          break;
        case loadInstClass:
          s_info.m_type.set(InstructionType::Load);


          mem_info.m_immediate = 0x0;
          mem_info.m_access_size = instr.mMemSize;
          mem_info.m_base_register = 255u;
          mem_info.m_offset_register = 255u;

          // This is incorrect since we are missing base updates.
          if(instr.mNumInRegs == 2)
            mem_info.m_mode = StaticMemoryInfo::AddressingMode::Indirect;
          else if(instr.mNumInRegs == 0)
          {
            mem_info.m_mode = StaticMemoryInfo::AddressingMode::PCRelative;
          }
          else
          {
            mem_info.m_mode = StaticMemoryInfo::AddressingMode::Immediate;
            mem_info.m_base_register = instr.mInRegs[0];
          }

          mem_info.m_type = StaticMemoryInfo::AccessType::Read;
          s_info.m_mem_info.emplace(mem_info);
          break;
        case storeInstClass:
          s_info.m_type.set(InstructionType::Store);

          mem_info.m_immediate = 0x0;
          mem_info.m_access_size = instr.mMemSize;
          mem_info.m_base_register = 255u;
          mem_info.m_offset_register = 255u;

          // This is incorrect since we are missing base updates and Indirect
          // because we can't really tell difference between STP and Store with indirect access
          mem_info.m_mode = StaticMemoryInfo::AddressingMode::Immediate;

          mem_info.m_type = StaticMemoryInfo::AccessType::Write;
          s_info.m_mem_info.emplace(mem_info);
          break;
        case condBranchInstClass:
          s_info.m_type.set(InstructionType::Integer | InstructionType::Branch | InstructionType::FlagReader);

          br_info.m_type = StaticBranchInfo::BranchType::Cond;
          br_info.m_target_type = StaticBranchInfo::BranchTargetType::Direct;
          s_info.m_br_info.emplace(br_info);
          f_info.m_input_flags = 15u; // NZCV even though not really
          s_info.m_flag_reader_info.emplace(f_info);
          break;
        case uncondDirectBranchInstClass:
          s_info.m_type.set(InstructionType::Integer | InstructionType::Branch);

          if(instr.mNumOutRegs == 1)
          br_info.m_type = StaticBranchInfo::BranchType::Call;
          else
            br_info.m_type = StaticBranchInfo::BranchType::Uncond;
          br_info.m_target_type = StaticBranchInfo::BranchTargetType::Direct;
          s_info.m_br_info.emplace(br_info);
          break;
        case uncondIndirectBranchInstClass:
          s_info.m_type.set(InstructionType::Integer | InstructionType::Branch);

          if(instr.mNumOutRegs == 1)
            br_info.m_type = StaticBranchInfo::BranchType::Call;
          else
          {
            if(instr.mInRegs[0] == 30)
              br_info.m_type = StaticBranchInfo::BranchType::Return; // Mostly correct
            else
              br_info.m_type = StaticBranchInfo::BranchType::Uncond;
          }

          br_info.m_target_type = StaticBranchInfo::BranchTargetType::Indirect;
          s_info.m_br_info.emplace(br_info);
          break;
        case fpInstClass:
          s_info.m_type.set(InstructionType::FP | InstructionType::Arithmetic);
          break;
        case slowAluInstClass:
          s_info.m_type.set(InstructionType::Integer | InstructionType::Mult);
          break;
        case undefInstClass:
        default:
          assert(false);
          break;
      }

      if(auto flag_writer =
        std::find_if(s_info.m_output_regs.m_regs.begin(), 
                     s_info.m_output_regs.m_regs.end(),
                     [](const auto & reg)
                     {
                       return reg == 64;
                     }); flag_writer != s_info.m_output_regs.m_regs.end())
      {
        StaticFlagWriterInfo f_info;
        f_info.m_output_flags = 15u; // NZCV 
        s_info.m_flag_writer_info.emplace(f_info);
        s_info.m_type.set(InstructionType::FlagWriter);
      }

      bool is_simd = std::find_if(
        s_info.m_output_regs.m_regs.begin(),
        s_info.m_output_regs.m_regs.end(),
        [](const auto & reg) -> bool
        {
          return (reg >= vecOffset) && (reg < ccOffset);
        }) != s_info.m_output_regs.m_regs.end();

      is_simd |= std::find_if(
        s_info.m_input_regs.m_regs.begin(),
        s_info.m_input_regs.m_regs.end(),
        [](const auto & reg) -> bool
        {
          return (reg >= vecOffset) && (reg < ccOffset);
        }) != s_info.m_input_regs.m_regs.end();

      if(is_simd)
      {
        s_info.m_type.set(InstructionType::Simd);
        StaticSimdInfo simd_info;
        simd_info.m_element_size = 16;
        s_info.m_simd_info.emplace(simd_info);
      }

      return s_info;
  }

  bool checkMatch(StaticInstInfo & info, const Instr & instr)
  {
    return info == fillStaticInfo(instr);
  }

  Context contextMatches(const ProgramCounter & pc, const Instr & instr)
  {
    for(uint64_t i = 0; i <= max_cid_so_far; i++)
    {
      Context c_user{i, 0};
      ProgramCounter pc_user{pc.m_pc, c_user};
      Context c_kernel{i, 1};
      ProgramCounter pc_kernel{pc.m_pc, c_kernel};

      if(m_static_info.count(pc_user) != 0)
      {
        auto & info = m_static_info[pc_user];
        if(checkMatch(info, instr))
        {
          return c_user;
        }
      }

      if(m_static_info.count(pc_kernel) != 0)
      {
        auto & info = m_static_info[pc_kernel];
        if(checkMatch(info, instr))
        {
          return c_kernel;
        }
      }
    }

    return invalid_context;
  }

  bool handleContextChange(ProgramCounter pc, const Instr & instr)
  {
    Context candidate_context = contextMatches(pc, instr);
    ProgramCounter new_pc = {0, candidate_context};

    if(candidate_context == invalid_context)
    {
      Context new_context = {++max_cid_so_far, 0};
      m_current_context = new_context;
    }
    else
    {
      m_current_context = candidate_context;
    }

    new_pc = {0xdeadbeef, m_current_context};
    serialize(*m_dyn_info_stream, new_pc.m_pc);

    DynTraceSpecialInfo t_info;
    t_info.m_type = DynTraceSpecialInfo::Type::ContextChange;
    t_info.m_new_context.emplace(m_current_context);
    serialize(*m_dyn_info_stream, t_info);

    ProgramCounter inst_pc{instr.mPc, m_current_context};

    return m_static_info.count(inst_pc) != 0;
  }

  bool populateNewInstr()
  {
    auto & instr = m_window.front();
    ProgramCounter pc{instr.mPc, m_current_context};

    if(!m_init_header)
    {
      m_header.m_context = m_current_context;
      m_init_header = true;
    }

    if(m_static_info.count(pc) != 0)
    {
      auto & info = m_static_info[pc];
      assert(info.m_type.none(InstructionType::TraceSpecial));

      // We have static information for the instruction
      // Need to check if it is the correct one
      if(!checkMatch(info, instr))
      {
        // If not, switch context
        if(!handleContextChange(pc, instr))
        {
          pc = {instr.mPc, m_current_context};
          m_static_info.emplace(pc, fillStaticInfo(instr));
        }
        else
        {
          pc = {instr.mPc, m_current_context};
        }
      }
    }
    else
    {
      if(auto ctxt = contextMatches(pc, instr); ctxt != invalid_context)
      {
        handleContextChange(pc, instr);
        pc = {instr.mPc, m_current_context};
      }
      else
      {
        m_static_info.emplace(pc, fillStaticInfo(instr));
      }
    }

    // Done with static info, now do dynamic
    StaticInstInfo & info = m_static_info[pc];
    resetInst();

    m_current_inst.m_pc = pc;
    DynRegInfo & d_output = m_current_inst.m_output_regs;

    d_output = {info.m_output_regs};
    int offset = 0;
    for(int i = 0; i < info.m_output_regs.m_num_regs; i++)
    {
      if(info.m_output_regs.m_reg_widths[i] == 64)
      {
        d_output.m_reg_values[offset] = instr.mOutRegsValues[offset];
        offset++;
      }
      else if(info.m_output_regs.m_reg_widths[i] == 128)
      {
        d_output.m_reg_values[offset] = instr.mOutRegsValues[offset];
        d_output.m_reg_values[offset+1] = instr.mOutRegsValues[offset+1];
        offset+=2;
      }
      else
      {
        assert(false);
      }
    }

    if(info.m_br_info)
    {
      m_current_inst.m_br_info.emplace(info.m_br_info.value());
      m_current_inst.m_br_info->m_taken = instr.mTaken;
      // Fixing incorrect indirect call to x30
      if(m_window.size() > 1 && instr.mNumInRegs == 1 && instr.mInRegs[0] == 30 && instr.mNumOutRegs == 1 && instr.mOutRegs[0] == 30)
      {
        m_current_inst.m_br_info->m_target = m_window.back().mPc;
      }
      else
      {
        m_current_inst.m_br_info->m_target = instr.mTarget;
      }
      //std::cout << "BranchInfo: " << br_info << std::endl;
    }

    if(info.m_mem_info)
    {
      m_current_inst.m_mem_info.emplace(info.m_mem_info.value());
      m_current_inst.m_mem_info->m_effective_address = instr.mEffAddr;
      //std::cout << "MemInfo: " << mem_info << std::endl;
    }

    if(info.m_sysreg_info)
    {
      assert(false);
    }

    if(info.m_flag_writer_info)
    {
      m_current_inst.m_flag_writer_info.emplace(info.m_flag_writer_info.value());
      DynFlagWriterInfo& f_info = m_current_inst.m_flag_writer_info.value();
      int i = 0;
      for(; i < instr.mNumOutRegs; i++)
      {
        if(instr.mOutRegs[i] == 64)
          break;
      }
      f_info.m_output_flags_values = 0;
      last_flags = instr.mOutRegsValues[i];
    }

    if(info.m_flag_reader_info)
    {
      m_current_inst.m_flag_reader_info.emplace(info.m_flag_reader_info.value());
      m_current_inst.m_flag_reader_info->m_input_flags_values = last_flags;
    }

    if(info.m_simd_info)
    {
      // Don't care
    }

    if(info.m_predication_info)
    {
      assert(false);
    }

    serializeDynInfo();

    // TODO : Decide if we need this
    /*if(m_window.size() > 1 && instr.mType == aluInstClass && instr.mTarget != m_window.back().mPc)
    {
      //std::cout << instr.mPc << std::hex << " into " << std::hex << m_window.back().mPc << std::endl;
      ProgramCounter new_pc{m_window.back().mPc, m_current_context};
      handleContextChange(new_pc, m_window.back());
    }*/


    return true;
  }

  // Read bytes from the trace and populate a buffer object.
  // Returns true if something was read from the trace, false if we the trace is over.
  bool readInstr()
  {
    // Trace Format :
    // Inst PC 				- 8 bytes
    // Inst Type			- 1 byte
    // If load/storeInst
    //   Effective Address 		- 8 bytes
    //   Access Size (one reg)		- 1 byte
    // If branch
    //   Taken 				- 1 byte
    //   If Taken
    //     Target			- 8 bytes
    // Num Input Regs 			- 1 byte
    // Input Reg Names 			- 1 byte each
    // Num Output Regs 			- 1 byte
    // Output Reg Names			- 1 byte each
    // Output Reg Values
    //   If INT (0 to 31) or FLAG (64) 	- 8 bytes each
    //   If SIMD (32 to 63)		- 16 bytes each
    mInstr.reset();
    dpressed_input->read((char*) &mInstr.mPc, sizeof(mInstr.mPc));

    if(dpressed_input->eof())
      return false;


    mInstr.mTarget = mInstr.mPc + 4;
    dpressed_input->read((char*) &mInstr.mType, sizeof(mInstr.mType));

    assert(mInstr.mType != undefInstClass);

    if(mInstr.mType == InstClass::loadInstClass || mInstr.mType == InstClass::storeInstClass)
    {
      dpressed_input->read((char*) &mInstr.mEffAddr, sizeof(mInstr.mEffAddr));
      dpressed_input->read((char*) &mInstr.mMemSize, sizeof(mInstr.mMemSize));
    }
    if(mInstr.mType == InstClass::condBranchInstClass || mInstr.mType == InstClass::uncondDirectBranchInstClass || mInstr.mType == InstClass::uncondIndirectBranchInstClass)
    {
      dpressed_input->read((char*) &mInstr.mTaken, sizeof(mInstr.mTaken));
      if(mInstr.mTaken)
        dpressed_input->read((char*) &mInstr.mTarget, sizeof(mInstr.mTarget));
    }

    dpressed_input->read((char*) &mInstr.mNumInRegs, sizeof(mInstr.mNumInRegs));

    for(auto i = 0; i != mInstr.mNumInRegs; i++)
    {
      uint8_t inReg;
      dpressed_input->read((char*) &inReg, sizeof(inReg));
      mInstr.mInRegs.push_back(inReg);
    }

    dpressed_input->read((char*) &mInstr.mNumOutRegs, sizeof(mInstr.mNumOutRegs));

    for(auto i = 0; i != mInstr.mNumOutRegs; i++)
    {
      uint8_t outReg;
      dpressed_input->read((char*) &outReg, sizeof(outReg));
      mInstr.mOutRegs.push_back(outReg);
    }

    bool vec_output = false;
    for(auto i = 0; i != mInstr.mNumOutRegs; i++)
    {
      uint64_t val;
      dpressed_input->read((char*) &val, sizeof(val));
      mInstr.mOutRegsValues.push_back(val);

      // Don't emit pieces for vector loads that only look at lo part of registers
      if(mInstr.mOutRegs[i] >= Offset::vecOffset && mInstr.mOutRegs[i] != Offset::ccOffset)
      {
        vec_output = true;
        dpressed_input->read((char*) &val, sizeof(val));
        mInstr.mOutRegsValues.push_back(val);
      }
    }

    // Rearrange output regs for vector loads such that base register is first if 
    // base update
    if(vec_output && mInstr.mNumOutRegs > 1 && mInstr.mOutRegs.back() < Offset::vecOffset)
    {
      auto tmp_reg = mInstr.mOutRegs.back();
      auto tmp_value = mInstr.mOutRegsValues.back();

      for(int i = mInstr.mOutRegs.size() - 1; i >= 0; i--)
      {
        mInstr.mOutRegs[i+1] = mInstr.mOutRegs[i];
      }
      mInstr.mOutRegs[0] = tmp_reg;

      for(int i = mInstr.mOutRegsValues.size() - 1; i >= 0; i--)
      {
         mInstr.mOutRegsValues[i+1] = mInstr.mOutRegsValues[i];
      }
      mInstr.mOutRegsValues[0] = tmp_value;
    }
 
    // Memsize has to be adjusted as it is giving only the access size for one register.
    if(mInstr.mType == InstClass::loadInstClass)
    {
      // Disregard base update output as it is not accessing memory
      mInstr.mMemSize = mInstr.mMemSize * std::max(1lu, (long unsigned) mInstr.mNumOutRegs - mInstr.mBaseUpdate);
    }

    // Trace INT instructions with 0 outputs are generally CMP, so we mark them as producing the flag register
    // The trace does not have the value of the flag register, though
    if(mInstr.mType == aluInstClass && mInstr.mOutRegs.size() == 0)
    {
      mInstr.mOutRegs.push_back(Offset::ccOffset);
      mInstr.mOutRegsValues.push_back(0xdeadbeef);
      mInstr.mNumOutRegs++;
    }
    else if(mInstr.mType == condBranchInstClass && mInstr.mInRegs.size() == 0)
    {
      mInstr.mInRegs.push_back(Offset::ccOffset);
      mInstr.mNumInRegs++;
    }

    return true;
  }
};

}
