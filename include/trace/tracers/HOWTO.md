# Tracers

Those are used to generate traces that Monarch can read

## Key Concepts

- The format can be inferred from the code of the tracers and trace reader
- Traces have a version number, and that number is used to set the correct deserialization functions in the trace reader
- Traces support (or not) specific features

### Trace Features

- `TraceFeatures::HasOutputValues` means the trace contains output register values
- `TraceFeatures::HasCorrectControlFlow` means the trace has the correct control flow (this means exceptions handling>
- `TraceFeatures::HasCorrectContextInfo` means the trace has correct PC and ASID/PCID information, otherwise you just>
- `TraceFeatures::HasCorrectAddressingModes` means the trace correctly reports load/store addressing modes
- `TraceFeatures::HasCorrectMemSize` means the trace correctly report load/store access size
- `TraceFeatures::HasScatterGatherSupport` is self-explanatory

## CVPTracer

Converting the CVP ARMv8 traces to the Monarch format.

### Using the CVP Tracer

#### Standalone

`CVPTracer input_trace output_dir`

#### Batch

`python/tracing/cvp.py` is provided to convert several traces in parallel

### Known Limitations of CVP Tracer

- Self-modifying code is handled by changing context, sometimes making it appear as if there are two ASID and a lot of context switching.
- Memory access size for loads using post/pre-increment indexing is incorrect (pessimistic)
- Addressing mode is not correctly reported for both loads and stores
