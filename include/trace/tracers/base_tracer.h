/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <unordered_map>
#include <optional>

#include <trace/header.h>
#include <trace/lzma_buf.h>
#include <trace/instruction.h>

namespace Monarch
{

static constexpr uint16_t CVP_TRACER_CURRENT_VERSION = 0;

struct BaseTracer
{
  shrinkwrap::xz::ostream * m_static_info_stream;
  shrinkwrap::xz::ostream * m_dyn_info_stream;

  std::unordered_map<ProgramCounter, StaticInstInfo> m_static_info;

  Instruction m_current_inst;
  size_t m_processed = 0;

  TraceHeader m_header;

  BaseTracer(const char * dir, int version, Arch arch, const Bitvector<TraceFeatures> & features)
  : m_static_info_stream(new shrinkwrap::xz::ostream(std::string(dir) + "/static_info.xz"))
  , m_dyn_info_stream(new shrinkwrap::xz::ostream(std::string(dir) + "/dynamic_info.xz"))
  , m_header(version, arch, features)
  {
  }

  virtual ~BaseTracer()
  {
    serializeStaticInfo();
    delete m_static_info_stream;
    delete m_dyn_info_stream;
  }

  void resetInst()
  {
    m_current_inst.m_br_info = std::nullopt;
    m_current_inst.m_mem_info = std::nullopt;
    m_current_inst.m_sysreg_info = std::nullopt;
    m_current_inst.m_flag_writer_info = std::nullopt;
    m_current_inst.m_flag_reader_info = std::nullopt;
    m_current_inst.m_simd_info = std::nullopt;
    m_current_inst.m_predication_info = std::nullopt;
    m_current_inst.m_trace_info = std::nullopt;
  }

  void serializeStaticInfo()
  {
    m_header.m_insts = m_processed;
    serialize(*m_static_info_stream, m_header);
    std::cout << "Serializing " << std::endl;
    std::cout << m_header << std::endl;
    size_t size = m_static_info.size();
    m_static_info_stream->write((char*) &size, sizeof(size_t));

    for(const auto & pair : m_static_info)
    {
      const auto & pc = pair.first;
      serialize(*m_static_info_stream, pc);
      const auto & sinfo = pair.second;
      serialize(*m_static_info_stream, sinfo);
    }
  }

  void serializeDynInfo()
  {
    serialize(*m_dyn_info_stream, getPC(m_current_inst));
    serialize(*m_dyn_info_stream, m_current_inst.m_input_regs);
    serialize(*m_dyn_info_stream, m_current_inst.m_output_regs);

    if(m_current_inst.m_br_info)
    {
      serialize(*m_dyn_info_stream, m_current_inst.m_br_info.value());
    }
    if(m_current_inst.m_mem_info)
    {
      serialize(*m_dyn_info_stream, m_current_inst.m_mem_info.value());
    }
    if(m_current_inst.m_sysreg_info)
    {
      serialize(*m_dyn_info_stream, m_current_inst.m_sysreg_info.value());
    }
    if(m_current_inst.m_flag_writer_info)
    {
      serialize(*m_dyn_info_stream, m_current_inst.m_flag_writer_info.value());
    }
    if(m_current_inst.m_flag_reader_info)
    {
      serialize(*m_dyn_info_stream, m_current_inst.m_flag_reader_info.value());
    }
    if(m_current_inst.m_simd_info)
    {
      serialize(*m_dyn_info_stream, m_current_inst.m_simd_info.value());
    }
    if(m_current_inst.m_predication_info)
    {
      serialize(*m_dyn_info_stream, m_current_inst.m_predication_info.value());
    }
    if(m_current_inst.m_trace_info)
    {
      serialize(*m_dyn_info_stream, m_current_inst.m_trace_info.value());
    }
    m_processed++;
  }

  virtual bool processOneInst() = 0;
};

}
