/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <cstring>
#include <iostream>
#include <inttypes.h>
#include <array>
#include <bitset>
#include <optional>

#include <util/serdes.h>

#include <trace/program_counter.h>
#include <trace/lzma_threaded_streamer.h>

namespace Monarch
{

constexpr uint8_t InvalidReg = 0xff;

struct __attribute__((packed)) StaticRegInfo : public SerDes<StaticRegInfo>
{
  int m_num_regs = 0;
  std::array<uint8_t, 8> m_regs = {0};
  std::array<uint16_t, 8> m_reg_widths = {0};

  size_t m_total_data_byte_widths = 0;

  StaticRegInfo() = default;

  REGISTER_SERDES_INIT_CTOR(StaticRegInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, StaticRegInfo& info)
  {
    input.read((char*) &info.m_num_regs, sizeof(info.m_num_regs));
    input.read((char*) &info.m_regs, info.m_num_regs * sizeof(uint8_t));
    input.read((char*) &info.m_reg_widths, info.m_num_regs * sizeof(uint16_t));

    for(int i = 0; i < info.m_num_regs; i++)
    {
      info.m_total_data_byte_widths += info.m_reg_widths[i] >> 3;
    }
  }

  static void serialize_v0(std::ostream& output, const StaticRegInfo& info)
  {
    output.write((char*) &info.m_num_regs, sizeof(info.m_num_regs));
    output.write((char*) &info.m_regs, info.m_num_regs * sizeof(uint8_t));
    output.write((char*) &info.m_reg_widths, info.m_num_regs * sizeof(uint16_t));
  }

  bool operator==(const StaticRegInfo & info) const
  {
    return memcmp((const void*) &m_num_regs,
      (const void*) &info.m_num_regs,
      sizeof(m_num_regs) + sizeof(m_regs) + sizeof(m_reg_widths)) == 0;
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticRegInfo& info)
  {
    os << std::dec << "[ ";
    for(int i = 0; i < info.m_num_regs; i++)
    {
      os << (int) info.m_regs[i] << "(" << info.m_reg_widths[i] << "b) ";
    }
    os << "]";
    return os;
  }
};

struct __attribute__((packed)) DynRegInfo
{
  std::array<uint64_t, 16> m_reg_values = {0};
  StaticRegInfo m_static_info;

  DynRegInfo() = default;

  DynRegInfo(const StaticRegInfo& info)
  : m_static_info(info)
  {}

  // has_value should be replaced by a reference to the trace configuration object
  DynRegInfo(ThreadedLZMAInputStreamer & input, const StaticRegInfo& info, bool has_value = true)
  : m_static_info(info)
  {
    if(has_value)
    {
      input.read((char*) &m_reg_values, info.m_total_data_byte_widths);
    }
  }

  friend void serialize(std::ostream & output, const DynRegInfo& info)
  {
    int offset = 0;
    for(int i = 0; i < info.m_static_info.m_num_regs; i++)
    {
      const int byte_width = info.m_static_info.m_reg_widths[i] >> 3;
      output.write((char*) &info.m_reg_values + offset, byte_width);
      offset += byte_width;
    }
  }

  friend std::ostream& operator<<(std::ostream& os, const DynRegInfo& info)
  {
    os << std::dec << "[ ";
    int offset = 0;
    for(int i = 0; i < info.m_static_info.m_num_regs; i++)
    {
      os << std::dec << (int) info.m_static_info.m_regs[i] << "(" << info.m_static_info.m_reg_widths[i] << "b) : 0x";
      const int lu_width = info.m_static_info.m_reg_widths[i] >> 6;
      for(int width = lu_width-1; width >= 0; width--)
      {
        os << std::hex << info.m_reg_values[offset + width];
      }
      os << " ";
      offset += lu_width;
    }
    os << "]";
    return os;
  }

  // TODO : What interface to retrieve values from analyzer ?
};

struct __attribute__((packed)) StaticMemoryInfo : public SerDes<StaticMemoryInfo>
{
  enum class AccessType
  {
    Read,
    Write,
    ReadWrite,
    Prefetch,
    Invalid
  };

  static constexpr const char * atype_info[] = {
    "Read", 
    "Write",
    "ReadWrite",
    "Prefetch",
    "Invalid"
  };

  enum class AddressingMode
  {
    Immediate,
    Indirect,
    PreIncrement,
    PostIncrement,
    ScatterGather,
    PCRelative,
    Invalid
  };

  static constexpr const char * amtype_info[] = {
    "Offset", 
    "Indirect",
    "PreIncrement",
    "PostIncrement",
    "ScatterGather",
    "PCRelative",
    "Invalid"
  };

  uint32_t m_immediate = 0x0;
  uint16_t m_access_size = 0;
  uint8_t m_base_register = InvalidReg;
  uint8_t m_offset_register = InvalidReg;
  
  AddressingMode m_mode = AddressingMode::Invalid;
  AccessType m_type = AccessType::Invalid;


  static constexpr size_t struct_size =  sizeof(m_immediate) +
      sizeof(m_access_size) +
      sizeof(m_base_register) +
      sizeof(m_offset_register) +
      sizeof(m_mode) +
      sizeof(m_type); 

  StaticMemoryInfo() = default;
  
  REGISTER_SERDES_INIT_CTOR(StaticMemoryInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, StaticMemoryInfo& info)
  {
    input.read((char*) &info.m_immediate, struct_size);
  }

  static void serialize_v0(std::ostream& output, const StaticMemoryInfo& info)
  {
    output.write((char*) &info.m_immediate, struct_size);
  }

  bool operator==(const StaticMemoryInfo & rhs) const
  {
    return memcmp((const void*) &m_immediate, (const void*) &rhs.m_immediate, sizeof(m_immediate) + sizeof(m_access_size) + sizeof(m_base_register) + sizeof(m_offset_register) + sizeof(m_mode) + sizeof(m_type)) == 0;
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticMemoryInfo & info)
  {
    os << std::dec << "[" << atype_info[static_cast<int>(info.m_type)] << " AMode: " << amtype_info[static_cast<int>(info.m_mode)];
    os << " Size: " << (int) info.m_access_size << " BaseR: ";
    if(info.m_base_register == InvalidReg)
    {
      os << "Inv";
    }
    else
    {
      os << (int) info.m_base_register;
    }
    os << " OffR: ";
    if(info.m_offset_register == InvalidReg)
    {
      os << "Inv";
    }
    else
    {
      os << (int) info.m_offset_register;
    }
    os << " Imm: " << info.m_immediate << "]";
    return os;
  }
};

size_t memAccessSize(const StaticMemoryInfo& info)
{
  return info.m_access_size;
}

struct __attribute__((packed)) DynMemoryInfo : public SerDes<DynMemoryInfo>
{
  Address m_effective_address;
  const StaticMemoryInfo * m_static_info;

  DynMemoryInfo(const StaticMemoryInfo& info)
  : SerDes<DynMemoryInfo>()
  , m_static_info(&info)
  {}

  DynMemoryInfo(ThreadedLZMAInputStreamer & input, const StaticMemoryInfo& info)
  : SerDes<DynMemoryInfo>()
  , m_static_info(&info)
  {
    deserialize(input, *this);
  }

  REGISTER_SERDES_INIT_CTOR(DynMemoryInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, DynMemoryInfo& info)
  {
    input.read((char*) &info.m_effective_address, sizeof(info.m_effective_address));
  }

  static void serialize_v0(std::ostream& output, const DynMemoryInfo& info)
  {
    output.write((char*) &info.m_effective_address, sizeof(info.m_effective_address));
  }

  friend std::ostream& operator<<(std::ostream & os, const DynMemoryInfo & info)
  {
    os << "[" << *(info.m_static_info) << " EA: " << std::hex << info.m_effective_address << "]";
    return os;
  }
};

Address memEffectiveAddr(const DynMemoryInfo& info)
{
  return info.m_effective_address;
}

std::bitset<64> generateOverlapMask(const DynMemoryInfo& lhs, const DynMemoryInfo& rhs)
{
  std::bitset<64> match_mask;

  Address lhs_start_address = lhs.m_effective_address;
  Address lhs_end_address = lhs_start_address +
    lhs.m_static_info->m_access_size - 1;

  for(
    int offset = 0;
    offset != std::min(lhs.m_static_info->m_access_size, rhs.m_static_info->m_access_size);
    offset++
  )
  {
    Address addr = rhs.m_effective_address + offset;
    match_mask.set(offset, addr >= lhs_start_address && addr <= lhs_end_address);
  }

  return match_mask;
}
struct __attribute__((packed)) StaticBranchInfo : public SerDes<StaticBranchInfo>
{
  enum class BranchType
  {
    Cond,
    Uncond,
    Call,
    Return,
    Invalid
  };

  static constexpr const char * btype_info[] = {
    "Cond", 
    "Uncond",
    "Call",
    "Return",
    "Invalid"
  };

  enum class BranchTargetType
  {
    Direct,
    Indirect,
    Invalid
  };

  static constexpr const char * tar_info[] = {
    "Direct", 
    "Indirect",
    "Invalid",
  };

  BranchType m_type = BranchType::Invalid;
  BranchTargetType m_target_type = BranchTargetType::Invalid;

  static constexpr size_t struct_size = sizeof(m_type) + sizeof(m_target_type);

  StaticBranchInfo() = default;

  REGISTER_SERDES_INIT_CTOR(StaticBranchInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, StaticBranchInfo& info)
  {
    input.read((char*) &info.m_type, struct_size);
  }

  static void serialize_v0(std::ostream& output, const StaticBranchInfo& info)
  {
    output.write((char*) &info.m_type, struct_size);
  }
  
  bool operator==(const StaticBranchInfo & rhs) const
  {
    return memcmp((const void*) &m_type, (const void*) &rhs.m_type, sizeof(m_type) + sizeof(m_target_type)) == 0;
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticBranchInfo & info)
  {
    os << std::dec << "[" << btype_info[static_cast<int>(info.m_type)] << tar_info[static_cast<int>(info.m_target_type)] << "]";
    return os; 
  }
};

bool isCondBranch(const StaticBranchInfo& info)
{
  return info.m_type == StaticBranchInfo::BranchType::Cond;
}

bool isUncondBranch(const StaticBranchInfo& info)
{
  return !isCondBranch(info);
}

bool isCall(const StaticBranchInfo& info)
{
  return info.m_type == StaticBranchInfo::BranchType::Call;
};

bool isReturn(const StaticBranchInfo& info)
{
  return info.m_type == StaticBranchInfo::BranchType::Return;
}

bool isDirectBranch(const StaticBranchInfo& info)
{
  return info.m_target_type == StaticBranchInfo::BranchTargetType::Direct;
}

bool isIndirectBranch(const StaticBranchInfo& info)
{
  return info.m_target_type == StaticBranchInfo::BranchTargetType::Indirect;
}

bool isUncondDirectBranch(const StaticBranchInfo& info)
{
  return isUncondBranch(info) && isDirectBranch(info);
}

bool isUncondIndirectBranch(const StaticBranchInfo& info)
{
  return isUncondBranch(info) && isIndirectBranch(info);
}

struct __attribute__((packed)) DynBranchInfo : public SerDes<DynBranchInfo>
{
  bool m_taken;
  Address m_target;
  const StaticBranchInfo * m_static_info;

  DynBranchInfo(const StaticBranchInfo& info)
  : SerDes<DynBranchInfo>()
  , m_static_info(&info)
  {}

  DynBranchInfo(ThreadedLZMAInputStreamer & input, const StaticBranchInfo& info)
  : SerDes<DynBranchInfo>()
  , m_static_info(&info)
  {
    deserialize(input, *this);
  }

  REGISTER_SERDES_INIT_CTOR(DynBranchInfo)

  static constexpr size_t struct_size = sizeof(m_taken) + sizeof(m_target);

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, DynBranchInfo& info)
  {
    input.read((char*) &info.m_taken, struct_size);
  }

  static void serialize_v0(std::ostream& output, const DynBranchInfo& info)
  {
    output.write((char*) &info.m_taken, struct_size);
  }

  friend std::ostream& operator<<(std::ostream& os, const DynBranchInfo& info)
  {
    os << std::dec << "[" << *(info.m_static_info) << (info.m_taken ? " Taken" : " NonTaken") << std::hex << "Tar: " << info.m_target << "]";
    return os;
  }
};

bool isTakenBranch(const DynBranchInfo & info)
{
  return info.m_taken;
}

Address branchTarget(const DynBranchInfo & info)
{
  return info.m_target;
}

struct __attribute__((packed)) StaticSysRegInfo : public SerDes<StaticSysRegInfo>
{
  uint8_t m_sys_reg;

  StaticSysRegInfo() = default;

  REGISTER_SERDES_INIT_CTOR(StaticSysRegInfo)
  
  static void deserialize_v0(ThreadedLZMAInputStreamer& input, StaticSysRegInfo& info)
  {
    input.read((char*) &info.m_sys_reg, sizeof(info.m_sys_reg));
  }

  static void serialize_v0(std::ostream& output, const StaticSysRegInfo& info)
  {
    output.write((char*) &info.m_sys_reg, sizeof(info.m_sys_reg));
  }

  bool operator==(const StaticSysRegInfo & rhs) const
  {
    return m_sys_reg == rhs.m_sys_reg;
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticSysRegInfo& info)
  {
    os << std::dec << "[" << (int) info.m_sys_reg << "]";
    return os;
  }
};

struct __attribute__((packed)) DynSysRegInfo : public SerDes<DynSysRegInfo>
{
  const StaticSysRegInfo * m_static_info;

  DynSysRegInfo(const StaticSysRegInfo& info)
  : SerDes<DynSysRegInfo>()
  , m_static_info(&info)
  {}

  DynSysRegInfo(ThreadedLZMAInputStreamer & input, const StaticSysRegInfo& info)
  : SerDes<DynSysRegInfo>()
  , m_static_info(&info)
  {
    deserialize(input, *this);
  }

  REGISTER_SERDES_INIT_CTOR(DynSysRegInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, DynSysRegInfo& info) {}

  static void serialize_v0(std::ostream& output, const DynSysRegInfo& info) {}

  friend std::ostream& operator<<(std::ostream& os, const DynSysRegInfo& info)
  {
    os << std::dec << "[" << *(info.m_static_info) << "]";
    return os;
  }
};

struct __attribute__((packed)) StaticFlagWriterInfo : public SerDes<StaticFlagWriterInfo>
{
  std::bitset<8> m_output_flags;  

  StaticFlagWriterInfo() = default;

  REGISTER_SERDES_INIT_CTOR(StaticFlagWriterInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, StaticFlagWriterInfo& info)
  {
    input.read((char*) &info.m_output_flags, sizeof(info.m_output_flags));
  }

  static void serialize_v0(std::ostream& output, const StaticFlagWriterInfo& info)
  {
    output.write((char*) &info.m_output_flags, sizeof(info.m_output_flags));
  }

  bool operator==(const StaticFlagWriterInfo & rhs) const
  {
    return m_output_flags == rhs.m_output_flags;
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticFlagWriterInfo& info)
  {
    os << std::dec << "[" << info.m_output_flags.to_string() << "]";
    return os;
  }
};

struct __attribute__((packed)) DynFlagWriterInfo : public SerDes<DynFlagWriterInfo>
{
  uint8_t m_output_flags_values;
  const StaticFlagWriterInfo * m_static_info;

  DynFlagWriterInfo(const StaticFlagWriterInfo& info)
  : SerDes<DynFlagWriterInfo>()
  , m_static_info(&info)
  {}

  DynFlagWriterInfo(ThreadedLZMAInputStreamer & input, const StaticFlagWriterInfo& info)
  : SerDes<DynFlagWriterInfo>()
  , m_static_info(&info)
  {
    deserialize(input, *this);
  }

  REGISTER_SERDES_INIT_CTOR(DynFlagWriterInfo)
  
  static void deserialize_v0(ThreadedLZMAInputStreamer& input, DynFlagWriterInfo& info)
  {
    input.read((char*) &info.m_output_flags_values, sizeof(info.m_output_flags_values));
  }

  static void serialize_v0(std::ostream& output, const DynFlagWriterInfo& info)
  {
    output.write((char*) &info.m_output_flags_values, sizeof(info.m_output_flags_values));
  }

  friend std::ostream& operator<<(std::ostream& os, const DynFlagWriterInfo& info)
  {
    os << std::dec << "[" << *(info.m_static_info) << " " << std::hex << (int) info.m_output_flags_values << "]";
    return os;
  }
};

struct  __attribute__((packed)) StaticFlagReaderInfo : public SerDes<StaticFlagReaderInfo>
{
  std::bitset<8> m_input_flags;

  StaticFlagReaderInfo() = default;

  REGISTER_SERDES_INIT_CTOR(StaticFlagReaderInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, StaticFlagReaderInfo& info)
  {
    input.read((char*) &info.m_input_flags, sizeof(info.m_input_flags));
  }

  static void serialize_v0(std::ostream& output, const StaticFlagReaderInfo& info)
  {
    output.write((char*) &info.m_input_flags, sizeof(info.m_input_flags));
  }

  bool operator==(const StaticFlagReaderInfo & rhs) const
  {
    return m_input_flags == rhs.m_input_flags;
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticFlagReaderInfo& info)
  {
    os << std::dec << "[" << info.m_input_flags.to_string() << "]";
    return os;
  }
};

struct __attribute__((packed)) DynFlagReaderInfo : public SerDes<DynFlagReaderInfo>
{
  uint8_t m_input_flags_values;
  const StaticFlagReaderInfo * m_static_info;  

  DynFlagReaderInfo(const StaticFlagReaderInfo& info)
  : SerDes<DynFlagReaderInfo>()
  , m_static_info(&info)
  {}

  DynFlagReaderInfo(ThreadedLZMAInputStreamer & input, const StaticFlagReaderInfo& info)
  : SerDes<DynFlagReaderInfo>()
  , m_static_info(&info)
  {
    deserialize(input, *this);
  }

  REGISTER_SERDES_INIT_CTOR(DynFlagReaderInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, DynFlagReaderInfo& info)
  {
    input.read((char*) &info.m_input_flags_values, sizeof(info.m_input_flags_values));
  }

  static void serialize_v0(std::ostream& output, const DynFlagReaderInfo& info)
  {
    output.write((char*) &info.m_input_flags_values, sizeof(info.m_input_flags_values));
  }

  friend std::ostream& operator<<(std::ostream& os, const DynFlagReaderInfo& info)
  {
    os << std::dec << "[" << *(info.m_static_info) << " " << std::hex << (int) info.m_input_flags_values << "]";
    return os;
  }
};

struct __attribute__((packed)) StaticSimdInfo : public SerDes<StaticSimdInfo>
{
  int m_element_size;

  StaticSimdInfo() = default;

  REGISTER_SERDES_INIT_CTOR(StaticSimdInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, StaticSimdInfo& info)
  {
    input.read((char*) &info.m_element_size, sizeof(info.m_element_size));
  }

  static void serialize_v0(std::ostream& output, const StaticSimdInfo & info)
  {
    output.write((char*) &info.m_element_size, sizeof(info.m_element_size));
  }

  bool operator==(const StaticSimdInfo & rhs) const
  {
    return m_element_size == rhs.m_element_size;
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticSimdInfo& info)
  {
    os << std::dec << "[EltSize: " << info.m_element_size << "]";
    return os;
  }
};

struct __attribute__((packed)) DynSimdInfo : public SerDes<DynSimdInfo>
{
  const StaticSimdInfo * m_static_info;

  DynSimdInfo(const StaticSimdInfo& info)
  : SerDes<DynSimdInfo>()
  , m_static_info(&info)
  {}

  DynSimdInfo(ThreadedLZMAInputStreamer & input, const StaticSimdInfo& info)
  : SerDes<DynSimdInfo>()
  , m_static_info(&info)
  {
    deserialize(input, *this);
  }

  REGISTER_SERDES_INIT_CTOR(DynSimdInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, DynSimdInfo& info) {}

  static void serialize_v0(std::ostream& output, const DynSimdInfo& info) {}

  friend std::ostream& operator<<(std::ostream& os, const DynSimdInfo& info)
  {
    os << std::dec << "[" << *(info.m_static_info) << "]";
    return os;
  }
};


struct __attribute__((packed)) StaticPredicationInfo : public SerDes<StaticPredicationInfo>
{
  std::bitset<64> m_predicates;

  StaticPredicationInfo() = default;

  REGISTER_SERDES_INIT_CTOR(StaticPredicationInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, StaticPredicationInfo& info)
  {
    input.read((char*) &info.m_predicates, sizeof(info.m_predicates));
  }

  static void serialize_v0(std::ostream& output, const StaticPredicationInfo& info)
  {
    output.write((char*) &info.m_predicates, sizeof(info.m_predicates));
  }

  bool operator==(const StaticPredicationInfo & rhs) const
  {
    return m_predicates == rhs.m_predicates;
  }

  friend std::ostream& operator<<(std::ostream& os, const StaticPredicationInfo& info)
  {
    os << std::dec << "[" << info.m_predicates.to_string() << "]";
    return os;
  }
};

struct __attribute__((packed)) DynPredicationInfo : public SerDes<DynPredicationInfo>
{
  std::bitset<64> m_predicate_values;
  const StaticPredicationInfo * m_static_info;

  DynPredicationInfo(const StaticPredicationInfo& info)
  : SerDes<DynPredicationInfo>()
  , m_static_info(&info)
  {}

  DynPredicationInfo(ThreadedLZMAInputStreamer & input, const StaticPredicationInfo& info)
  : SerDes<DynPredicationInfo>()
  , m_static_info(&info)
  {
    deserialize(input, *this);
  }

  REGISTER_SERDES_INIT_CTOR(DynPredicationInfo)

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, DynPredicationInfo& info)
  {
    input.read((char*) &info.m_predicate_values, sizeof(info.m_predicate_values));
  }

  static void serialize_v0(std::ostream& output, const DynPredicationInfo& info)
  {
    output.write((char*) &info.m_predicate_values, sizeof(info.m_predicate_values));
  }

  friend std::ostream& operator<<(std::ostream& os, const DynPredicationInfo& info)
  {
    os << std::dec << "[" << info.m_predicate_values.to_string() << *(info.m_static_info) << "]";
    return os;
  }
};

struct __attribute__((packed)) DynTraceSpecialInfo
{
  enum class Type
  {
    ContextChange,
    Invalid
  };

  Type m_type = Type::Invalid;
  std::optional<Context> m_new_context;

  friend void serialize(std::ostream& output, const DynTraceSpecialInfo& info)
  {
    output.write((char*) &info.m_type, sizeof(info.m_type));
    if(info.m_type == Type::ContextChange)
    {
      serialize(output, info.m_new_context.value());
    }
  }

  friend void deserialize(ThreadedLZMAInputStreamer& input, DynTraceSpecialInfo& info)
  {
    input.read((char*) &info.m_type, sizeof(info.m_type));
    if(info.m_type == Type::ContextChange)
    {
      deserialize(input, info.m_new_context.emplace(Context{}));
    }
  }

  DynTraceSpecialInfo() = default;

  DynTraceSpecialInfo(ThreadedLZMAInputStreamer & input)
  {
    deserialize(input, *this);
  }

  friend std::ostream& operator<<(std::ostream& os, const DynTraceSpecialInfo& info)
  {
    if(info.m_new_context)
    {
      os << std::dec << "[" << info.m_new_context.value() << "]";
    }
    else
    {
      os << "[]";
    }
    return os;
  }
};

REGISTER_SERDES_DUMMY(StaticRegInfo)
REGISTER_SERDES_DUMMY(StaticBranchInfo)
REGISTER_SERDES_DUMMY(StaticMemoryInfo)
REGISTER_SERDES_DUMMY(StaticSysRegInfo)
REGISTER_SERDES_DUMMY(StaticFlagWriterInfo)
REGISTER_SERDES_DUMMY(StaticFlagReaderInfo)
REGISTER_SERDES_DUMMY(StaticSimdInfo)
REGISTER_SERDES_DUMMY(StaticPredicationInfo)

REGISTER_SERDES_DUMMY(DynBranchInfo)
REGISTER_SERDES_DUMMY(DynMemoryInfo)
REGISTER_SERDES_DUMMY(DynSysRegInfo)
REGISTER_SERDES_DUMMY(DynFlagWriterInfo)
REGISTER_SERDES_DUMMY(DynFlagReaderInfo)
REGISTER_SERDES_DUMMY(DynSimdInfo)
REGISTER_SERDES_DUMMY(DynPredicationInfo)

}
