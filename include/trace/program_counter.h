/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <inttypes.h>
#include <iostream>

#include <util/serdes.h>
#include <trace/lzma_threaded_streamer.h>

namespace Monarch
{

// TODO : 128-bit support
using Address = uint64_t;
using Value = uint64_t;

void serialize(std::ostream& output, const Address& addr)
{
  output.write((char*) &addr, sizeof(Address));
}

void deserialize(ThreadedLZMAInputStreamer& input, Address& addr)
{
  input.read((char*) &addr, sizeof(Address));
}

struct __attribute__((packed)) Context : public SerDes<Context>
{
  uint64_t m_context_id;
  uint8_t m_ring;

  Context()
  : SerDes<Context>()
  , m_context_id(0xdeadbeef)
  , m_ring(255)
  {}

  Context(uint64_t context, uint8_t id)
  : SerDes<Context>()
  , m_context_id(context)
  , m_ring(id)
  {}

  REGISTER_SERDES_INIT_CTOR(Context)

  static constexpr size_t struct_size = sizeof(m_context_id) + sizeof(m_ring);

  static void serialize_v0(std::ostream& output, const Context &ctxt)
  {
    output.write((char*) &ctxt.m_context_id, struct_size);
  }

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, Context &ctxt)
  {
    input.read((char*) &ctxt.m_context_id, struct_size);
  }

  bool operator==(const Context & ctxt) const
  {
    return (m_context_id == ctxt.m_context_id) && (m_ring == ctxt.m_ring);
  }

  bool operator!=(const Context & ctxt) const
  {
    return !(*this == ctxt);
  }

  friend std::ostream& operator<<(std::ostream & os, const Context & ctxt)
  {
    os << std::hex << "[ID: " << ctxt.m_context_id << " Ring: " << (int) ctxt.m_ring << "]";
    return os;
  }
};

struct __attribute__((packed)) ProgramCounter : public SerDes<ProgramCounter>
{
  // Version 0
  Address m_pc;
  Context m_context;

  ProgramCounter()
  : SerDes<ProgramCounter>()
  , m_pc(0xdeadbeef)
  , m_context(0,0)
  {}

  ProgramCounter(const Address & pc, const Context & context)
  : SerDes<ProgramCounter>()
  , m_pc(pc)
  , m_context(context)
  {}
  
  REGISTER_SERDES_INIT_CTOR(ProgramCounter)

  static void serialize_v0(std::ostream& output, const ProgramCounter &pc)
  {
    output.write((char*) &pc.m_pc, sizeof(pc.m_pc));
    serialize(output, pc.m_context);  
  }

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, ProgramCounter &pc) 
  {
    input.read((char*) &pc.m_pc, sizeof(pc.m_pc));
    deserialize(input, pc.m_context);
  }

  bool operator==(const ProgramCounter & pc) const
  {
    return (m_pc == pc.m_pc) && (m_context == pc.m_context);
  }

  bool operator<(const ProgramCounter & pc) const
  {
    return m_pc < pc.m_pc;
  }

  friend std::ostream& operator<<(std::ostream & os, const ProgramCounter & pc)
  {
    os << std::hex << "[PC: " << pc.m_pc << " Ctxt: " << pc.m_context << "]";
    return os;
  }
};

REGISTER_SERDES_DUMMY(ProgramCounter)
REGISTER_SERDES_DUMMY(Context)

}

namespace std {

  template <>
  struct hash<Monarch::Context>
  {
    std::size_t operator()(const Monarch::Context& c) const
    {
      using std::size_t;
      using std::hash;
      
      return (((uint64_t) c.m_ring) << 56) ^ (c.m_context_id);
    }
  };

  template <>
  struct hash<Monarch::ProgramCounter>
  {
    std::size_t operator()(const Monarch::ProgramCounter& pc) const
    {
      using std::size_t;
      using std::hash;
      
      return hash<uint64_t>()(pc.m_pc) ^
        hash<Monarch::Context>()(pc.m_context);
    }
  };
}
