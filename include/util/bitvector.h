/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <util/serdes.h>

namespace Monarch
{

class Bitvectorbase : public SerDes<Bitvectorbase>
{
protected:
  uint64_t m_data;

public:
  Bitvectorbase()
  : SerDes<Bitvectorbase>()
  , m_data(0)
  {}

  Bitvectorbase(uint64_t data)
  : SerDes<Bitvectorbase>()
  , m_data(data)
  {}

  REGISTER_SERDES_INIT_CTOR(Bitvectorbase)

  static void serialize_v0(std::ostream& output, const Bitvectorbase& flags)
  {
    output.write((char*) &flags.m_data, sizeof(flags.m_data));
  }

  static void deserialize_v0(ThreadedLZMAInputStreamer& input, Bitvectorbase& flags)
  {
    input.read((char*) &flags.m_data, sizeof(flags.m_data));
  }
};

REGISTER_SERDES_DUMMY(Bitvectorbase)

template<typename Flags>
class Bitvector : public Bitvectorbase
{
  static_assert(static_cast<int>(Flags::numFlags) <= 64, "I am too lazy to implement larger bitvector");

public:
  Bitvector() = default;

  // TODO Make private
  Bitvector(uint64_t data)
  : Bitvectorbase(data)
  {}

  Bitvector(const Flags & flags)
  : Bitvectorbase(1lu << static_cast<uint64_t>(flags))
  {}

  Bitvector(const Bitvector & bv)
  : Bitvectorbase(bv.m_data)
  {}

  bool operator==(const Bitvector & rhs) const
  {
    return m_data == rhs.m_data;
  }
  
  Bitvector operator|(const Bitvector & rhs) const
  {
    return {this->m_data | rhs.m_data};
  }

  Bitvector operator&(const Bitvector & rhs) const
  {
    return {this->m_data & rhs.m_data};
  }

  Bitvector operator^(const Bitvector & rhs) const
  {
    return {this->m_data ^ rhs.m_data};
  }

  void set(const Bitvector & bv)
  {
    this->m_data |= bv.m_data;
  }

  bool any(const Bitvector & bv) const
  {
    return (this->m_data & bv.m_data) != 0;
  }

  bool all(const Bitvector & bv) const
  {
    return (this->m_data & bv.m_data) == bv.m_data;
  }

  bool none(const Bitvector & bv) const
  {
    return (this->m_data & bv.m_data) == 0;
  }

  bool none() const
  {
    return this->m_data == 0;
  }

  // TODO : Fix this so we can just have the << operator
  friend std::ostream& print(std::ostream& os, const Bitvector<Flags> & bv, char ** names)
  {
    os << "[ ";
    for(int i = 0; i < static_cast<int>(Flags::numFlags); i++)
    {
      if((bv.m_data & (1 << i)) != 0)
      {
        os << names[i] << " ";
      }
    }
    os << "]";
    return os;
  }
};

template<typename Flags>
Bitvector<Flags> operator|(const Flags & lhs, const Flags & rhs)
{
    return {(1lu << static_cast<uint64_t>(lhs)) | (1lu << static_cast<uint64_t>(rhs))};
}

}