/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <deque>
#include <unordered_set>

#if __cplusplus == 202002L
#include <concepts>

template <typename T>
concept HasDefaultConstructor = requires(T v)
{
    { T() } -> std::same_as<T>;
};
#endif

template<typename Object, int StartSize = 10>
#if __cplusplus == 202002L
requires HasDefaultConstructor<Object>
#endif
struct ObjectPool
{
  // Cannot use vector because vector can reallocate,
  // which invalidates pointers :)
  // Consider using something where we can reserve
  // space and not construct so Object does not have
  // to implement default constructor if we do not need
  // it
  std::deque<Object> m_storage;
  std::vector<Object*> m_free_list;

#ifndef NDEBUG
  std::unordered_set<Object*> m_busy_list;
#endif

  ObjectPool()
  : m_storage(StartSize)
  {
    for(auto & obj_ref : m_storage)
    {
      m_free_list.push_back(&obj_ref);
    }
  }

  ~ObjectPool() = default;
  
  template<class... Args> 
  Object* allocate(Args&&... args)
  {
    if(m_free_list.size())
    {
      Object * ptr = m_free_list.back();
#ifndef NDEBUG
      assert(m_busy_list.count(ptr) == 0);
      m_busy_list.insert(ptr);
#endif
      m_free_list.pop_back();
      return new (ptr) Object(args...);
    }
    else
    {
      Object * ptr = &m_storage.emplace_back(args...);
#ifndef NDEBUG
      m_busy_list.insert(ptr);
#endif
      return ptr;
    }
  }

  void deallocate(Object * ptr)
  {
    assert(m_busy_list.count(ptr));

    ptr->~Object();

#ifndef NDEBUG
    m_busy_list.erase(ptr);
#endif
    m_free_list.push_back(ptr);
  }

#ifndef NDEBUG
  size_t capacity() const
  {
    return m_free_list.size() + m_busy_list.size();
  }

  size_t occupancy() const
  {
    return m_busy_list.size();
  }
#endif
};