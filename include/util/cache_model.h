/*
 * Copyright (c) 2022 Arthur Perais & Ugo Avdibegovic
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <inttypes.h>
#include <numeric>
#include <cmath>
#include <assert.h> 
#include <variant>
#include <map>
#include <optional>

#include <util/fixed_array.h>

#include <stats/stats.h>

/**
#if __cplusplus == 202002L
#include <concepts>
**/

namespace Monarch
{

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
// explicit deduction guide (not needed as of C++20)
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

enum class ReplPolicy
{ 
  Random,
  LRU,
  TypeAwareLRU,
  Invalid  
};

static ReplPolicy getReplPolicyFromStr(const std::string & str)
{
  static const std::map<std::string, ReplPolicy> names = 
  {
    {"Random", ReplPolicy::Random},
    {"LRU", ReplPolicy::LRU},
    {"TypeAwareLRU", ReplPolicy::TypeAwareLRU},
    {"Invalid", ReplPolicy::Invalid}
  };
  assert(names.count(str) != 0);
  return names.at(str);
}

template <typename EntryType> class Set;

template <typename EntryType>
class ReplPolicyBase
{
public:
  ReplPolicyBase() = delete;
  ReplPolicyBase(const ReplPolicyBase&) = delete;
  ReplPolicyBase(ReplPolicyBase&&) = delete;
 
  ReplPolicyBase(Set<EntryType> * set);

  virtual ~ReplPolicyBase() = default;

  virtual int getVictimWay(const typename EntryType::ContentType new_entry_type) const = 0;
  
  virtual void updateOnSearch(int way, const EntryType & entry) = 0;

  virtual void updateOnInsert(int way, const EntryType & entry) = 0;

  friend class Set<EntryType>;

  Set<EntryType> * _set; 
};

template <typename EntryType>
class RandomRepl : public ReplPolicyBase<EntryType>
{
public:
  RandomRepl(Set<EntryType> * set);

  virtual ~RandomRepl() = default;

  int getVictimWay(const typename EntryType::ContentType new_entry_type) const override;

  void updateOnSearch(int way, const EntryType & entry) override;

  void updateOnInsert(int way, const EntryType & entry) override;
};

template <typename EntryType>
class LRURepl : public ReplPolicyBase<EntryType>
{
public:
  LRURepl(Set<EntryType> * set);

  virtual ~LRURepl() = default;

  int getVictimWay(const typename EntryType::ContentType new_entry_type) const override;

  void updateOnSearch(int way, const EntryType & entry) override;

  void updateOnInsert(int way, const EntryType & entry) override;

  FixedArray<uint64_t> _lru;
  uint64_t _lru_count;
};

template<typename EntryType>
class TypeAwareLRURepl : public ReplPolicyBase<EntryType>
{
public:
  TypeAwareLRURepl(Set<EntryType> * set);

  virtual ~TypeAwareLRURepl() = default;

  int getVictimWay(const typename EntryType::ContentType new_entry_type) const override;

  void updateOnSearch(int way, const EntryType & entry) override;

  void updateOnInsert(int way, const EntryType & entry) override;

  FixedArray<uint64_t> _lru;
  uint64_t _lru_count;
};

/**
#if __cplusplus == 202002L
template<typename T>
concept InstDataAwareCacheEntry = requires(T a)
{
  T::ContentType::Instruction;
  T::ContentType::Data;
};
**/

template <class EntryType>
struct StatsBase
{
  virtual ~StatsBase() = default;

  virtual void updateOnSearch(const EntryType * entry, typename EntryType::ContentType type, int way) = 0;

  virtual void updateOnInsert(const EntryType * old_entry, const EntryType& new_entry, int way) = 0;
};

template <class EntryType>
struct SetStats : public StatsBase<EntryType>
{
  // TODO Add boolean so that it does not print the stats, but still registers them
  SetStats(const std::string& name, StatsContainer* container, int ways, bool register_stats)
  : StatsBase<EntryType>()
  , hits(container->regStat<Counter>(name + std::string(".hits"), register_stats))
  , misses(container->regStat<Counter>(name + std::string(".misses"), register_stats))
  , total_accesses(container->regStat<Formula>(name + std::string(".accesses"), register_stats))
  , hitrate(container->regStat<Formula>(name + std::string(".hitrate"), register_stats))
  {
    total_accesses = hits + misses;
    hitrate = hits / total_accesses;
  }

  virtual ~SetStats() = default;

  void updateOnSearch(const EntryType * entry, typename EntryType::ContentType type, int way) override
  {
    hits += (entry != nullptr);
    misses += (entry == nullptr);    
  }

  void updateOnInsert(const EntryType * old_entry, const EntryType& new_entry, int way) override
  {

  }

  Counter& hits;
  Counter& misses;
  Formula& total_accesses;
  Formula& hitrate;
};

template <class EntryType>
/**
#if __cplusplus == 202002L
requires InstDataAwareCacheEntry<EntryType>
**/
struct TypeAwareSetStats : public SetStats<EntryType>
{
  // TODO Add boolean so that it does not print the stats, but still registers them
  TypeAwareSetStats(const std::string& name, StatsContainer* container, int ways, bool register_stats)
  : SetStats<EntryType>(name, container, ways, register_stats)
  , data_hits(container->regStat<Counter>(name + std::string(".data_hits"), register_stats))
  , insts_hits(container->regStat<Counter>(name + std::string(".insts_hits"), register_stats))
  , data_misses(container->regStat<Counter>(name + std::string(".data_misses"), register_stats))
  , insts_misses(container->regStat<Counter>(name + std::string(".insts_misses"), register_stats))
  , data_hitrate(container->regStat<Formula>(name + std::string(".data_hitrate"), register_stats))
  , insts_hitrate(container->regStat<Formula>(name + std::string(".insts_hitrate"), register_stats))
  , insertion_cat_heatmap(container->regStat<FixedBucketsHistogram>(name + std::string(".cat_heatmap"), 1, 2, std::vector<std::string>{"Data", "Instruction"}, register_stats))
  {
    data_hitrate = data_hits / (data_hits + data_misses);
    insts_hitrate = insts_hits / (insts_hits + insts_misses);
  }

  virtual ~TypeAwareSetStats() = default;

  void updateOnSearch(const EntryType * entry, typename EntryType::ContentType type, int way) override
  {
    SetStats<EntryType>::updateOnSearch(entry, type, way);

    insts_hits += (entry != nullptr) && type == EntryType::ContentType::Instruction;
    data_hits += (entry != nullptr) && type == EntryType::ContentType::Data;
    insts_misses += (entry == nullptr) && type == EntryType::ContentType::Instruction;
    data_misses += (entry == nullptr) && type == EntryType::ContentType::Data;
  }

  void updateOnInsert(const EntryType * old_entry, const EntryType& new_entry, int way) override
  {
    if(new_entry.getContentType() == EntryType::ContentType::Instruction)
    {
      insertion_cat_heatmap.addElement(0);
    }
    else
    {
      insertion_cat_heatmap.addElement(1);
    }
  }
  
  Counter& data_hits;
  Counter& insts_hits;
  Counter& data_misses;
  Counter& insts_misses;
  Formula& data_hitrate;
  Formula& insts_hitrate;
  FixedBucketsHistogram& insertion_cat_heatmap;
};



template <typename EntryType>
class Set
{
public:  
  Set() = delete;
  Set(const Set& rhs) = delete;
  Set(Set&& rhs) = delete;
  ~Set() = default;

  Set(int ways, ReplPolicy repl_pol, const NameGenerator& name_gen, StatsContainer* container, bool register_stats = false)
  : _set(ways)
  , _stats(name_gen(), container, ways, register_stats)
  {
     switch(repl_pol)
    {
      case ReplPolicy::Random:
        _repl.template emplace<RandomRepl<EntryType>>(this);
        break;
      case ReplPolicy::LRU:
        _repl.template emplace<LRURepl<EntryType>>(this);
        break;
      case ReplPolicy::TypeAwareLRU:
        /**
        if constexpr(InstDataAwareCacheEntry<EntryType>)
        {
          _repl.template emplace<TypeAwareLRURepl<EntryType>>(this);
        }
        else
        {
        **/
          throw std::invalid_argument("Cache Entry Type not compatible with TypeAwareLRU policy");
        /**
        }
        **/
        break;
      default:
        throw std::invalid_argument("Did not choose a repl policy");
        break;
    }
  }

  void updateOnSearch(int victim_way, const EntryType & entry)
  {
    std::visit(overloaded{
      [](std::monostate & ugh) { throw std::invalid_argument("No valid updateOnSearch function"); },
      [=](auto & repl) -> void { repl.updateOnSearch(victim_way, entry); }
    }, _repl);
  }

  void updateOnInsert(int victim_way, const EntryType & entry)
  {
    std::visit(overloaded{
      [](std::monostate & ugh) { throw std::invalid_argument("No valid updateOnInsert function"); },
      [=](auto & repl) -> void { repl.updateOnInsert(victim_way, entry); }
    }, _repl);
  }

  EntryType * insert(const EntryType &entry, int way)
  {
    assert(entry.isValid());
    auto old_entry = getWay(way);
    _stats.updateOnInsert(old_entry, entry, way);
    *old_entry = entry;
    return old_entry;
  }
    
  void invalidate(const typename EntryType::TagType & victim_tag)
  {
    if(auto [entry, way] = functionalSearch(victim_tag); entry != nullptr)
    {
      entry->invalidate();
    }
  }

  std::pair<EntryType*, int> functionalSearch(const typename EntryType::TagType & search_tag)
  {
    for(int way = 0; way < (int) _set.size(); way++)
    {
      auto & entry = _set[way];
      const bool matches = entry.isValid() && (search_tag == entry.getTag());
      if(matches)
      {
        return {&entry, way};
      }
    }
    return {nullptr, -1};
  }

  std::pair<EntryType*, int> search(const typename EntryType::TagType & search_tag, typename EntryType::ContentType type)
  {
    auto [entry, way] = functionalSearch(search_tag);
  
    _stats.updateOnSearch(entry, type, way);
   
    return {entry, way};
  }

  EntryType * getWay(int way)
  {
    assert(way >= 0 && (size_t) way < _set.size());
    return &_set[way];
  }

  int getVictimWay(const typename EntryType::ContentType new_entry_type) const
  {
    return std::visit(overloaded{
      [](const std::monostate & ugh) { throw std::invalid_argument("No valid getVictimWay() function"); return 0; },
      [=](const auto & repl) -> int { return repl.getVictimWay(new_entry_type); }
    }, _repl);
  }

  bool isWayValid(int way) const
  {
    return _set[way].isValid();
  }

  int numWays() const
  {
    return _set.size();
  }

  int numValidWays() const
  {
    return std::count_if(_set.begin(), _set.end(), [](const auto & entry)
    {
      return entry.isValid();
    });
  }

  const typename EntryType::StatType& getStats() const
  {
    return _stats;
  }
  
  FixedArray<EntryType> _set; 
  std::variant<std::monostate, RandomRepl<EntryType>, LRURepl<EntryType>, TypeAwareLRURepl<EntryType>> _repl; 
  typename EntryType::StatType _stats;
};

template <typename EntryType>
class BaseSetAssocStructure
{
public:
  BaseSetAssocStructure() = delete;
  BaseSetAssocStructure(const BaseSetAssocStructure&) = delete;
  BaseSetAssocStructure(BaseSetAssocStructure&&) = delete;

  BaseSetAssocStructure(int line_size, int sets, int ways)
  : _sets(sets)
  , _ways(ways)
  , _set_offset(log2(sets))
  , _word_offset(log2(line_size))
  , _set_mask((0x1lu << _set_offset) - 1)
  , _word_mask((0x1lu << _word_offset) - 1)
  {
    // Enforce power of two sets and line size
    assert((sets & (sets - 1)) == 0);
    assert((line_size & (line_size - 1)) == 0);
  }

  virtual ~BaseSetAssocStructure() = default;

  virtual uint64_t getSet(const typename EntryType::TagType & item_address) = 0;

  virtual uint64_t getTag(const typename EntryType::TagType & item_address) = 0;

  int numSets() const { return _sets; }

  int numWays() const { return _ways; }

  virtual std::tuple<EntryType*, uint64_t, int> insert(const typename EntryType::TagType & address, const EntryType & entry) = 0;

  virtual void invalidate(const typename EntryType::TagType & victim_address) = 0;

  virtual std::tuple<EntryType*, uint64_t, int> functionalSearch(const typename EntryType::TagType & address) = 0;

  virtual std::tuple<EntryType*, uint64_t, int> search(const typename EntryType::TagType & address, typename EntryType::ContentType type) = 0;

protected:                                             
  int _sets;             
  int _ways;            

  int _set_offset;
  int _word_offset;

  uint64_t _set_mask;
  uint64_t _word_mask;
};

template<typename EntryType>
class SetAssocStructure : public BaseSetAssocStructure<EntryType>     
{	      
public :
  template<class... Args>
  SetAssocStructure(const std::string & name, StatsContainer * container, int line_size, int sets, int ways, ReplPolicy repl_pol, Args&&... args)
  : BaseSetAssocStructure<EntryType>(line_size, sets, ways)
  , _cache(sets, ways, repl_pol, NameGenerator(name + std::string(".set")), container, args...)
  , _stats(name, container, ways, true)
  {
  }
    
  virtual ~SetAssocStructure() = default;

  uint64_t getSet(const typename EntryType::TagType &  item_address) override
  {
    return (item_address >> this->_word_offset) & this->_set_mask;
  }

  uint64_t getTag(const typename EntryType::TagType &  item_address) override
  {
    return (item_address >> (this->_word_offset + this->_set_offset));
  }

  virtual std::tuple<EntryType*, uint64_t, int> insert(const typename EntryType::TagType & address, const EntryType & entry) override
  {
    assert(entry.isValid());

    auto set_idx = getSet(address);
    auto & set = _cache[set_idx];
    auto victim_way = set.getVictimWay(entry.getContentType());
    auto victim = set.insert(entry, victim_way);

    _stats.updateOnInsert(victim, entry, victim_way);

    set.updateOnInsert(victim_way, entry);
    return {victim, set_idx, victim_way};
  }

  void invalidate(const typename EntryType::TagType & victim_address) override
  {
    auto & set = _cache[getSet(victim_address)];
    set.invalidate(getTag(victim_address));
  }

  std::tuple<EntryType*, uint64_t, int> functionalSearch(const typename EntryType::TagType & address) override
  {
    auto set_idx = getSet(address);
    auto & set = _cache[set_idx];
    auto [entry, way] = set.functionalSearch(getTag(address));  
    return {entry, set_idx, way};
  }

  std::tuple<EntryType*, uint64_t, int> search(const typename EntryType::TagType & address, typename EntryType::ContentType type) override
  {
    auto set_idx = getSet(address);
    auto & set = _cache[set_idx];
    auto [entry, way] = set.search(getTag(address), type);  

    if(entry != nullptr)
    {
      set.updateOnSearch(way, *entry);
      
    }

    _stats.updateOnSearch(entry, type, way);
   
    return {entry, set_idx, way};
  }
    
  FixedArray<Set<EntryType>> _cache;   
  typename EntryType::StatType _stats;
};

enum class BaseContentType
{
  Instruction,
  Data,
  Invalid
};

template<typename _TagType, typename _PayloadType, typename _ContentType>
struct _BaseCacheEntry
{

  // To bring those in BaseCacheEntry::
  using TagType = _TagType;
  using PayloadType = _PayloadType;
  using ContentType = _ContentType;

  TagType tag;
  PayloadType payload;
  ContentType content;

  _BaseCacheEntry() 
  : tag(0x0)
  , payload(0x0)
  , content(ContentType::Invalid)
  {}

  _BaseCacheEntry(TagType tag, PayloadType pl, ContentType type)
  : tag(tag)
  , payload(pl)
  , content(type)
  {}

  virtual ~_BaseCacheEntry() = default;

  TagType getTag() const
  {
    return tag;
  }

  PayloadType getPayload() const
  {
    return payload;
  }

  ContentType getContentType() const
  {
    return content;
  }

  bool isValid() const
  {
    return content != ContentType::Invalid;
  }

  void invalidate()
  {
    content = ContentType::Invalid;
  }
};

template <typename _TagType, typename _PayloadType, typename _ContentType, template <typename EntryType>class  _StatType>
struct BaseCacheEntry : public _BaseCacheEntry<_TagType, _PayloadType, _ContentType>
{
  using StatType = _StatType<_BaseCacheEntry<_TagType, _PayloadType, _ContentType>>;

  BaseCacheEntry()
  : _BaseCacheEntry<_TagType, _PayloadType,_ContentType>()
  {}

  BaseCacheEntry(
    typename _BaseCacheEntry<_TagType, _PayloadType,_ContentType>::TagType tag,
    typename _BaseCacheEntry<_TagType, _PayloadType, _ContentType>::PayloadType pl,
    typename _BaseCacheEntry<_TagType, _PayloadType, _ContentType>::ContentType type
  )
  : _BaseCacheEntry<_TagType, _PayloadType,_ContentType>(tag, pl, type)
  {}

  virtual ~BaseCacheEntry() = default;
};

}