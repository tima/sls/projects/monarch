/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <iterator>

namespace Monarch
{

template <class EntryType>
class FixedArray
{
public:
  template<class... Args>
  FixedArray(size_t size, Args&&... args)
  : _size(size)
  {
    _storage = (EntryType*) new char [size * sizeof(EntryType)];
    for(size_t i = 0; i < size; i++)
    {
      new (&_storage[i]) EntryType(args...);
    }
  }

  ~FixedArray()
  {
    delete _storage;
  }

  size_t size() const
  {
    return _size;
  }

  const EntryType& operator[](size_t idx) const
  {
    return _storage[idx];
  }

  EntryType& operator[](size_t idx)
  {
    return _storage[idx];
  }

  struct iterator 
  {
    using iterator_category = std::random_access_iterator_tag;
    using difference_type   = std::ptrdiff_t;
    using value_type        = EntryType;
    using pointer           = EntryType*;
    using reference         = EntryType&;

    iterator(pointer ptr) : m_ptr(ptr) {}

    reference operator*() const { return *m_ptr; }
    pointer operator->() { return m_ptr; }

    iterator& operator++() { m_ptr++; return *this; }  
    iterator& operator--() { m_ptr--; return *this; }  

    iterator operator++(int) { iterator tmp = *this; ++(*this); return tmp; }
    iterator operator--(int) { iterator tmp = *this; --(*this); return tmp; }

    friend bool operator== (const iterator& a, const iterator& b) { return a.m_ptr == b.m_ptr; };
    friend bool operator!= (const iterator& a, const iterator& b) { return a.m_ptr != b.m_ptr; };     

  private:
    pointer m_ptr;
  };

  iterator begin() { return iterator(_storage); }
  iterator begin() const { return iterator(_storage); }
  iterator end()   { return iterator(_storage + _size); } 
  iterator end() const  { return iterator(_storage + _size); } 

private:
  size_t _size;
  EntryType * _storage;
};

}

 