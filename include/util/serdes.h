/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <iostream>
#include <functional>

#include <trace/lzma_threaded_streamer.h>

#define SET_SER_FUNCTION(Type, ver) \
  SerDes<Type>::ser = &Type::serialize_##ver

#define SET_DES_FUNCTION(Type, ver) \
  SerDes<Type>::des = &Type::deserialize_##ver

#define REGISTER_SERDES_INIT_CTOR(Type) \
  Type(SerDesDummy_t) \
  : SerDes<Type>(SerDesDummy_t()) \
  {} 

#define REGISTER_SERDES_DUMMY(Type) \
  static inline Type dummy_##Type = {SerDesDummy_t()};

namespace Monarch
{

struct SerDesBase
{
  static inline std::vector<std::function<void(int)>> ver_setup;

  static void setupSerDesVersion(int version)
  {
    for(auto & func : ver_setup)
    {
      func(version);
    }
  }
};

struct SerDesDummy_t {};

// TODO : Make ThreadedLZMA Useable with std::istream
template<typename UnderlyingType>
struct SerDes : public SerDesBase
{

  static inline void (*ser) (std::ostream&, const UnderlyingType&);
  static inline void (*des) (ThreadedLZMAInputStreamer&, UnderlyingType&);

  SerDes() {}

  SerDes(SerDesDummy_t)
  {
    ver_setup.emplace_back(UnderlyingType::setupSerDesVersion);
  }
  
  virtual ~SerDes() = default;

  static void setupSerDesVersion(int version)
  {
    switch(version)
    {
      case 0:
        SET_SER_FUNCTION(UnderlyingType, v0);
        SET_DES_FUNCTION(UnderlyingType, v0);
        break;
      default:
        std::cerr << "Trace version " << std::dec << version << " not handled " << std::endl;
    }
    
  }

  friend void deserialize(ThreadedLZMAInputStreamer& input, UnderlyingType& obj)
  {
    SerDes<UnderlyingType>::des(input, obj);
  }

  friend void serialize(std::ostream& output, const UnderlyingType& obj)
  {
    SerDes<UnderlyingType>::ser(output, obj);
  }
};

}
