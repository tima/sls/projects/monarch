/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <util/object_pool.h>

template <typename Pointed>
struct ReferenceCounted
{
  // Maybe we could bundle those together ?
  static inline ObjectPool<Pointed> m_pool;
  static inline ObjectPool<size_t> m_counters;

  Pointed * m_ptr; 
  size_t * m_count;

  ReferenceCounted(std::nullptr_t)
  : m_ptr(nullptr)
  , m_count(nullptr)
  {}

  ReferenceCounted(const ReferenceCounted<Pointed> & rc)
  {
    this->m_ptr = rc.m_ptr;
    this->m_count = rc.m_count;
    increaseRefCount();
  }

  ReferenceCounted(ReferenceCounted<Pointed> && rc)
  {
    this->m_ptr = rc.m_ptr;
    this->m_count = rc.m_count;
    rc.m_ptr = nullptr;
    rc.m_count = nullptr;
  }

  // TODO : If this breaks, switch to static function
  template
  <
    class... Args, 
    // Do this so this constructor is not used instead of copy/move ctor
    typename = std::enable_if_t
    <
      (!std::is_base_of<ReferenceCounted<Pointed>, std::decay_t<Args>>::value && ...) 
    >
  >
  ReferenceCounted<Pointed>(Args&&... args)
  {
    m_ptr = m_pool.allocate(args...);
    m_count = m_counters.allocate(1);
  }

  virtual ~ReferenceCounted()
  {
    decreaseRefCount();
    releaseIfNeeded();
  }

  ReferenceCounted<Pointed>& operator=(const ReferenceCounted<Pointed> & rc)
  {
    // Auto-assign is idempotent
    if(this == &rc)
    {
      return *this;
    }

    decreaseRefCount();
    releaseIfNeeded();
    this->m_ptr = rc.m_ptr;
    this->m_count = rc.m_count;
    increaseRefCount();
    return *this;
  }

  ReferenceCounted<Pointed>& operator=(ReferenceCounted<Pointed> && rc)
  {
    // Let's not support auto-move, not sure it makes sense
    assert(this != &rc);
    decreaseRefCount();
    releaseIfNeeded();
    this->m_ptr = rc.m_ptr;
    this->m_count = rc.m_count;
    rc.m_ptr = nullptr;
    rc.m_count = nullptr;
    return *this;
  }

  void decreaseRefCount()
  {
    if(m_count == nullptr)
    {
      return;
    }
    assert(*m_count);
    (*m_count)--;
  }

  void increaseRefCount()
  {
    (*m_count)++;
  }

  void releaseIfNeeded()
  {
    if(m_count == nullptr)
    {
      assert(m_ptr == nullptr);
      return;
    }

    if(*m_count == 0)
    {
      if(m_ptr)
      {
        m_pool.deallocate(m_ptr);
      }
      m_counters.deallocate(m_count);
    }
  }
  
  Pointed& operator*() const
  {
    return *m_ptr;
  }

  Pointed* operator->() const
  {
    return m_ptr;
  }

  Pointed* get() const
  {
    return m_ptr;
  }

  bool operator==(std::nullptr_t) const
  {
    return m_ptr == nullptr && m_count == nullptr;
  }

  bool operator!=(std::nullptr_t) const
  {
    return !(*this == nullptr);
  }

  bool operator==(const ReferenceCounted<Pointed> & ref) const
  {
    return m_ptr == ref.m_ptr && m_count == ref.m_count;
  }

  bool operator!=(const ReferenceCounted<Pointed>& ref) const
  {
    return !(*this == ref);
  }

  operator bool() const
  {
    return m_ptr != nullptr && m_count != nullptr;
  }
};