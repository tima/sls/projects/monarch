/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <chrono>

#include <tools/interfaces.h>

#include <stats/stats.h>

#include <util/timer.h>


namespace Monarch
{

struct MIPSProfiler : public AbstractProfiler
{
  Counter total_instructions;
  Formula &mips = regStat<Formula>("MIPS");

  bool m_started = false;

  std::chrono::time_point<std::chrono::system_clock> m_StartTime;
  
  MIPSProfiler(py::dict params)
  : AbstractProfiler(params)
  {}

  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wunused-parameter"
  void processInst(const InstPointer & inst) override
  {
    if(!m_started)
    {
      m_StartTime = std::chrono::system_clock::now();
      m_started = true;
    }
    total_instructions++;
  }
  #pragma clang diagnostic pop

  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  py::dict postProcessStats() const override
  {
    auto total_time =  std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - m_StartTime).count();
    mips = total_instructions / (total_time * 1000.0);

    return {};
  }
};

}
