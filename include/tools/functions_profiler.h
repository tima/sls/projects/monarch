/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <list>
#include <vector>
#include <map>

#include <tools/interfaces.h>

#include <stats/stats.h>

namespace Monarch
{

struct FunctionsProfiler : public AbstractProfiler
{
  struct FunctionFootprint
  {
    uint64_t start_pc = 0x0;
    uint64_t ret_addr = 0x0;
    uint64_t end_pc = 0x0;
    int num_calls = 0;
    int num_insts = 0;

    FunctionFootprint(uint64_t pc)
    : start_pc(pc)
    {
    }
  };

  FunctionsProfiler(py::dict params)
  : AbstractProfiler(params)
  {
    avg_callret_to_callret = callret_to_callret_cumulated / (num_calls + num_rets);
  }

  std::vector<FunctionFootprint> m_call_stack;
  std::map<uint64_t, std::vector<FunctionFootprint>> m_functions;

  uint64_t current_callret_to_callret = 0;

  // Stats
  Counter& max_call_depth = regStat<Counter>("MaxCallDepth");
  Counter& sequence_breaks = regStat<Counter>("SeqBreak");
  Counter& total_insts = regStat<Counter>("TotalInsts");

  Counter& num_calls = regStat<Counter>("NumCalls");
  Counter& num_rets = regStat<Counter>("NumRets");
  
  Counter callret_to_callret_cumulated;
  
  Formula& avg_callret_to_callret = regStat<Formula>("AvgCallRetToCallRetDist");

  void dumpState()
  {
    std::cout << "CallStack: ";
    for(const auto & elt : m_call_stack)
    {
      std::cout << "[SA:" << std::hex << elt.start_pc << " RA:" << elt.ret_addr << "] ";
    }
    std::cout << std::endl;
  }
  
  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  void processInst(const InstPointer & inst) override
  {
    total_insts++;

    if(isCall(inst))
    {
      num_calls++;
      callret_to_callret_cumulated += current_callret_to_callret;
      current_callret_to_callret = 0;
      if(!m_call_stack.empty())
      {
        m_call_stack.back().num_calls++;
        // Does not work for x86 !
        m_call_stack.back().ret_addr = getFallthroughPC(inst);
      }
      m_call_stack.emplace_back(getBranchTarget(inst));
      max_call_depth = std::max((uint64_t) max_call_depth, m_call_stack.size());
    }
    else if(isReturn(inst) && !m_call_stack.empty())
    { 
      num_rets++;
      callret_to_callret_cumulated += current_callret_to_callret;
      current_callret_to_callret = 0;
      auto & func = m_call_stack.back();

      func.end_pc = getPC(inst);
      func.num_insts++;
      m_functions[func.start_pc].push_back(func);
      m_call_stack.pop_back();

      // We may actually have to pop several if the code is doing weird things
      // like not the same amount of call and returns (tail call elim?)
      // Note that we *have* to find a valid entry here, unless it is the last
      // Ret of the program
      // If we are in kernel, do not pop userland stuff
      bool skipped_levels = false;
      while(!m_call_stack.empty() && 
          m_call_stack.back().ret_addr != getBranchTarget(inst))
      {
        auto & func = m_call_stack.back();
        func.end_pc = getPC(inst);
        m_functions[func.start_pc].push_back(func);
  
        skipped_levels = true;
        m_call_stack.pop_back();
      }
    }
    else if(!m_call_stack.empty())
    {
      current_callret_to_callret++;
      m_call_stack.back().num_insts++;
    }
  }

  py::dict postProcessStats() const override
  {
    Counter& total_static_functions = regStat<Counter>("TotalStaticFunctions");
    total_static_functions = m_functions.size();
        
    Counter& total_func_calls = regStat<Counter>("TotalDynamicFunctions");
    
    Counter total_func_lengths;

    Formula& avg_func_lengths = regStat<Formula>("AvgCallLength");
    avg_func_lengths = total_func_lengths / total_func_calls;

    for(const auto & elt : m_functions)
    {
      total_func_calls += elt.second.size();
      for(const auto & invocation : elt.second)
      {
        total_func_lengths += invocation.num_insts;
      }
    }

    std::list<std::vector<FunctionFootprint>> m_sortable;
    for(const auto & elt : m_functions)
    {
      m_sortable.push_back(elt.second);
    }

    m_sortable.sort([](const std::vector<FunctionFootprint> & lhs, const std::vector<FunctionFootprint> & rhs){
      return lhs.size() > rhs.size();
    });

  

    py::dict supplemental = {};
    supplemental["PerFunctionStats"] = py::dict{};
    supplemental["PerFunctionStats"]["Type"] = "Custom"; 
    supplemental["PerFunctionStats"]["Value"] = py::dict{};

    for(const auto & elt : m_sortable)
    {
      uint64_t num_calls = 0;
      uint64_t num_insts = 0;
      int max_insts = 0;
      int min_insts = elt.front().num_insts;
      for(const auto & invocation : elt)
      {
        num_calls += invocation.num_calls;
        num_insts += invocation.num_insts;
        max_insts = std::max(max_insts, invocation.num_insts);
        min_insts = std::min(min_insts, invocation.num_insts);
      }
      
      supplemental["PerFunctionStats"]["Value"][std::to_string(elt.front().start_pc).c_str()] = py::dict{};
      supplemental["PerFunctionStats"]["Value"][std::to_string(elt.front().start_pc).c_str()]["MinLen"] = min_insts;
      supplemental["PerFunctionStats"]["Value"][std::to_string(elt.front().start_pc).c_str()]["AvgLen"] = (double) num_insts / (double) elt.size();
      supplemental["PerFunctionStats"]["Value"][std::to_string(elt.front().start_pc).c_str()]["MaxLen"] = max_insts;
      supplemental["PerFunctionStats"]["Value"][std::to_string(elt.front().start_pc).c_str()]["AvgCalls"] = (double) num_calls / (double) elt.size();
      supplemental["PerFunctionStats"]["Value"][std::to_string(elt.front().start_pc).c_str()]["NumInvocs"] = elt.size();
    }

    return supplemental;
  }
};

}