/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <map>
#include <vector>
#include <deque>
#include <bitset>
#include <algorithm>

#include <stats/stats.h>

#include <tools/interfaces.h>

namespace Monarch
{

struct LoopStreamProfiler : public AbstractProfiler
{
  const int max_inst_per_entry = 56;
  const int max_taken_branches_per_entry = 56;
  // Ignore mismach push/pop

  struct Loop
  {
    std::bitset<56> taken_branches;
    std::deque<InstPointer> instructions;

    void reset()
    {
      instructions.clear();
      taken_branches.reset();
    }

    int takenBranches() const
    {
      return taken_branches.count();
    }

    void recordTakenBranch()
    {
      taken_branches.set(instructions.size() - 1);
    }

    void recordPC(const InstPointer & inst)
    {
      assert(instructions.size() < 56);
      instructions.push_back(*inst);
    }

    void pushOneOut()
    {
      assert(instructions.size());
      instructions.pop_front();
      taken_branches <<= 1;
    }

    bool contains(Address pc) const
    {
      return std::find_if(instructions.begin(), instructions.end(), [=](const auto & inst)
      {
        return pc == getPC(inst);
      }) != instructions.end();
    }

    void adjust(Address pc)
    {
      instructions.erase(instructions.begin(), std::find_if(instructions.begin(), instructions.end(), [=](const auto & inst)
      {
        return pc == getPC(inst);
      }));
    }


    size_t size()
    {
      return instructions.size();
    }

    void dump() const
    {
      assert(instructions.size() <= 56);

      std::cout << "Block starts at " << std::hex << getPC(instructions.front()) << " " << std::dec << instructions.size() << " insts " << std::endl;
      int index = 0;
      for(auto pc : instructions)
      {
        std::cout << pc << " ";
        std::cout << "TkbBr: " << taken_branches.test(index) << std::endl;
        index++;
      }
    }

  };

  struct LoopInfo
  {
    Address start_pc;
    Address end_pc;
    std::vector<int> trip_count;
    std::bitset<56> m_taken_branches;
    std::deque<InstPointer> m_instrs;
    int size;

    int current_run_count = 1;
    int total_run_count = 1;

    LoopInfo(const Loop & loop)
    : start_pc(getPC(loop.instructions.front()))
    , end_pc(getPC(loop.instructions.back()))
    , m_taken_branches(loop.taken_branches)
    , m_instrs(loop.instructions)
    , size(loop.instructions.size())
    {
      assert(size > 0);
    }

    void dump() const
    {
      std::cout << "[" << std::hex << start_pc << ":" << end_pc << "] (" << std::dec << size << " insts) -> " << std::endl;
      for(const auto &pc : m_instrs)
      {
        std::cout << pc << " ";
      }
      for(const auto count : trip_count)
      {
        std::cout << "{" << count << "} ";
      }
      std::cout << std::endl;
    }
  };

  struct IdentifiedLoops
  {
    // Stats
    Counter& numIdentifiedLoops;
    ExponentialBucketsHistogram& tripCountDistribution;
    FixedBucketsHistogram& sizeDistribution;
    FixedBucketsHistogram& loopEndReasons;
    Counter& totalNumInstsInLoops;
    Counter& numInsts;
    Formula& percentInstsInLoops;

    IdentifiedLoops(StatsContainer * container)
    : numIdentifiedLoops(container->regStat<Counter>("NumIdentifiedLoops"))
    , tripCountDistribution(container->regStat<ExponentialBucketsHistogram>("TripCountDistribution", 64, 8, 8))
    , sizeDistribution(container->regStat<FixedBucketsHistogram>("SizeDistribution", 8, 8))
    , loopEndReasons(container->regStat<FixedBucketsHistogram>("loopEndReasons", 1, 4, std::vector<std::string>{"CallRet", "SeqBreak", "TooManyTaken", "TooManyInsts"}))
    , totalNumInstsInLoops(container->regStat<Counter>("totalNumInstsInLoops"))
    , numInsts(container->regStat<Counter>("numInsts"))
    , percentInstsInLoops(container->regStat<Formula>("percentInstsInLoop"))
    {
      percentInstsInLoops = totalNumInstsInLoops / numInsts;
    }

    std::deque<LoopInfo> m_identified;

    LoopInfo* current_loop = nullptr;

    void terminateCurrentLoop()
    {
      if(!current_loop)
      {
        return;
      }

      totalNumInstsInLoops += current_loop->size * current_loop->current_run_count;
      assert(totalNumInstsInLoops.evaluate() <= numInsts.evaluate());
      sizeDistribution.addElement(current_loop->size);
      tripCountDistribution.addElement(current_loop->current_run_count);
      current_loop->trip_count.push_back(current_loop->current_run_count);
      current_loop->current_run_count = 0;
      current_loop = nullptr;
    }

    void addLoop(const Loop & loop)
    {
      if(auto existing = findLoop(loop); existing)
      {
        if(current_loop != existing)
        {
          terminateCurrentLoop();
          current_loop = existing;
        }
        
        current_loop->current_run_count++;
        current_loop->total_run_count++;        
      }
      else
      {
        numIdentifiedLoops++;
        m_identified.push_back(loop);
        terminateCurrentLoop();
        current_loop = &m_identified.back();
      }
    }

    LoopInfo * findLoop(const Loop & loop)
    {
      auto it = std::find_if(m_identified.begin(), m_identified.end(), [&](const auto & elt) {
        const bool same_start = elt.start_pc == getPC(loop.instructions.front());
        const bool same_end = elt.end_pc == getPC(loop.instructions.back());
        const bool same_length = ((size_t) elt.size) == loop.instructions.size();

        return same_start && same_end && same_length;
      });

      if(it != m_identified.end())
      {
        return &(*it);
      }

      return nullptr;
    }

    int findOverlap() const
    {
      int count = 0;
      for(const auto & loop : m_identified)
      {
        for(const auto & other_loop : m_identified)
        {
          if(&other_loop == &loop)
          {
            continue;
          }

          bool overlap = false;
          // Just determine if they share some instructions
          for(const auto & inst_pc : loop.m_instrs)
          {
            for(const auto & other_pc : other_loop.m_instrs)
            {
              overlap |= getPC(inst_pc) == getPC(other_pc);
            }
          }
          if(overlap)
          {
            std::cout << "Found overlap between " << std::endl;
            loop.dump();
            std::cout << "and" << std::endl;
            other_loop.dump();
          }
          count += overlap;
        }
      }
      return count;
    }

  };

  enum class TerminateReason
  {
    CallRet,
    SeqBreak,
    TooManyTaken,
    TooManyInsts,
    Done,
    Unexpected,
    Invalid
  };

  IdentifiedLoops m_loops{this};

  Loop m_current_loop;

  Loop m_running_loop;
  int m_running_loop_count = 0;
  bool m_running_loop_valid = false;

  LoopStreamProfiler(py::dict params)
  : AbstractProfiler(params)
  {}

  void pushOneOut()
  {
    m_current_loop.pushOneOut();
  }

  void invalidateLoop(TerminateReason reason)
  {
    m_current_loop.reset();
    if(reason != TerminateReason::Done)
    {
      m_loops.loopEndReasons.addElement(static_cast<uint64_t>(reason));
      m_loops.terminateCurrentLoop();
    }
  }

  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  void processInst(const InstPointer & inst) override
  {
    // Todo push this to AbtractProfiler
    m_loops.numInsts++;

    if(isCall(inst) || isReturn(inst))
    {
      invalidateLoop(TerminateReason::CallRet);
      return;
    }

    // If Taken branch, we might be starting a loop
    if(isTakenBranch(inst))
    {
      int count = m_current_loop.takenBranches();
     

      const bool block_too_big = m_current_loop.size() >= ((size_t) max_inst_per_entry);

      if(block_too_big)
      {
        pushOneOut();
      }

      const bool too_many_taken = count == max_taken_branches_per_entry;
      const bool jumps_to_same_block = m_current_loop.contains(getBranchTarget(inst));

      if(too_many_taken)
      {
        invalidateLoop(TerminateReason::TooManyTaken); 
      }
      else if(jumps_to_same_block)
      {
        m_current_loop.recordPC(inst);
        m_current_loop.recordTakenBranch();
        // We are branching back, notify
        m_current_loop.adjust(getBranchTarget(inst));
        m_loops.addLoop(m_current_loop);
        invalidateLoop(TerminateReason::Done);
      }
      else
      {
        m_current_loop.recordPC(inst);
        m_current_loop.recordTakenBranch();
      }

      return;
    }
    else
    {
      const bool block_too_big = m_current_loop.size() >= ((size_t) max_inst_per_entry);

      if(block_too_big)
      {
        pushOneOut();
      }
      m_current_loop.recordPC(inst);
    }
  }

  virtual py::dict postProcessStats() const override
  {
    std::cout << "Found " << m_loops.m_identified.size() << " loops " << std::endl;
    for(const auto & elt : m_loops.m_identified)
    {
      elt.dump();
    }

    // Try to find overlap
    std::cout << "Overlap: " << m_loops.findOverlap() << std::endl;
    return {};
  }
};

}