/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <tools/interfaces.h>

#include <util/fixed_array.h>

#include <stats/stats.h>

namespace Monarch
{

constexpr bool is_power_of_two(size_t size)
{
  return size != 0 && ((size & (size - 1)) == 0);
}

struct PredictorBase
{
  virtual ~PredictorBase() = default;

  virtual bool predict(Address addr) = 0;

  virtual void update(Address addr, bool actually_taken) = 0;
};

struct StupidPredictor : public PredictorBase
{
  bool predict(Address addr) override
  {
    return true;
  }

  void update(Address addr, bool actually_taken) override
  {

  }
};

struct RandomPredictor : public PredictorBase
{
  int bias;

  RandomPredictor(int prediction_proba)
  : PredictorBase()
  , bias(prediction_proba)
  {
    assert(bias >= 0 && bias <= 100);
  }

  bool predict(Address addr) override
  {
    return (rand() % 101) > (100 - bias);
  }

  void update(Address addr, bool actually_taken) override
  {

  }
};

struct BimodalPredictor : public PredictorBase
{

  // [0;1] means taken
  // [-2;-1] means not taken
  FixedArray<int> _counters;

  BimodalPredictor(int num_entries)
  : PredictorBase()
  , _counters(num_entries, 0)
  {
    assert(is_power_of_two(num_entries));
    std::fill(_counters.begin(), _counters.end(), 0);
  }

  bool predict(Address addr) override
  {
    // Shift right by 2 because instructions are 4B and always aligned to 4B
    uint64_t index = (addr >> 2) & (_counters.size() - 1);
    return _counters[index] >= 0;
  }

  void update(Address addr, bool actually_taken) override
  {
    // Shift right by 2 because instructions are 4B and always aligned to 4B
    uint64_t index = (addr >> 2) & (_counters.size() - 1);
    int& counter = _counters[index];

    counter += (counter < 1 && actually_taken);
    counter -= (counter > -2 && !actually_taken);
  }
};

struct GSharePredictor : public PredictorBase
{
  FixedArray<int> _counters;
  uint64_t _ghr = 0;
  uint64_t _ghr_mask;

  GSharePredictor(int num_entries)
  : PredictorBase()
  , _counters(num_entries, 0)
  , _ghr_mask(num_entries - 1)
  {
    assert(is_power_of_two(num_entries));
    std::fill(_counters.begin(), _counters.end(), 0);
  }

  bool predict(Address addr) override
  {
    uint64_t index = ((addr >> 2) ^ (_ghr & _ghr_mask)) & _counters.size() - 1;
    return _counters[index] >= 0;
  }

  void update(Address addr, bool actually_taken) override
  {
    uint64_t index = ((addr >> 2) ^ (_ghr & _ghr_mask)) & _counters.size() - 1;
    int & counter = _counters[index];

    counter += (counter < 1 && actually_taken);
    counter -= (counter > -2 && !actually_taken);

    _ghr = (_ghr << 1) | actually_taken;
  }
};

// local à compléter
struct LocalPredictor : public PredictorBase
{
  LocalPredictor(int num_pht_entries, int num_bht_entries)
  : PredictorBase()
  {
  }

  bool predict(Address addr) override
  {
    return true;
  }

  void update(Address addr, bool actually_taken) override
  {

  }
};

// Tournament à compléter
struct TournamentPredictor : public PredictorBase
{
  Counter preds_by_gshare;
  Counter preds_by_local;

  Formula& ratio_preds_by_gshare;

  TournamentPredictor(int gshare_num_entries, int local_num_pht_entries, int local_num_bht_entries, StatsContainer & container)
  : PredictorBase()
  , ratio_preds_by_gshare(container.regStat<Formula>("RatioPredsByGShare"))
  {
    ratio_preds_by_gshare = preds_by_gshare / (preds_by_gshare + preds_by_local);
  }

  bool predict(Address addr) override
  {
    preds_by_gshare++;
    return true;
  }

  void update(Address addr, bool actually_taken) override
  {

  }
};

// Perceptron à compléter
struct PerceptronPredictor : public PredictorBase
{
  PerceptronPredictor(int num_entries, int history_length, int theta)
  : PredictorBase()
  {
  }

  bool predict(Address addr) override
  {
    return true;
  }

  void update(Address addr, bool actually_taken) override
  {

  }
};

struct BPProfiler : public AbstractProfiler
{
  Counter num_instructions;
  Counter num_predictions;
  Counter num_correct_predictions;
  
  Formula &hitrate;
  Formula &mpki;
  
  PredictorBase * predictor = nullptr;

  BPProfiler(py::dict params)
  : AbstractProfiler(params)
  , hitrate(this->regStat<Formula>("Hitrate"))
  , mpki(this->regStat<Formula>("MPKI"))
  {
    mpki = (num_predictions - num_correct_predictions) / num_instructions * 1000;
    hitrate = num_correct_predictions / num_predictions;

    std::string pred = params["knobs"]["predictor"].cast<std::string>();
    
    if(pred == std::string("Stupid"))
    {
      predictor = new StupidPredictor();
    }
    else if(pred == std::string("Random"))
    {
      predictor = new RandomPredictor(params["knobs"]["chance_taken"].cast<int>());
    }
    else if(pred == std::string("Bimodal"))
    {
      predictor = new BimodalPredictor(params["knobs"]["entries"].cast<int>());
    }
    else if(pred == std::string("GShare"))
    {
      predictor = new GSharePredictor(params["knobs"]["entries"].cast<int>());
    }
    else if(pred == std::string("Local"))
    {
      predictor = new LocalPredictor(
        params["knobs"]["entries_pht"].cast<int>(),
        params["knobs"]["entries_bht"].cast<int>()
      );
    }
    else if(pred == std::string("Tournament"))
    {
      predictor = new TournamentPredictor(
        params["knobs"]["gshare_entries"].cast<int>(),
        params["knobs"]["local_entries_pht"].cast<int>(),
        params["knobs"]["local_entries_bht"].cast<int>(),
        *this
      );
    }
    else if(pred == std::string("Perceptron"))
    {
      predictor = new PerceptronPredictor(
        params["knobs"]["entries"].cast<int>(),
        params["knobs"]["ghr_length"].cast<int>(),
        params["knobs"]["theta"].cast<int>()
      );
    }
    /**
    else if(pred == "...")
    {
      predictor = new ...Predictor();
    }
    **/
    else
    {
      throw std::invalid_argument("Wrong predictor type");
    }
  }

  ~BPProfiler()
  {
    if(predictor)
    {
      delete predictor;
    }
  }

  void processInst(const InstPointer & inst) override
  {
    num_instructions++;

    if(isBranch(inst) && isCondBranch(inst))
    {
      const bool actually_taken = isTakenBranch(inst);
      const auto inst_address = getPC(inst);
      
      
      // Predict it
      const bool predict_taken = predictor->predict(inst_address);
      
      // Update the predictor
      predictor->update(inst_address, actually_taken);

      // Update the stats
      num_predictions++;
      num_correct_predictions += actually_taken == predict_taken;
    }
  }

  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  py::dict postProcessStats() const override
  {
    return {};
  }
};

}
