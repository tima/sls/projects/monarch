/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <string>

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>

#include <stats/stats.h>

#include <util/bitvector.h>

namespace py = pybind11;

namespace Monarch
{

struct AbstractProfiler : public StatsContainer
{
  const std::string m_name;
  const std::string m_category;

  AbstractProfiler(py::dict params)
  : StatsContainer()
  , m_name(params["name"].cast<std::string>())
  , m_category(params["category"].cast<std::string>())
  {
  }

  virtual ~AbstractProfiler() = default;

  const std::string & name() const
  {
    return m_name;
  }

  const std::string & category() const
  {
    return m_category;
  }

  virtual void checkFeatureReqs(Bitvector<TraceFeatures> & features) = 0;

  virtual void processInst(const InstPointer & inst) = 0;

  virtual py::dict postProcessStats() const = 0;

  py::dict dumpStats() const override
  {
    py::dict stats = {};
      
    stats["Supplemental"] = postProcessStats();
    stats["Stats"] = StatsContainer::dumpStats();

    return stats;
  }

  void serializeStats() const
  {
    py::dict stats = dumpStats();
    stats["Filename"] = name() + ".json";
    stats["ToolCategory"] = category();
    stats["ToolName"] = name(); 

    py::exec(R"(
        my_dict = locals().copy()
        import os
        import json
        with open(os.path.join(os.getcwd(), locals()['Filename']), "w") as outfile:
          json.dump(my_dict, outfile)
    )", py::globals(), stats);
  }
};

}