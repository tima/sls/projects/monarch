/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <tools/interfaces.h>

#include <stats/stats.h>

namespace Monarch
{

struct ISideFootprintProfiler : public AbstractProfiler
{
  using StatsContainer::regStat;

  uint64_t m_insts = 0;
  std::unordered_map<uint64_t, uint64_t> m_data;
  std::unordered_set<uint64_t> m_epoch_data;

  PeriodicFormula& window = regStat<PeriodicFormula>("periodic1MFootprintInMB", [this]() -> double
  {
    double result = (double) (m_epoch_data.size() * 64) / (double) (1024 * 1024);
    m_epoch_data.clear();
    return result;
  });

  ISideFootprintProfiler(py::dict params)
  : AbstractProfiler(params)
  {}

  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  void processInst(const InstPointer & inst) override
  {
    if((++m_insts % 1000000) == 0)
    {
      window.sample();
    }
    m_data[(getPC(inst) >> 6)]++;
    m_epoch_data.insert(getPC(inst) >> 6);
  }

  void processPercentage(double threshold) const
  {
    Counter num_elts;

    Formula& footprint = regStat<Formula>("TotalIFootprintMB" + std::to_string((uint64_t) threshold));
    footprint = (num_elts * 64) / (1024 * 1024);

    std::vector<uint64_t> count;
    uint64_t total_count = 0;

    for(const auto & elt : m_data)
    {
      count.push_back(elt.second);
      total_count += elt.second;
    }

    std::sort(count.begin(), count.end(), [](const auto & lhs, const auto & rhs){
      return lhs > rhs;
    });

    double limit = (threshold / 100.0) * total_count;
    total_count = 0;

    for(const auto & elt : count)
    {
      num_elts++;
      total_count += elt;
      if(total_count > limit)
      {
        break;
      }
    }
  }

  py::dict postProcessStats() const override
  {
    processPercentage(100);
    processPercentage(99);
    processPercentage(95);
    processPercentage(90);

    return {};
  }
};

}
