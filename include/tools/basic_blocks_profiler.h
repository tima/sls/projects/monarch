/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <map>
#include <set>

#include <tools/interfaces.h>

#include <stats/stats.h>

namespace Monarch
{

struct BasicBlocksProfiler : public AbstractProfiler
{
  struct StaticBBInfo
  {
    std::unordered_map<int, uint64_t> m_sizes;
  };

  struct DynBBInfo
  {
    std::vector<Address> m_insts;

    void reset()
    {
      m_insts.clear();
    }

    void addInst(Address pc)
    {
      m_insts.push_back(pc);
    };

    size_t size() const
    {
      return m_insts.size();
    }
  };

  BasicBlocksProfiler(py::dict params)
  : AbstractProfiler(params)
  {
    percent_dyn_bb_ending_with_taken_cond = basic_blocks_ending_with_taken_cond / total_dynamic_basic_blocks;
    percent_dyn_bb_ending_with_taken_uncond_direct = basic_blocks_ending_with_always_taken_uncond_direct / total_dynamic_basic_blocks;
    percent_dyn_bb_ending_with_taken_uncond_indirect = basic_blocks_ending_with_always_taken_uncond_indirect / total_dynamic_basic_blocks;

    avg_dyn_bb_size = cum_basic_block_size / total_dynamic_basic_blocks;

    avg_dyn_cache_lines_per_dyn_bb = total_dyn_cache_lines / total_dynamic_basic_blocks;
  }

  Address m_current_bb_start_pc;
  std::unordered_map<ProgramCounter, StaticBBInfo> m_bbinfo;

  DynBBInfo m_current_bb;

  // Working variables
  int current_basic_block_size = 0;
  Context current_context;

  // Stats  
  Counter cum_basic_block_size;
  Counter basic_blocks_ending_with_taken_cond;
  Counter basic_blocks_ending_with_always_taken_uncond_direct;
  Counter basic_blocks_ending_with_always_taken_uncond_indirect;
  Counter total_dyn_cache_lines;

  Counter& total_insts = regStat<Counter>("TotalInsts");
  Counter& total_dynamic_basic_blocks = regStat<Counter>("DynamicBasicBlocks");
  Counter& total_static_basic_blocks = regStat<Counter>("StaticBasicBlocks");

  Formula& percent_dyn_bb_ending_with_taken_cond = regStat<Formula>("PercentDynamicBBEndingWithTakenCond");
  Formula& percent_dyn_bb_ending_with_taken_uncond_direct = regStat<Formula>("PercentDynamicBBEndingWithTakenUncondDirect");
  Formula& percent_dyn_bb_ending_with_taken_uncond_indirect = regStat<Formula>("PercentDynamicBBEndingWithTakenUncondIndirect");

  Formula& avg_dyn_bb_size = regStat<Formula>("AverageDynBBSize");

  Formula& avg_dyn_cache_lines_per_dyn_bb = regStat<Formula>("AvgDynCacheLinesPerDynBB");
  
  FixedBucketsHistogram& bb_distribution = regStat<FixedBucketsHistogram>("BBDistrib", 1, 32);
  FixedBucketsHistogram& cache_lines_per_dyn_bb_distrib = regStat<FixedBucketsHistogram>("DynCacheLineDistrib", 1, 4);

  int computeTouchedCacheLines(DynBBInfo & info) const
  {
    std::unordered_set<Address> lines;
    for(const auto pc : info.m_insts)
    {
      lines.insert(pc >> 6);
    }
    return lines.size();
  }

  void terminateBasicBlock()
  {
    total_dynamic_basic_blocks++;

    cum_basic_block_size += m_current_bb.size();
    m_bbinfo[{m_current_bb_start_pc, current_context}].m_sizes[m_current_bb.size()]++;

    int cache_lines = computeTouchedCacheLines(m_current_bb);
    total_dyn_cache_lines += cache_lines;
    cache_lines_per_dyn_bb_distrib.addElement(cache_lines);
        
    bb_distribution.addElement(m_current_bb.size());

  }

  void handleContextChange(const InstPointer & inst)
  {
    if(getContext(inst) != current_context)
    {
      if(current_basic_block_size != 0)
      {
        terminateBasicBlock();
      }
      
      current_context = getContext(inst);
      m_current_bb_start_pc = getPC(inst);
      m_current_bb.reset();
    }
  }

  virtual void handleTakenBranch(const InstPointer & inst)
  {
    if(!isBranch(inst))
    {
      return;
    }

    if(isTakenBranch(inst))
    {
      assert(m_current_bb.size() > 0);
      terminateBasicBlock();

      m_current_bb_start_pc = getBranchTarget(inst);
      m_current_bb.reset();

      basic_blocks_ending_with_taken_cond += isCondBranch(inst);
      basic_blocks_ending_with_always_taken_uncond_direct += isUncondDirectBranch(inst);
      basic_blocks_ending_with_always_taken_uncond_indirect += isUncondIndirectBranch(inst);
    }
  }

  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  void processInst(const InstPointer & inst) override
  {
    handleContextChange(inst);
    current_basic_block_size++;
    m_current_bb.addInst(getPC(inst));
    handleTakenBranch(inst);
  }

  py::dict postProcessStats() const override
  {
    py::dict supplemental;

    // Some sanity checking
#ifndef NDEBUG
    double avg_bb_size = cum_basic_block_size.evaluate() / total_dynamic_basic_blocks.evaluate();
#endif
    double other_avg_bb_size = 0;
    double other_cum_size = 0;
    double other_total_bb = 0;
    for(const auto & bb : m_bbinfo)
    {
      for(const auto& size : bb.second.m_sizes)
      {
        other_cum_size += size.first * size.second;
        other_total_bb += size.second;
      }
    }
    other_avg_bb_size = other_cum_size / other_total_bb;
    assert(avg_bb_size == other_avg_bb_size);

   
    total_static_basic_blocks = m_bbinfo.size();

    // Work variables
    Counter dynamic_size;
    Counter total_static_cache_lines;

    Formula& avg_dyn_sizes_per_static_bb = regStat<Formula>("AvgDynSizesPerStaticBB");
    avg_dyn_sizes_per_static_bb = dynamic_size / m_bbinfo.size();
  
   
    
    Formula& avg_dyn_cache_lines_per_static_bb = regStat<Formula>("AvgDynCacheLinesPerStaticBB");
    avg_dyn_cache_lines_per_static_bb = total_static_cache_lines / m_bbinfo.size();
    

    FixedBucketsHistogram& cache_lines_per_static_bb_distrib = regStat<FixedBucketsHistogram>("StaticCacheLineDistrib", 1, 4);

    for(const auto & elt : m_bbinfo)
    {
      std::set<int> num_lines_static_block;
      dynamic_size += elt.second.m_sizes.size();
      for(const auto & exec : elt.second.m_sizes)
      {
        const ProgramCounter & pc = elt.first;
        Address start_cache_line = pc.m_pc >> 6;
        Address end_cache_line = (pc.m_pc + ((exec.first - 1) * 4)) >> 6;
        int num_cache_lines = (1 + (end_cache_line - start_cache_line));
        num_lines_static_block.insert(num_cache_lines);
      }
      cache_lines_per_static_bb_distrib.addElement(num_lines_static_block.size());
      total_static_cache_lines += num_lines_static_block.size();
    }

    FixedBucketsHistogram& num_size_per_bb_distrib = regStat<FixedBucketsHistogram>("StaticBBDynSizeDistrib", 1, 8);

    for(const auto & elt : m_bbinfo)
    {
      num_size_per_bb_distrib.addElement(elt.second.m_sizes.size());
    }

    Counter static_block_in_how_many_bigger_blocks{m_bbinfo.size()};
    Counter dyn_block_in_how_many_bigger_blocks{total_dynamic_basic_blocks};

    Formula& static_bb_dup_factor = regStat<Formula>("StaticBBDuplicationFactor");
    static_bb_dup_factor = static_block_in_how_many_bigger_blocks / m_bbinfo.size();

    Formula& dyn_bb_dup_factor = regStat<Formula>("DynamicBBDuplicationFactor");
    dyn_bb_dup_factor = dyn_block_in_how_many_bigger_blocks / total_dynamic_basic_blocks;
    
    // Let's look at BB duplication as well
    for(const auto & elt : m_bbinfo)
    {
      const ProgramCounter & pc = elt.first;
      Address current_pc = pc.m_pc;
      
      for(const auto & other_elt : m_bbinfo)
      {   
        const ProgramCounter & opc = other_elt.first;
        Address other_pc = opc.m_pc;
        bool found_in_one = false;

        if(other_pc == current_pc)
        {
          continue;
        }
    
        for(const auto & execs : other_elt.second.m_sizes)
        {
          int size = execs.first;

          const bool after_start = current_pc > other_pc;
          const bool before_end = current_pc <= (other_pc + ((size - 1) * 4));

          if(after_start && before_end)
          {
            found_in_one = true;
            dyn_block_in_how_many_bigger_blocks += execs.second;
          }
        }
        static_block_in_how_many_bigger_blocks += found_in_one;
      }
    }
   
    return supplemental;
  }
};


template<int MaxTaken>
struct PastTakenCondBasicBlocksProfiler : public BasicBlocksProfiler
{
  int m_current_taken_branches = 0;

  PastTakenCondBasicBlocksProfiler(py::dict params)
  : BasicBlocksProfiler(params)
  {}

  void handleTakenBranch(const InstPointer & inst) override
  {
    if(!isBranch(inst))
    {
      return;
    }
  
    if(isTakenBranch(inst))
    {
      const bool uncond_direct = isUncondDirectBranch(inst);
      const bool uncond_indirect = isUncondIndirectBranch(inst);
      if(m_current_taken_branches == MaxTaken || uncond_direct || uncond_indirect)
      {
        assert(m_current_bb.size() > 0);
        terminateBasicBlock();
      
        m_current_bb_start_pc = inst->m_br_info->m_target;
        m_current_bb.reset();
        m_current_taken_branches = 0;

        basic_blocks_ending_with_taken_cond += isCondBranch(inst);
        basic_blocks_ending_with_always_taken_uncond_direct += uncond_direct;
        basic_blocks_ending_with_always_taken_uncond_indirect += uncond_indirect;
      }
      else
      {
        m_current_taken_branches += isCondBranch(inst);
      }
    }
  }
};

template<int MaxTaken>
struct PastTakenUncondDirectBasicBlocksProfiler : public BasicBlocksProfiler
{
  int m_current_taken_uncond_direct_branches = 0;

  PastTakenUncondDirectBasicBlocksProfiler(py::dict params)
  : BasicBlocksProfiler(params)
  {}

  void handleTakenBranch(const InstPointer & inst) override
  {
    if(!isBranch(inst))
    {
      return;
    }

    if(isTakenBranch(inst))
    {
      const bool uncond_direct = isUncondDirectBranch(inst);
      const bool uncond_indirect = isUncondIndirectBranch(inst);
      if(m_current_taken_uncond_direct_branches == MaxTaken || isCondBranch(inst) || uncond_indirect)
      {
        assert(m_current_bb.size() > 0);
        terminateBasicBlock();
      
        m_current_bb_start_pc = inst->m_br_info->m_target;
        m_current_bb.reset();
        m_current_taken_uncond_direct_branches = 0;

        basic_blocks_ending_with_taken_cond += isCondBranch(inst);
        basic_blocks_ending_with_always_taken_uncond_direct += uncond_direct;
        basic_blocks_ending_with_always_taken_uncond_indirect += uncond_indirect;
      }
      else
      {
        m_current_taken_uncond_direct_branches += uncond_direct;
      }
    }
  }
};

}