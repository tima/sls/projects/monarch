/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <tools/interfaces.h>

#include <stats/stats.h>

#include <util/cache_model.h>

namespace Monarch
{

struct L1DCacheProfiler : public AbstractProfiler
{ 
  enum class ContentType
  {
    Valid,
    Invalid
  };

  using Entry = BaseCacheEntry<uint64_t, uint64_t, ContentType, SetStats>;

  SetAssocStructure<Entry> dcache;

  L1DCacheProfiler(py::dict params)
  : AbstractProfiler(params)
  , dcache
  (
    std::string("dcache"),
    this,
    params["knobs"]["line_size"].cast<int>(),
    params["knobs"]["num_sets"].cast<int>(),
    params["knobs"]["num_ways"].cast<int>(),
    getReplPolicyFromStr(params["knobs"]["repl_policy"].cast<std::string>())
  )
  {}

  void processInst(const InstPointer & inst) override
  {
    if(!isStore(inst) && !isLoad(inst))
    {
      return;
    }

    auto ea = memEffectiveAddr(inst);

    auto [entry, set, way] = dcache.search(ea, ContentType::Valid);

    if(entry == nullptr)
    {
      dcache.insert(ea, Entry{dcache.getTag(ea), ea, ContentType::Valid});
    }
  }

  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  py::dict postProcessStats() const override
  {
    return {};
  }
};

}