/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <map>
#include <set>

#include <tools/interfaces.h>

#include <stats/stats.h>

namespace Monarch
{

struct BranchProfiler : public AbstractProfiler
{
  struct ProfilerBranchInfo
  {
    uint64_t m_count = 0;
    uint64_t m_taken = 0;
    uint64_t m_non_taken = 0;
    
    StaticBranchInfo m_info;

    // To count how many different targets for indirect branches
    std::unordered_set<Address> targets;

    ProfilerBranchInfo()
    {
      assert(false);
    }

    ProfilerBranchInfo(const StaticBranchInfo & info)
    : m_info(info)
    {}

    void recordBranchExec(const InstPointer & inst)
    {
      m_count++;
      m_taken += isTakenBranch(inst);
      m_non_taken += !isTakenBranch(inst);

      if(isTakenBranch(inst))
      {
        targets.insert(getBranchTarget(inst));
      }
    }
  };

  BranchProfiler(py::dict params)
  : AbstractProfiler(params)
  {}

  InstPointer prev_inst = nullptr;

  std::unordered_map<ProgramCounter, ProfilerBranchInfo> m_data;
 
  // Stats
  Counter& total_insts = regStat<Counter>("TotalInsts");
  Counter& total_branches = regStat<Counter>("TotalBranches");
  
  Counter basic_blocks_ending_with_always_taken_cond;
  Counter basic_blocks_ending_with_always_taken_uncond_direct;
  Counter basic_blocks_ending_with_always_taken_uncond_indirect;

  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  void processInst(const InstPointer & inst) override
  { 
    total_insts++;

    if(!isBranch(inst))
    {
      return;
    }

    total_branches++;

    if(m_data.count(getFullPC(inst)) == 0)
    {
      m_data.insert({getFullPC(inst), *(inst->m_br_info->m_static_info)});
    }

    m_data[getFullPC(inst)].recordBranchExec(inst);

    if(isTakenBranch(inst))
    {
      basic_blocks_ending_with_always_taken_cond += isCondBranch(inst) && m_data[getFullPC(inst)].m_non_taken == 0;
      basic_blocks_ending_with_always_taken_uncond_direct += isUncondDirectBranch(inst);
      basic_blocks_ending_with_always_taken_uncond_indirect += isUncondIndirectBranch(inst);
    }

    prev_inst = inst;
  }

  // Yes, I am making a copy
  void processPercentage(std::vector<ProfilerBranchInfo> data, int thresh, uint64_t total_branches, const std::string & suffix) const
  {
    const double threshold = (thresh * 0.01) * (double) total_branches;

    Counter static_never_taken;
    Counter static_always_taken;
    
    Counter dynamic_never_taken;
    Counter dynamic_always_taken;

    Counter& dynamic_branches = regStat<Counter>("Dynamic" + suffix + std::to_string(thresh));
    Counter& static_branches = regStat<Counter>("Static" + suffix + std::to_string(thresh));
    
    // Uncond are always taken
    if(suffix == "CondBranches")
    {
      Formula& prc_static_never_taken = regStat<Formula>("PercentStatic" + suffix + "NeverTaken" + std::to_string(thresh));
      prc_static_never_taken = static_never_taken / static_branches;

      Formula& prc_dyn_never_taken = regStat<Formula>("PercentDynamic" + suffix + "NeverTaken" + std::to_string(thresh));
      prc_dyn_never_taken = dynamic_never_taken / dynamic_branches;
    }
  

    Formula& prc_static_always_taken = regStat<Formula>("PercentStatic" + suffix + "AlwaysTaken" + std::to_string(thresh));
    prc_static_always_taken = static_always_taken / static_branches;

    
    Formula& prc_dyn_always_taken = regStat<Formula>("PercentDynamic" + suffix + "AlwaysTaken" + std::to_string(thresh));
    prc_dyn_always_taken = dynamic_always_taken / dynamic_branches;

    double current_count = 0;

    for(auto it = data.begin(); it != data.end(); ++it)
    {
      current_count += it->m_count;
      if(current_count > threshold)
      {
        it++;
        data.erase(it, data.end());
        break;
      }
    }

    for(const auto & elt : data)
    {
      static_branches++;
      static_always_taken += (elt.m_non_taken == 0);
      static_never_taken += (elt.m_taken == 0);
      dynamic_branches += elt.m_count;
      dynamic_always_taken += (elt.m_non_taken == 0) ? elt.m_count : 0;
      dynamic_never_taken += (elt.m_taken == 0) ? elt.m_count : 0;
    }
  }

  py::dict postProcessStats() const override
  {
    py::dict supplemental;

    Counter static_cond_branches_never_taken;
    Counter static_cond_branches_always_taken;
    Counter static_cond_branches_sometimes_taken;
    Counter dynamic_cond_never_taken_branches;
    Counter dynamic_cond_always_taken_branches;
    Counter dynamic_cond_sometimes_taken_branches;
    Counter targets;
    Counter dyn_ind_br_single_target;
    
    Counter& static_cond_branches = regStat<Counter>("StaticCondBranches");
    Counter& static_uncond_direct_branches = regStat<Counter>("StaticUncondDirectBranches");
    Counter& static_uncond_indirect_branches = regStat<Counter>("StaticUncondIndirectBranches");
    Counter& dynamic_cond_branches = regStat<Counter>("DynamicCondBranches");
    Counter& dynamic_uncond_direct_branches = regStat<Counter>("DynamicUncondDirectBranches");
    Counter& dynamic_uncond_indirect_branches = regStat<Counter>("DynamicUncondIndirectBranches");

    Formula& prc_static_cond_never_taken  = regStat<Formula>("PercentStaticCondBranchesNeverTaken");
    prc_static_cond_never_taken = static_cond_branches_never_taken / static_cond_branches;

    Formula& prc_static_cond_always_taken = regStat<Formula>("PercentStaticCondBranchesAlwaysTaken");
    prc_static_cond_always_taken = static_cond_branches_always_taken / static_cond_branches;

    Formula& prc_dyn_cond_never_taken = regStat<Formula>("PercentDynamicCondBranchesNeverTaken");
    prc_dyn_cond_never_taken = dynamic_cond_never_taken_branches / dynamic_cond_branches;

    Formula& prc_dyn_cond_always_taken = regStat<Formula>("PercentDynamicCondBranchesAlwaysTaken");
    prc_dyn_cond_always_taken = dynamic_cond_always_taken_branches / dynamic_cond_branches;
   
    Formula& avg_tag_per_ind_br = regStat<Formula>("AvgTargsPerDynIndBr");
    avg_tag_per_ind_br = targets / dynamic_uncond_indirect_branches;

    Formula& static_ind_br_single_tar = regStat<Formula>("PercentageDynIndBrSingleTar");
    static_ind_br_single_tar = dyn_ind_br_single_target / dynamic_uncond_indirect_branches;
  
    std::vector<ProfilerBranchInfo> cond_br_list;
    std::vector<ProfilerBranchInfo> uncond_br_list;
    std::vector<ProfilerBranchInfo> ind_br_list;

    for(const auto & elt : m_data)
    {
      if(isCondBranch(elt.second.m_info))
      {
        static_cond_branches++;
        dynamic_cond_branches += elt.second.m_count;
        dynamic_cond_always_taken_branches += (elt.second.m_non_taken == 0) ? elt.second.m_count : 0;
        dynamic_cond_never_taken_branches += (elt.second.m_taken == 0) ? elt.second.m_count : 0;
        dynamic_cond_sometimes_taken_branches += (elt.second.m_taken != 0) && (elt.second.m_non_taken != 0) ? elt.second.m_count : 0;
        static_cond_branches_always_taken += elt.second.m_non_taken == 0;
        static_cond_branches_never_taken += elt.second.m_taken == 0;
        static_cond_branches_sometimes_taken += elt.second.m_taken != 0 && elt.second.m_non_taken != 0;
        cond_br_list.push_back(elt.second);
      }
      else if(isUncondDirectBranch(elt.second.m_info))
      {
        static_uncond_direct_branches++;
        dynamic_uncond_direct_branches += elt.second.m_count;
        uncond_br_list.push_back(elt.second);
      }
      else if(isUncondIndirectBranch(elt.second.m_info))
      {
        static_uncond_indirect_branches++;
        dynamic_uncond_indirect_branches += elt.second.m_count;
        ind_br_list.push_back(elt.second);
        targets += elt.second.targets.size();
        if(elt.second.targets.size() == 1)
        {
          dyn_ind_br_single_target += elt.second.m_count;
        }
      }
    }

    FixedBucketsHistogram& br_histo = regStat<FixedBucketsHistogram>("branches", 1, 6, 
      std::vector<std::string>{"IndBrMultiTarget", "IndBrSingleTarget", "CondNeverTaken", "CondAlwaysTaken", "Cond", "UncondDir"});

    br_histo.addElement(0, dyn_ind_br_single_target);
    br_histo.addElement(1, dynamic_uncond_indirect_branches.evaluate() - dyn_ind_br_single_target.evaluate());
    br_histo.addElement(2, dynamic_cond_never_taken_branches);
    br_histo.addElement(3, dynamic_cond_always_taken_branches);
    br_histo.addElement(4, dynamic_cond_branches.evaluate() - dynamic_cond_always_taken_branches.evaluate() - dynamic_cond_always_taken_branches.evaluate());
    br_histo.addElement(5, dynamic_uncond_direct_branches);

    std::cout << static_cond_branches.evaluate() << std::endl; 
    std::cout << static_uncond_direct_branches.evaluate() << std::endl;
    std::cout << static_uncond_indirect_branches.evaluate() << std::endl;
    std::cout << m_data.size() << std::endl;

    assert(static_cond_branches.evaluate() + static_uncond_direct_branches.evaluate() + static_uncond_indirect_branches.evaluate() == m_data.size());
    assert(static_cond_branches_always_taken.evaluate() + static_cond_branches_never_taken.evaluate() + static_cond_branches_sometimes_taken.evaluate() == static_cond_branches.evaluate());
    assert(dynamic_cond_branches.evaluate() + dynamic_uncond_direct_branches.evaluate() + dynamic_uncond_indirect_branches.evaluate() == total_branches.evaluate());
    assert(dynamic_cond_sometimes_taken_branches.evaluate() + dynamic_cond_never_taken_branches.evaluate() + dynamic_cond_always_taken_branches.evaluate() == dynamic_cond_branches.evaluate());

    return supplemental;
  }
};

}
