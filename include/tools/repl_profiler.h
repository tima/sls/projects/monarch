/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <tools/interfaces.h>

#include <stats/stats.h>

#include <util/cache_model.h>

namespace Monarch
{

struct ReplProfiler : public AbstractProfiler
{ 
  
  using Entry = BaseCacheEntry<uint64_t, uint64_t, BaseContentType, TypeAwareSetStats>;

  SetAssocStructure<Entry> dcache;
  SetAssocStructure<Entry> icache;
  SetAssocStructure<Entry> l2cache;

  PeriodicFormula& l2_content_type = this->regStat<PeriodicFormula>("l2_insts_percentage", [&]() -> double 
  {
    double result = 0.0;
    
    for(int i = 0; i < l2cache.numSets(); i++)
    {
      const auto & set = l2cache._cache[i];
      result += std::count_if(set._set.begin(), set._set.end(), [](const auto & entry)
      {
        return entry.getContentType() == Entry::ContentType::Instruction;
      });
    }

    return result / (l2cache.numSets() * l2cache.numWays() * 100.0);
  });

  uint64_t insts = 0;


  ReplProfiler(py::dict params)
  : AbstractProfiler(params)
  , dcache
  (
    std::string("dcache"),
    this,
    params["knobs"]["l1d_line_size"].cast<int>(),
    params["knobs"]["l1d_num_sets"].cast<int>(),
    params["knobs"]["l1d_num_ways"].cast<int>(),
    getReplPolicyFromStr(params["knobs"]["l1d_repl_policy"].cast<std::string>())
  )
  , icache
  (
    std::string("icache"),
    this,
    params["knobs"]["l1i_line_size"].cast<int>(),
    params["knobs"]["l1i_num_sets"].cast<int>(),
    params["knobs"]["l1i_num_ways"].cast<int>(),
    getReplPolicyFromStr(params["knobs"]["l1i_repl_policy"].cast<std::string>())
  )
  , l2cache
  (
    std::string("l2cache"),
    this,
    params["knobs"]["l2_line_size"].cast<int>(),
    params["knobs"]["l2_num_sets"].cast<int>(),
    params["knobs"]["l2_num_ways"].cast<int>(),
    getReplPolicyFromStr(params["knobs"]["l2_repl_policy"].cast<std::string>())
  )
  {}

  void processInst(const InstPointer & inst) override
  {
    if(insts++ % 100000 == 0)
    {
      l2_content_type.sample();
    }

    auto pc = getPC(inst);

    // We are assuming instructions are 4B aligned so no cache crossers.
    auto [l1i_entry, l1i_set, l1i_way] = icache.search(pc, Entry::ContentType::Instruction);

    // Look in L2
    if(l1i_entry == nullptr)
    {
      auto [l2_entry, l2_set, l2_way] = l2cache.search(pc, Entry::ContentType::Instruction);

      if(l2_entry == nullptr)
      {
        l2cache.insert(pc, Entry{l2cache.getTag(pc), pc, Entry::ContentType::Instruction});
      }

      auto [l1i_victim, l1i_victim_set, l1i_victim_way] = icache.insert(pc, Entry{icache.getTag(pc), pc, Entry::ContentType::Instruction});

      if(l1i_victim->isValid() && std::get<0>(l2cache.functionalSearch(pc)) == nullptr)
      {
        l2cache.insert(l1i_victim->getPayload(), Entry{l2cache.getTag(l1i_victim->getPayload()), l1i_victim->getPayload(), Entry::ContentType::Instruction});
      }
    }

    if(!isStore(inst) && !isLoad(inst))
    {
      return;
    }

    auto ea = memEffectiveAddr(inst);

    auto [l1d_entry, l1d_set, l1d_way] = dcache.search(ea, Entry::ContentType::Data);

    if(l1d_entry == nullptr)
    {
      auto [l2_entry, l2_set, l2_way] = l2cache.search(ea, Entry::ContentType::Data);

      if(l2_entry == nullptr)
      {
        l2cache.insert(ea, Entry{l2cache.getTag(ea), ea, Entry::ContentType::Data});
      }

      auto [l1d_victim, l1d_victim_set, l1d_victim_way] = dcache.insert(ea, Entry{dcache.getTag(ea), ea, Entry::ContentType::Data});

      if(l1d_victim->isValid() && std::get<0>(l2cache.functionalSearch(ea)) == nullptr)
      {
        l2cache.insert(l1d_victim->getPayload(), Entry{l2cache.getTag(l1d_victim->getPayload()), l1d_victim->getPayload(), Entry::ContentType::Data});
      }
    }

    // Cache crosser
    if(((ea + memAccessSize(inst) - 1) & 63) != (ea & 63))
    {
      ea = ea + memAccessSize(inst) - 1;
      
      auto [l1d_entry, l1d_set, l1d_way] = dcache.search(ea, Entry::ContentType::Data);

      if(l1d_entry == nullptr)
      {
        auto [l2_entry, l2_set, l2_way] = l2cache.search(ea, Entry::ContentType::Data);

        if(l2_entry == nullptr)
        {
          l2cache.insert(ea, Entry{l2cache.getTag(ea), ea, Entry::ContentType::Data});
        }

        auto [l1d_victim, l1d_victim_set, l1d_victim_way] = dcache.insert(ea, Entry{dcache.getTag(ea), ea, Entry::ContentType::Data});

        if(l1d_victim->isValid() && std::get<0>(l2cache.functionalSearch(ea)) == nullptr)
        {
          l2cache.insert(l1d_victim->getPayload(), Entry{l2cache.getTag(l1d_victim->getPayload()), l1d_victim->getPayload(), Entry::ContentType::Data});
        }
      }
    }
  }

  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  py::dict postProcessStats() const override
  {
    return {};
  }
};

}