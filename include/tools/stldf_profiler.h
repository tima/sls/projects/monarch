/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#include <tools/interfaces.h>

#include <stats/stats.h>

namespace Monarch
{

struct STLDFProfiler : public AbstractProfiler
{
  Counter total_instructions;
 
  std::unordered_map<Address, uint64_t> last_writer;

  Counter& stldf_loads = regStat<Counter>("stldf_loads");
  FixedBucketsHistogram& stldf_dist = regStat<FixedBucketsHistogram>("stldf_dist", 8, 32);
  
  STLDFProfiler(py::dict params)
  : AbstractProfiler(params)
  {}

  void processWrite(const InstPointer & inst)
  {
    for(unsigned i = 0; i < memAccessSize(inst); i++)
    {
      last_writer[memEffectiveAddr(inst) + i] = total_instructions.evaluate();
    }
  }

  uint64_t processRead(const InstPointer & inst)
  {
    uint64_t distance = 0;
    for(unsigned i = 0; i < memAccessSize(inst); i++)
    {
      Address ea = memEffectiveAddr(inst);

      if(last_writer.count(ea + i) != 0)
      {
        assert(total_instructions.evaluate() > last_writer[ea + i]);
        distance = std::max(distance, (uint64_t) total_instructions.evaluate() - last_writer[ea + i]);
      } 
    }
    return distance;
  }

  void checkFeatureReqs(Bitvector<TraceFeatures> & features) override {}

  void processInst(const InstPointer & inst) override {
    total_instructions++;
    if(isStore(inst))
    {
      processWrite(inst);
    }
    else if (isLoad(inst))
    {
      auto dist = processRead(inst);
      if(dist != 0 && dist < 512)
      {
        stldf_loads += dist != 0;
        stldf_dist.addElement(dist);
      }
    }
  }

  py::dict postProcessStats() const override
  {  
    return {};
  }
};

}