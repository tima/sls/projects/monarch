/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <trace/tracers/cvp/cvp_tracer.h>
#include <trace/lzma_buf.h>

using namespace Monarch;

int main(int argc, char ** argv)
{
  if(argc < 3)
  {
    std::cerr << "Expected trace path as argument" << std::endl;
    return -1;
  }

  uint64_t processed = 0;
  Monarch::Tracers::CVPTracer reader(argv[1], argv[2]);

  while(reader.processOneInst())
  {
    processed++;

    if(processed%1000000 == 0)
    {
      std::cout << "Processed " << std::dec << processed << " insts " << std::endl;
    }
  }

  return 0;
}
