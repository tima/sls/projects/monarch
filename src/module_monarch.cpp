/*
 * Copyright (c) 2022 Arthur Perais
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <string>
#include <sstream>

#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>

#include <trace/instruction.h>
#include <trace/driver.h>
#include <trace/reader/trace_reader.h>
#include <trace/reader/dummy_reader.h>

#include <util/cache_model.h>

#include <stats/stats.h>

#include <tools/interfaces.h>
#include <tools/mips_profiler.h>
#include <tools/iside_footprint_profiler.h>
#include <tools/l1d_cache_profiler.h>
#include <tools/repl_profiler.h>
#include <tools/functions_profiler.h>
#include <tools/basic_blocks_profiler.h>
#include <tools/branch_profiler.h>
#include <tools/loop_profiler.h>
#include <tools/stldf_profiler.h>
#include <tools/branch_predictors.h>

namespace py = pybind11;

#include "./cache_model.cpp"

using namespace Monarch;

PYBIND11_MODULE(Monarch, monarch) {
  monarch.doc() = "A set of helpful tools to analyze traces";

  py::class_<MonarchDriver>(monarch, "Driver")
    .def(py::init<>())
    .def("registerTraceReader", &MonarchDriver::registerTraceReader)
    .def("registerTool", &MonarchDriver::registerTool)
    .def("run", &MonarchDriver::run, py::call_guard<py::scoped_ostream_redirect, py::scoped_estream_redirect>())
    ;

  py::module_ tracing = monarch.def_submodule("Tracing", "Things dealing with tracing");

  py::class_<BaseTraceReader, std::shared_ptr<BaseTraceReader>>(tracing, "BaseTraceReader")
    .def("getInst", &BaseTraceReader::get_inst)
    ;

  py::class_<TraceReader, BaseTraceReader, std::shared_ptr<TraceReader>>(tracing, "TraceReader")
    .def(py::init<const char *>())
    ;

  py::class_<DummyTraceReader, BaseTraceReader, std::shared_ptr<DummyTraceReader>>(tracing, "DummyTraceReader")
    .def(py::init<>())
    ;

  py::class_<InstPointer>(tracing, "Instruction")
    .def("__repr__", 
    [](const InstPointer& inst)
    {
      std::stringstream ss;
      ss << *inst;
      return ss.str();
    })
    ;

  py::module_ stats = monarch.def_submodule("Stats", "Things dealing with stats");
  py::class_<Stat>(stats, "BaseStat")
    .def("getName", &Stat::name)
    .def("evaluate", &Stat::evaluate)
    .def("dump", &Stat::dump)
    ;

  py::class_<FixedBucketsHistogram, Stat>(stats, "FixedBucketsHistogram") 
    .def(py::init<const std::string &, uint64_t, uint64_t>())
    .def("addElement", &FixedBucketsHistogram::addElement)
    ;

  py::module_ tools = monarch.def_submodule("Tools", "Things dealing with tools");

  py::class_<AbstractProfiler, std::shared_ptr<AbstractProfiler>>(tools, "BaseProfiler")
    .def("processInst", &AbstractProfiler::processInst)
    .def("dumpStats", &AbstractProfiler::dumpStats)
    .def("serializeStats", &AbstractProfiler::serializeStats)
    .def("name", &AbstractProfiler::name)
    .def("category", &AbstractProfiler::category)
    ;

  py::class_<MIPSProfiler, AbstractProfiler, std::shared_ptr<MIPSProfiler>>(tools, "MIPSProfiler")
    .def(py::init<py::dict>())
    ;

  py::class_<ISideFootprintProfiler, AbstractProfiler, std::shared_ptr<ISideFootprintProfiler>>(tools, "ISideFootprintProfiler")
    .def(py::init<py::dict>())
    ;

  py::class_<L1DCacheProfiler, AbstractProfiler, std::shared_ptr<L1DCacheProfiler>>(tools, "L1DCacheProfiler")
    .def(py::init<py::dict>())
    ;

  py::class_<ReplProfiler, AbstractProfiler, std::shared_ptr<ReplProfiler>>(tools, "ReplProfiler")
    .def(py::init<py::dict>())
    ;

  py::class_<FunctionsProfiler, AbstractProfiler, std::shared_ptr<FunctionsProfiler>>(tools, "FunctionsProfiler")
    .def(py::init<py::dict>())
    ;

  py::class_<BasicBlocksProfiler, AbstractProfiler, std::shared_ptr<BasicBlocksProfiler>>(tools, "BasicBlocksProfiler")
    .def(py::init<py::dict>())
    ;
    
  py::class_<BranchProfiler, AbstractProfiler, std::shared_ptr<BranchProfiler>>(tools, "BranchProfiler")
    .def(py::init<py::dict>())
    ;

  py::class_<LoopStreamProfiler, AbstractProfiler, std::shared_ptr<LoopStreamProfiler>>(tools, "LoopStreamProfiler")
    .def(py::init<py::dict>())
    ;

  py::class_<STLDFProfiler, AbstractProfiler, std::shared_ptr<STLDFProfiler>>(tools, "STLDFProfiler")
    .def(py::init<py::dict>())
    ;

  py::class_<BPProfiler, AbstractProfiler, std::shared_ptr<BPProfiler>>(tools, "BPProfiler")
    .def(py::init<py::dict>())
    ;

}
