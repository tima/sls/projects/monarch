/*
 * Copyright (c) 2022 Arthur Perais & Avdibegovic Ugo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <util/cache_model.h>

namespace Monarch
{

template <typename EntryType>
ReplPolicyBase<EntryType>::ReplPolicyBase(Set<EntryType> * set)
: _set(set)
{}

template <typename EntryType>
RandomRepl<EntryType>::RandomRepl(Set<EntryType> * set)
: ReplPolicyBase<EntryType>(set)
{}

template <typename EntryType>
int RandomRepl<EntryType>::getVictimWay(const typename  EntryType::ContentType new_entry_type) const
{
  for(int way = 0; way < this->_set->numWays(); way++)
  {
    if(!this->_set->isWayValid(way))
    {
      return way;
    }
  }
  
  return static_cast<int>(rand() % this->_set->numWays());
} 

template <typename EntryType>
void RandomRepl<EntryType>::updateOnSearch(int way, const EntryType & entry) 
{}

template <typename EntryType>
void RandomRepl<EntryType>::updateOnInsert(int way, const EntryType & entry) 
{}

template <typename EntryType>
LRURepl<EntryType>::LRURepl(Set<EntryType> * set)
: ReplPolicyBase<EntryType>(set)
, _lru(set->numWays(), 0)
, _lru_count(0)
{
}

template <typename EntryType>
int LRURepl<EntryType>::getVictimWay(const typename  EntryType::ContentType new_entry_type) const
{
  uint64_t min_lru = ~0lu;
  int min_lru_way = 0;
  for(int way = 0; way < this->_set->numWays(); ++way)
  {
    if(!this->_set->isWayValid(way))
    {
      return way;
    }
    else if(_lru[way] < min_lru)
    {
      min_lru_way = way;
      min_lru = _lru[way];
    }
  }
  return min_lru_way;
} 

template <typename EntryType>
void LRURepl<EntryType>::updateOnSearch(int way, const EntryType & entry) 
{
  _lru[way] = ++_lru_count;
}

template <typename EntryType>
void LRURepl<EntryType>::updateOnInsert(int way, const EntryType & entry) 
{
  updateOnSearch(way, entry);
}

template <typename EntryType>
TypeAwareLRURepl<EntryType>::TypeAwareLRURepl(Set<EntryType> * set)
: ReplPolicyBase<EntryType>(set)
, _lru(set->numWays(), 0)
, _lru_count(0)
{
}

template <typename EntryType>
int TypeAwareLRURepl<EntryType>::getVictimWay(const typename EntryType::ContentType new_entry_type) const
{
  if constexpr(false /**InstDataAwareCacheEntry<EntryType>**/)
  {
    for(int way = 0; way < this->_set->numWays(); ++way)
    {
      if(!this->_set->isWayValid(way))
      {
        return way;
      }
    }

    uint64_t min_lru = ~0lu;
    int min_lru_way = 0;
    uint64_t min_i_lru = ~0lu;
    int min_i_lru_way = 0;
    uint64_t min_d_lru = ~0lu;
    int min_d_lru_way = 0;

  

    int i = 0;
    int num_inst_ways = 0;
    int num_data_ways = 0;

    for(const auto & entry : this->_set->_set)
    {
      if(_lru[i] < min_lru)
      {
        min_lru = _lru[i];
        min_lru_way = i;
      }

      if(entry.getContentType() == EntryType::ContentType::Instruction)
      {
        if(_lru[i] < min_i_lru)
        {
          min_i_lru = _lru[i];
          min_i_lru_way = i;
        }
        num_inst_ways++;
      }
      else if(entry.getContentType() == EntryType::ContentType::Data)
      {
        if(_lru[i] < min_d_lru)
        {
          min_d_lru = _lru[i];
          min_d_lru_way = i;
        }
        num_data_ways++;
      }
    }

    uint64_t i_proba = num_inst_ways;
    uint64_t d_proba = num_data_ways;

    auto i_way = min_i_lru_way;
    auto d_way = min_d_lru_way;

    switch(new_entry_type)
    {
      case EntryType::ContentType::Instruction:
        if((((rand() % (1 << i_proba)) == 0) && num_data_ways != 0) ||
          (d_way == min_lru_way))
        {
          return d_way;
        }
        else
        {
          return i_way;
        }
      case EntryType::ContentType::Data:
        if((((rand() % (1 << d_proba)) == 0) && num_inst_ways != 0) ||
          (i_way == min_lru_way))
        {
          return i_way;
        }
        else
        {
          return d_way;
        }
      default:
        assert(false);
        break;
    }

    return min_lru_way;
  }
  else
  {
    // Cannot use this repl policy if EntryType cannot tell Inst from Data
    // Will be handled in the constructor of Set
  }
  return -1;
} 

template <typename EntryType>
void TypeAwareLRURepl<EntryType>::updateOnSearch(int way, const EntryType & entry) 
{
  _lru[way] = ++_lru_count;
}

template <typename EntryType>
void TypeAwareLRURepl<EntryType>::updateOnInsert(int way, const EntryType & entry) 
{
  updateOnSearch(way, entry);
}


}